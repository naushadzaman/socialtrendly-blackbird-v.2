(function () {
	'use strict';

	angular.module("exampleApp")
	.directive('scrollToTop', function($rootScope){
		return {
			restrict: 'A',
			link: function(scope, $elm) {
		      $elm.on('click', function() {
		        $(".gallery-wrap").animate({scrollTop: 0}, 300);
		        $rootScope.streamToggleCheck = true;
            	$rootScope.showNewPostsButton = false;
		      });
		    }
		}
	});
})();
