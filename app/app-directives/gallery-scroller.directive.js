(function () {
	'use strict';

	angular.module("exampleApp")
	.directive('galleryScroller', function($rootScope){
		return {
			restrict: 'A',
			scope: true,
			link: function(scope, element, attributes){
				$(element).on('scroll', function(){
					if (element.scrollTop() > 100) {
						$rootScope.streamToggleCheck = false;
						$rootScope.showNewPostsButton = true;
						$rootScope.$apply();
					}
					else {
						$rootScope.streamToggleCheck = true;
						$rootScope.showNewPostsButton = false;
						$rootScope.$apply();
					}
				});
			}
		}
	});
})();