(function () {
	'use strict';

	angular.module("exampleApp")
	.directive('imgError', function() {
	  return {
	    restrict:'A',
	    link: function(scope, element, attr) {
	      element.on('error', function() {
	        element.attr('src', attr.imgError);
	      })
	    }
	  }
	});
})();
