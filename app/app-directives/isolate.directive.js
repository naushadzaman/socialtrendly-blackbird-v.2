(function () {
	'use strict';

	angular.module("exampleApp")
	.directive('isolate', function() {
    	return {scope: true};
	});
})();
