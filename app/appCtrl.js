(function () {
  'use strict';

  angular.module("exampleApp")
  .controller("globalCtrl", ['$rootScope', '$scope', '$location', '$http','NgMap', 'mapService', 'modalService', function($rootScope, $scope, $location, $http, NgMap, mapService, modalService) {
      /*$rootScope.userInfo = {
        username: "",
        api_key: ""
      }*/

      $rootScope.checkInit = false;
      $rootScope.checkInitRecorded = false;
      $rootScope.radiusValue = 5;
      $rootScope.radiusUnit = 'mi';
      $rootScope.plusDisabled = false;
      $rootScope.minusDisabled = false;
      $rootScope.smallGalleryItem = false;
      $rootScope.smallSortSelects = false;
      $rootScope.makeOvermapSmall = false;

      //$rootScope.setRadiusDisabled = false;

      $rootScope.galleryShow = 'col-md-6';
      $rootScope.mapShow = 'col-md-6';
      $rootScope.mapShowStatus = true;
      $rootScope.galleryShowStatus = true;
      $rootScope.tabIndex = 'split-view';

      $rootScope.showNewPostsButton = false;

      $rootScope.postsId = [];

      $rootScope.markersArray = [];
      $rootScope.hotspotArray = [];
      $scope.nowDate = new Date();

      $rootScope.history = [];

      $rootScope.showFullImage = {
        image: '',
        post: ''
      };

      $rootScope.instAccount = {
        username: '',
        pass: ''
      }

      $rootScope.dashboardCache = {
        radius: $rootScope.radiusValue,
        uom: $rootScope.radiusUnit,
        zoom: 11,
        feed: '',
        hotspots: '',
        cached: false,
        center: { lat: 40.74888970000001, lng: -73.99025010000003},
        stream: false
      }

      $rootScope.saveNewLocation = false;    

      $scope.getClass = function (path) {
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
      }

      angular.element(document).on('click', function () {
        $scope.showSignOutPopup = false;
        $('.sign-out-popup').removeClass('active');
        $scope.showNotificationPopup = false;
        $('.notif-popup').removeClass('active');
        $scope.$apply();
      });

      $scope.$on('$locationChangeStart', function(next, current) {
        $rootScope.history.push($location.$$path);

        $rootScope.$emit("stopStreamData", {});
        /*if ($location.$$path == '/dashboard') {
          $state.go($state.current, {}, {reload: true});
        }*/
      });

      $scope.callInstagramPopUp = function($event) {
        modalService.addInstagramPopUp($event, $scope);
      }

    }]);
})();
