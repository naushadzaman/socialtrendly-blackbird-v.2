(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('tabService', ['$rootScope', function($rootScope) {
      var service = {};

      service.splitView = splitView;
      service.mapShowFull = mapShowFull;
      service.galleryShowFull = galleryShowFull;
      service.examineView = examineView;
      service.changeTab = changeTab;

      return service;

      function splitView() {
        $rootScope.galleryShow = 'col-md-6';
        $rootScope.mapShow = 'col-md-6';
        $rootScope.mapShowStatus = true;
        $rootScope.smallSortSelects = false;
        $rootScope.galleryShowStatus = true;
        $rootScope.smallGalleryItem = false;
        $rootScope.makeOvermapSmall = false;
        $rootScope.examineShowStatus = false;
      }

      function mapShowFull() {
        $rootScope.mapShow = 'col-md-12';
        $rootScope.galleryShowStatus = false;
        $rootScope.mapShowStatus = true;
        $rootScope.makeOvermapSmall = true;
        $rootScope.examineShowStatus = false;
      }

      function galleryShowFull() {
        $rootScope.smallSortSelects = true;
        $rootScope.smallGalleryItem = true;
        $rootScope.galleryShow = 'col-md-12';
        $rootScope.mapShowStatus = false;
        $rootScope.galleryShowStatus = true;
        $rootScope.examineShowStatus = false;
      }

      function examineView () {
        $rootScope.mapShowStatus = false;
        $rootScope.galleryShowStatus = false;
        $rootScope.examineShow = 'col-md-12';
        $rootScope.examineShowStatus = true;
      }

      function changeTab(tab) {
        $rootScope.tabIndex = tab;
      }

    }]);

})();
