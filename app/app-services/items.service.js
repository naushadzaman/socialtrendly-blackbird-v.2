(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('ItemsService', ['$http', '_apiItems', function($http, _apiItems) {
      var service = {};

      service.GetAll = GetAll;
      service.GetById = GetById;
      service.UpdateItem = UpdateItem;
      service.RemoveById = RemoveById;
      service.CreateItem = CreateItem;

      return service;

      // ALL THIS CASES FOR RESTFUL API:

      function GetAll() {
        return $http.get(_apiItems).then(handleSuccess);
      }

      function GetById(id) {
        return $http.get(_apiItems + id).then(handleSuccess);
      }

      function RemoveById(item) {
        return $http.delete(_apiItems + item.id, item).then(handleSuccess);
      }

      function UpdateItem(item) {
        return $http.put(_apiItems + item.id, item).then(handleSuccess);
      }

      function CreateItem(item) {
        return $http.post(_apiItems, item).then(handleSuccess);
      }

      // PRIVATE FUNCTIONS:
      function handleSuccess(res) {
        return res.data;
      }

      function handleError(error) {
        return function () {
          return { success: false, message: error };
        };
      }
    }])


})();
