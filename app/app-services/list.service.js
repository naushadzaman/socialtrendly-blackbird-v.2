(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('ListService', ['$http', '_apiUsers', '$cookies', '$rootScope', function($http, _apiUsers, $cookies, $rootScope) {
      var service = {};

      service.createNewList = createNewList;
      service.getListData = getListData;
      service.deleteList = deleteList;
      service.getListDataFull = getListDataFull;
      service.checkSort = checkSort;
      service.addPostToList = addPostToList;
      service.deletePostFromList = deletePostFromList;
      
      return service;

      function checkSort(sort) {
        if (sort == undefined) return '';
        else return '&order_by='+sort;
      }

      function createNewList(body, callback, error) {
        $http({
          method: 'POST',
          url: _apiUsers + 'api/user_list/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          data: body
        }).then(callback, error);
      }

      function getListData(sort, callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/user_list/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key+checkSort(sort)
        }).then(callback, error);
      }

      function deleteList(listId, callback, error) {
        $http({
          method: 'DELETE',
          url: _apiUsers + 'api/user_list/'+listId+'/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }

      function getListDataFull(listId, callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/user_list/'+listId+'/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }

      function addPostToList(postId, listId, callback, error) {
        var postIdArray = [];
        postIdArray.push(postId);
        var body = {
          objects: postIdArray
        }
        $http({
          method: 'PATCH',
          url: _apiUsers + 'api/user_list/'+listId+'/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          data: body
        }).then(callback, error);
      }

      function deletePostFromList(listId, postsId, callback, error) {
        var body = {
          objects: postsId
        }
        $http({
          method: 'PUT',
          url: _apiUsers + 'api/user_list/'+listId+'/?username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          data: body
        }).then(callback, error);
      }

    }]);
})();



