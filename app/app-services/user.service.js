(function () {
  'use strict';

  angular.module("exampleApp")
  .factory('UserService', ['$http', '_apiUsers', function($http, _apiUsers) {
    var service = {};

    service.Create = Create;

    return service;

    function Create(user) {
      return $http.post(_apiUsers, user).then(handleSuccess);
    }

    function handleSuccess(res) {
      return res.data;
    }
  }]);

})();
