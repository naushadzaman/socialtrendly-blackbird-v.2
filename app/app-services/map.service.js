(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('mapService', ['$rootScope', '$http', '_apiUsers', 'NgMap', 'modalService', '$compile', '$location', '$window', function($rootScope, $http, _apiUsers, NgMap, modalService, $compile, $location, $window) {
      var service = {};

      var circle;
      var circleRecorded;
      var circleEvent;
      var circleHotspot;
      var circleSavedLocation;

      var circlesArray = [circle, circleRecorded, circleEvent, circleHotspot, circleSavedLocation];

      var mapDefault = {};
      var mapZoomed = false;

      var infoWindowArray = [];

      service.mapInit = mapInit;
      service.mapUpdate = mapUpdate;
      service.mapInitRecorded = mapInitRecorded;
      service.mapInitEvent = mapInitEvent;
      service.radiusMinus = radiusMinus;
      service.radiusPlus = radiusPlus;
      service.getAddress = getAddress;
      service.circleUpdate = circleUpdate;
      service.circleActive = circleActive;
      service.circleDefault = circleDefault;
      service.drawMarker = drawMarker;
      service.drawHotspots = drawHotspots;
      service.cleanDefaultMapValue = cleanDefaultMapValue;
      service.zoomToPostPin = zoomToPostPin;
      service.zoomToMap = zoomToMap;
      service.showPinOnMap = showPinOnMap;
      service.displayTwitterPins = displayTwitterPins;
      service.displayInstagramPins = displayInstagramPins;
      service.checkMapRadius = checkMapRadius;
      service.checkUserAvatar = checkUserAvatar;
      service.clearMarkers = clearMarkers;
      service.clearHotspots = clearHotspots;
      service.checkSocialCount = checkSocialCount;
      service.setDefaultPin = setDefaultPin;
      service.initHotspot = initHotspot;
      service.mapInitHotspot = mapInitHotspot;
      service.mapInitSavedLocation = mapInitSavedLocation;
      service.checkDasboardCache = checkDasboardCache;

      return service;

      function cleanDefaultMapValue() {
        mapDefault = {};
      }

      function checkMapRadius(radius, radiusUnit) {
        if(radiusUnit == "mi") {
          return radius * 1.6 * 1000;
        }
        else {
          return radius * 0.3048;
        }
      }
      //
      // INIT MAP FOR DASHBOARD PAGE
      //

      function checkDasboardCache(cached) {
        if(cached == true) return {lat: $rootScope.dashboardCache.center.lat(), lng: $rootScope.dashboardCache.center.lng()};
        else return {lat: $rootScope.dashboardCache.center.lat, lng: $rootScope.dashboardCache.center.lng};
      }

      function mapInit() {
        NgMap.getMap("mapDashboard").then(function(map) {
          /*var mapCenter = {
            lat: 40.74888970000001,
            lng: -73.99025010000003
          };*/
          function checkMapZoom(zoom) {
            if (zoom == undefined) {
              return 11;
            }
            else return zoom;
          }

          map.setOptions({
            mapTypeId: 'hybrid',
            zoom: checkMapZoom($rootScope.dashboardCache.zoom),
            center: checkDasboardCache($rootScope.dashboardCache.cached),
            mapTypeControl: false
          });

          circle = new google.maps.Circle({
            center: checkDasboardCache($rootScope.dashboardCache.cached),
            radius:  checkMapRadius($rootScope.dashboardCache.radius, $rootScope.dashboardCache.uom),
            strokeColor: '#feffff',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#d6d5d3',
            fillOpacity: 0.35,
            map: map,
            draggable: true,
            active: false
          });

          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(circle.getCenter());
          },100);

          circle.addListener('dragend', function(event) {
            map.setCenter(circle.getCenter());
            getCirclePosition();
            getAddress(circle.getCenter().lat(), circle.getCenter().lng());
          });

          function getCirclePosition() {
            $rootScope.circlePosition = circle.getCenter();
          }
          
          getCirclePosition();

          getAddress(circle.getCenter().lat(), circle.getCenter().lng());

          /*$rootScope.setRadiusDisabled = false;
          $rootScope.plusDisabled = false;
          $rootScope.minusDisabled = false;
          $rootScope.radiusUnit == 'mi';
          $rootScope.radiusValue = 10;*/
          $rootScope.dashboardMapCenter = map.getCenter();
        });
      };

      //
      // INIT MAP FOR RECORDED PAGE
      //

      function mapInitRecorded(mapCenterLat, mapCenterLng, mapRadius, mapRadiusUnit, mapZoom) {
        NgMap.getMap("mapRecorded").then(function(map) {
          var mapCenter = {
            lat: mapCenterLat,
            lng: mapCenterLng
          };

          map.setOptions({
            mapTypeId: 'hybrid',
            zoom: mapZoom,
            center: {lat: mapCenter.lat, lng: mapCenter.lng},
            mapTypeControl: false
          });

          circleRecorded = new google.maps.Circle({
            center: {lat: mapCenter.lat, lng: mapCenter.lng},
            radius:  checkMapRadius(mapRadius, mapRadiusUnit),
            strokeColor: '#15ef39',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#000',
            fillOpacity: 0.35,
            map: map,
            draggable: false
          });

          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(circleRecorded.getCenter());
          },100);

          $rootScope.circlePositionRecorded = circleRecorded.getCenter();
        });
      };


      //
      // INIT MAP FOR EVENT PAGE
      //

      function mapInitEvent(mapCenterLat, mapCenterLng, mapRadius, mapRadiusUnit, mapZoom) {
        NgMap.getMap("mapEvent").then(function(map) {
          var mapCenter = {
            lat: mapCenterLat,
            lng: mapCenterLng
          };

          map.setOptions({
            mapTypeId: 'hybrid',
            zoom: mapZoom,
            center: {lat: mapCenter.lat, lng: mapCenter.lng},
            mapTypeControl: false
          });

          circleEvent = new google.maps.Circle({
            center: {lat: mapCenter.lat, lng: mapCenter.lng},
            radius:  checkMapRadius(mapRadius, mapRadiusUnit),
            strokeColor: '#15ef39',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#000',
            fillOpacity: 0.35,
            map: map,
            draggable: false
          });

          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(circleEvent.getCenter());
          },100);

          getAddress(circleEvent.getCenter().lat(), circleEvent.getCenter().lng());

          $rootScope.circlePositionEvent = circleEvent.getCenter();
        });
      };

      function mapInitHotspot(mapCenterLat, mapCenterLng, mapRadius, mapRadiusUnit, mapZoom) {
        NgMap.getMap("mapHotspot").then(function(map) {
          var mapCenter = {
            lat: mapCenterLat,
            lng: mapCenterLng
          };

          map.setOptions({
            mapTypeId: 'hybrid',
            zoom: mapZoom,
            center: {lat: parseFloat(mapCenter.lat), lng: parseFloat(mapCenter.lng)},
            mapTypeControl: false
          });

          circleHotspot = new google.maps.Circle({
            center: {lat: parseFloat(mapCenter.lat), lng: parseFloat(mapCenter.lng)},
            radius:  checkMapRadius(mapRadius, mapRadiusUnit),
            strokeColor: '#15ef39',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#000',
            fillOpacity: 0.35,
            map: map,
            draggable: false
          });

          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(circleHotspot.getCenter());
          },100);

          getAddress(circleHotspot.getCenter().lat(), circleHotspot.getCenter().lng());

          $rootScope.circlePositionHotspot = circleHotspot.getCenter();
        });
      };

      function mapInitSavedLocation(mapCenterLat, mapCenterLng, mapRadius, mapRadiusUnit, mapZoom) {
        NgMap.getMap("mapSaveLocation").then(function(map) {
          var mapCenter = {
            lat: mapCenterLat,
            lng: mapCenterLng
          };

          map.setOptions({
            mapTypeId: 'hybrid',
            zoom: mapZoom,
            center: {lat: parseFloat(mapCenter.lat), lng: parseFloat(mapCenter.lng)},
            mapTypeControl: false
          });

          circleSavedLocation = new google.maps.Circle({
            center: {lat: parseFloat(mapCenter.lat), lng: parseFloat(mapCenter.lng)},
            radius:  checkMapRadius(mapRadius, mapRadiusUnit),
            strokeColor: '#15ef39',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: '#000',
            fillOpacity: 0.35,
            map: map,
            draggable: false
          });

          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            map.setCenter(circleSavedLocation.getCenter());
          },100);

          getAddress(circleSavedLocation.getCenter().lat(), circleSavedLocation.getCenter().lng());

          $rootScope.circlePositionSavedLocation = circleSavedLocation.getCenter();
        });
      };


      //
      // CIRCLE RADIUS DECREASE FUNCTION
      //

      function radiusMinus($scope) {
        $rootScope.plusDisabled = false;
        if ($rootScope.radiusUnit == 'mi'){
            $rootScope.radiusValue = $rootScope.radiusValue - 0.25;
            if ($rootScope.radiusValue - 1 < 0){
                $rootScope.radiusValue = 5000;
                $rootScope.radiusUnit = 'ft';
                circle.setRadius($rootScope.radiusValue * 0.3048);
            }
            else {
              circle.setRadius($rootScope.radiusValue * 1.6 * 1000);
            }
        }
        else {
          $rootScope.radiusValue = $rootScope.radiusValue - 500;
          if ($rootScope.radiusValue - 500 < 0) {
            $rootScope.radiusValue = 100;
            $rootScope.minusDisabled = true;
            circle.setRadius($rootScope.radiusValue * 0.3048);
          }
          else {
            circle.setRadius($rootScope.radiusValue * 0.3048);
          }
        }
      };

      //
      // CIRCLE RADIUS INCREASE FUNCTION
      //

      function radiusPlus($scope) {
        $rootScope.minusDisabled = false;     
        if ($rootScope.radiusUnit == 'mi'){
          $rootScope.radiusValue = $rootScope.radiusValue + 0.25;
          circle.setRadius($rootScope.radiusValue * 1.6 * 1000);
          if ($rootScope.radiusValue + 1 > 20.75 && $rootScope.radiusUnit == 'mi') {
            $rootScope.plusDisabled = true;
          }
        }
        else {
          if ($rootScope.radiusValue == 100) {
            $rootScope.radiusValue = $rootScope.radiusValue + 400;
          }
          else {
            $rootScope.radiusValue = $rootScope.radiusValue + 500;
          }
          if ($rootScope.radiusValue + 100 > 5000) {
            $rootScope.radiusValue = 1;
            $rootScope.radiusUnit = 'mi';
            circle.setRadius($rootScope.radiusValue * 1.6 * 1000);
          }
          else {
            circle.setRadius($rootScope.radiusValue * 0.3048);
          }
        }
      };

      //
      // MAP UPDATE FUNCTION
      //

      function mapUpdate(page, map) {
        if (page == 'dashboard') {
          var centerLocation = $rootScope.circlePosition;
        }
        else if (page == 'event') {
          var centerLocation = $rootScope.circlePositionEvent;
        }
        else if (page == 'hotspot') {
          var centerLocation = $rootScope.circlePositionHotspot;
        }
        else if (page == 'savedLocation') {
          var centerLocation = $rootScope.circlePositionSavedLocation;
        }
        else {
          var centerLocation = $rootScope.circlePositionRecorded;
        }

        NgMap.getMap(map).then(function(map) {
          window.setTimeout(function(){
            google.maps.event.trigger(map, 'resize');
            //map.setCenter(centerLocation); Set map on center
          }, 100);
        });
      };

      //
      // CIRCLE UPDATE FUNCTION
      //

      function circleUpdate(latlng) {
        NgMap.getMap().then(function(map) {
          circle.setCenter(latlng);
          $rootScope.circlePosition = circle.getCenter();
        });
      };

      //
      // CONVERT LAT/LON OF CIRCLE CENTER TO REAL ADDRESS
      //

      function getAddress(latitude, longitude) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            var method = 'GET';
            var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&sensor=true';
            var async = true;

            request.open(method, url, async);
            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        var data = JSON.parse(request.responseText);
                        var address = data.results[0];
                        resolve(address);
                        $rootScope.addressName = address.formatted_address;
                        $rootScope.$apply();
                    }
                    else {
                        reject(request.status);
                    }
                }
            };
            request.send();
        });
      };

      //
      // SET CIRCLE ACTIVE
      //

      function circleActive(map, circleName) {
        $rootScope.setRadiusDisabled = true;
        var activeCircleStyles = {
          strokeColor: '#15ef39',
          fillColor: '#000',
          fillOpacity: 0.35,
          draggable: false,
          active: true
        }
        NgMap.getMap(map).then(function(map) {
          if (circleName == 'circleDashboard') circle.setOptions(activeCircleStyles);
          else circleRecorded.setOptions(activeCircleStyles);
        });
      };


      //
      //SET CIRCLE DEFAULT
      //
      function circleDefault(map, circleName) {
        $rootScope.setRadiusDisabled = false;
        var activeCircleStyles = {
          strokeColor: '#feffff',
          fillColor: '#d6d5d3',
          fillOpacity: 0.35,
          draggable: true,
          active: false
        }
        NgMap.getMap(map).then(function(map) {
          if (circleName == 'circleDashboard') circle.setOptions(activeCircleStyles);
          else circleRecorded.setOptions(activeCircleStyles);
        });
      };

      //
      //DRAW MAKRER
      //

      function setDefaultPin() {
        for (var item in $rootScope.markersArray) {
          if($rootScope.markersArray[item].type == 'twitter') {
              if ($rootScope.markersArray[item].category != 'Generic') {
                if ($rootScope.markersArray[item].category == '') {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/twitter-Pin.png',
                    zIndex: 9900
                  });
                }
                else {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/'+$rootScope.markersArray[item].category+'-pin.png',
                    zIndex: 1
                  });
                }
              }
              else {
                $rootScope.markersArray[item].setOptions({
                  icon: '../dist/images/twitter-Pin.png',
                  zIndex: 1
                });
              }
            }
            else if ($rootScope.markersArray[item].type == 'instagram') {
              if ($rootScope.markersArray[item].category != 'Generic') {
                if ($rootScope.markersArray[item].category == '') {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/instagram-Pin.png',
                    zIndex: 9900
                  });
                }
                else {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/'+$rootScope.markersArray[item].category+'-pin.png',
                    zIndex: 1
                  });
               }
              }
              else {
                $rootScope.markersArray[item].setOptions({
                  icon: '../dist/images/instagram-Pin.png',
                  zIndex: 1
                });
              }
            }
        }
      }

      function markLastPin(platform){
        for (var item in $rootScope.markersArray) {
          if ($rootScope.markersArray[item] == $rootScope.markersArray[$rootScope.markersArray.length-1]) {
            $rootScope.markersArray[item].setOptions({
              icon: '../dist/images/'+platform+'-recent-pin.png'
            });
          }
          else {
            if($rootScope.markersArray[item].type == 'twitter') {
              if ($rootScope.markersArray[item].category != 'Generic') {
                if ($rootScope.markersArray[item].category == '') {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/twitter-Pin.png',
                    zIndex: 9900
                  });
                }
                else {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/'+$rootScope.markersArray[item].category+'-pin.png',
                    zIndex: 9900
                  });
                }
              }
              else {
                $rootScope.markersArray[item].setOptions({
                  icon: '../dist/images/twitter-Pin.png'
                });
              }
            }
            else if ($rootScope.markersArray[item].type == 'instagram') {
              if ($rootScope.markersArray[item].category != 'Generic') {
                if ($rootScope.markersArray[item].category == '') {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/instagram-Pin.png',
                    zIndex: 9900
                  });
                }
                else {
                  $rootScope.markersArray[item].setOptions({
                    icon: '../dist/images/'+$rootScope.markersArray[item].category+'-pin.png',
                    zIndex: 9900
                  });
                }
              }
              else {
                $rootScope.markersArray[item].setOptions({
                  icon: '../dist/images/instagram-Pin.png'
                });
              }
            }
          }
        }
      };

      function closeAllInfoWindows() {
        for(var infowindow in infoWindowArray) {
          infoWindowArray[infowindow].close();
        }
      }


      function checkUserAvatar(image) {
        /*if (image != 'https://s3.amazonaws.com/strendly-resources/ProfileRemoved.jpg' && image.indexOf('https://si0.twimg.com') < 0 && image.indexOf('https://pbs.twimg.com/') < 0) {
          var xhr = new XMLHttpRequest();
          xhr.open('HEAD', image, false);
          xhr.send();
          if (xhr.status == "404") {
              return '../../dist/images/no-user-img.png';
          } else {
              return image;
          }
        }
        else {
          return '../../dist/images/no-user-img.png';
        }*/
        return image;
      }

      function drawMarker(mapId, circleName, $scope, location, platform, category, post_id, content, user_avatar, user_name, media_url){
        NgMap.getMap(mapId).then(function(map) {
            var platformIcon = (platform == "twitter") ? '../dist/images/twitter-Pin.png' : '../dist/images/instagram-Pin.png';

            var imageExistClass;

            var marker = new google.maps.Marker({
              id: post_id,
              position: new google.maps.LatLng(location[1], location[0]),
              map: map,
              icon: platformIcon,
              animation: google.maps.Animation.DROP,
              type: platform,
              category: category,
              zIndex: (typeof platform !== "undefined") ? 1 : 0
            });

            if (media_url.length > 1) {
              imageExistClass = 'yes';
            }
            else {
              imageExistClass = 'no';
            }

            var markerInfoWindowContent = '<div class="infowindow"><div class="user-avatar"><img src="'
                     + checkUserAvatar(user_avatar) 
                     + '"><span class="platform-icon"><img src="../dist/images/'
                     + platform +'Platform.png" alt="" /></span></div>' 
                     + '<span class="user-name">'
                     + user_name +'</span>' 
                     + '<p class="infowindow-content">' 
                     + content + '</p><div class="infowindow-content-image '
                     + imageExistClass +'"><i class="fa fa-search map-show-full-img" aria-hidden="true"></i><img src="'+ media_url +'"></div></div>';

            var markerInfoWindow = new google.maps.InfoWindow({
              content: markerInfoWindowContent
            });

            if (platform == 'instagram' && $scope.checkInstagramPins == false) {
              marker.setVisible(false);
            }
            if (platform == 'twitter' && $scope.checkTwitterPins == false) {
              marker.setVisible(false);
            }

            infoWindowArray.push(markerInfoWindow);

            marker.addListener('click', function() {
              closeAllInfoWindows();
              markerInfoWindow.open(map, marker);
            });

            map.addListener('click', function() {
              markerInfoWindow.close();
            });

            if (circleName == 'circle')google.maps.event.addListener(circle, 'click', function(){markerInfoWindow.close();});
            else if (circleName == 'circleRecorded')google.maps.event.addListener(circleRecorded, 'click', function(){markerInfoWindow.close();});
            else if (circleName == 'circleEvent')google.maps.event.addListener(circleEvent, 'click', function(){markerInfoWindow.close();});
            else if (circleName == 'circleSavedLocation')google.maps.event.addListener(circleSavedLocation, 'click', function(){markerInfoWindow.close();});

            
            if(mapId == 'mapDashboard') $rootScope.markersArray.push(marker);
            else $rootScope.markersArray.unshift(marker);
            markLastPin(platform);
          });

        };

        function checkSocialCount(postsCount) {
          if (postsCount == undefined) {
            return 0;
          }
          else {
            return postsCount;
          }
        }

        function drawHotspots(mapId, circleName, location, postCount, twitterCount, instagramCount, hotspotId, sentimentValue, categoryCount, category, redirectToValue) {
          NgMap.getMap(mapId).then(function(map) {
            var icon = '';          
            
            if(category == 'Generic') {
              if(sentimentValue > 0){
                icon = '../dist/images/Pins/GREEN/G';
                if(postCount < 100){
                    var pin = Math.floor(postCount / 10) * 10;
                    pin = pin + '.png';
                    icon = icon.concat(pin);
                }else if (postCount <1000){
                    var pin = Math.floor(postCount / 100) * 100;
                    pin = pin + '.png';
                    icon = icon.concat(pin);
                }else if (postCount < 6000){
                    var k = 2000;
                    var pin = nFormatter(postCount, 0);
                    pin = pin + '.png';
                    icon = icon.concat(pin);
                }
              }else if (sentimentValue < 0){ //negative
                  icon = '../dist/images/Pins/RED/R';
                  if(postCount < 100){
                      var pin = Math.floor(postCount / 10) * 10;
                      pin = pin + '.png';
                      icon = icon.concat(pin);
                  }else if (postCount <1000){
                      var pin = Math.floor(postCount / 100) * 100;
                      pin = pin + '.png';
                      icon = icon.concat(pin);
                  }else if (postCount < 6000){
                      var k = 2000;
                      var pin = nFormatter(postCount, 0);
                      pin = pin + '.png';
                      icon = icon.concat(pin);
                  }
              }
            }
            else {
              icon = '../dist/images/category/'+category+'-Hotspot.png';
            }

            var hotspotMarker = new google.maps.Marker({
              id: hotspotId,
              position: new google.maps.LatLng(location[1], location[0]),
              map: map,
              icon: icon,
              /*animation: google.maps.Animation.DROP,*/
              sentiment: sentimentValue,
              postCount: postCount,
              zIndex: postCount,
              disableAutoPan: true
            });

            if (category != 'Generic') {
              var hotspotInfoWindowContent = '<div class="infowindow"><div class="hotspot-wrapper"><span class="hotspot-id">HOTSPOT ID: ' + hotspotId + '</span><div class="social-counters"><div class="twitter-line"><span class="social-icon"><img src="../../dist/images/category/'+category+'-Pin.png"></span>Posts count <span class="count-number twitter">' + categoryCount[Object.keys(categoryCount)[0]] + '</span> posts</div> <a class="view-hotspot-btn" hotspotId="'+hotspotId+'" redirectToValue="'+redirectToValue+'">View HOTSPOT</a></div></div>';
            }else {
              var hotspotInfoWindowContent = '<div class="infowindow"><div class="hotspot-wrapper"><span class="hotspot-id">HOTSPOT ID: ' + hotspotId + '</span><div class="social-counters"><div class="twitter-line"><span class="social-icon twitter"></span>Twitter <span class="count-number twitter">' + checkSocialCount(twitterCount) + '</span> posts</div><div class="instagram-line"><span class="social-icon instagram"></span>Instagram <span class="count-number twitter">' + checkSocialCount(instagramCount) + '</span> posts</div></div><a class="view-hotspot-btn" hotspotId="'+hotspotId+'" redirectToValue="'+redirectToValue+'">View HOTSPOT</a></div></div>';
            }

            var hotspotInfoWindow = new google.maps.InfoWindow({
              content: hotspotInfoWindowContent,
              type: 'hotspot'
            });

            infoWindowArray.push(hotspotInfoWindow);

            hotspotMarker.addListener('click', function() {
              closeAllInfoWindows();
              hotspotInfoWindow.open(map, hotspotMarker);
            });

            map.addListener('click', function() {
              hotspotInfoWindow.close();
            });

            if (circleName == 'circle')google.maps.event.addListener(circle, 'click', function(){hotspotInfoWindow.close();});
            else if (circleName == 'circleRecorded')google.maps.event.addListener(circleRecorded, 'click', function(){hotspotInfoWindow.close();});
            else if (circleName == 'circleEvent')google.maps.event.addListener(circleEvent, 'click', function(){hotspotInfoWindow.close();});
            else if (circleName == 'circleSavedLocation')google.maps.event.addListener(circleSavedLocation, 'click', function(){hotspotInfoWindow.close();});

            $rootScope.hotspotArray.unshift(hotspotMarker);
          });
        };

        function zoomToPostPin(LatLng, mapName) {
          NgMap.getMap(mapName).then(function(map) {
            if (!mapZoomed) {
              mapDefault = {
                center: map.getCenter(),
                zoom: map.getZoom()
              }
            }
            mapZoomed = true;
            map.setCenter({lat: LatLng[1], lng: LatLng[0]});
            map.setZoom(27);
          });
        };

        function zoomToMap(mapName) {
          if (mapZoomed == true) {
            NgMap.getMap(mapName).then(function(map) {
              map.setCenter(mapDefault.center);
              map.setZoom(mapDefault.zoom);
              mapZoomed = false;
            });
          }
        }

        function showPinOnMap(postID, mapName) {
          for (var marker in $rootScope.markersArray) {
            $rootScope.markersArray[marker].setZIndex(1);
            if (postID == $rootScope.markersArray[marker].id) {
              $rootScope.markersArray[marker].setAnimation(4);
              $rootScope.markersArray[marker].setZIndex(9999);
            }
          }
        }

        //Show/Hide Twiiter/Instagram pins on map
        function displayTwitterPins(ev, $scope) {
          $scope.checkTwitterPins != $scope.checkTwitterPins;
          if($scope.checkTwitterPins == false) {
              for(var pin in $rootScope.markersArray) {
                  if($rootScope.markersArray[pin].type == 'twitter') {
                      $rootScope.markersArray[pin].setVisible(false);
                  }
              }
          }
          else {
              for(var pin in $rootScope.markersArray) {
                  if($rootScope.markersArray[pin].type == 'twitter') {
                      $rootScope.markersArray[pin].setVisible(true);
                  }
              }
          }
        }

        function displayInstagramPins(ev, $scope) {
          $scope.checkInstagramPins != $scope.checkInstagramPins;
          if($scope.checkInstagramPins == false) {
              for(var pin in $rootScope.markersArray) {
                  if($rootScope.markersArray[pin].type == 'instagram') {
                      $rootScope.markersArray[pin].setVisible(false);
                  }
              }
          }
          else {
              for(var pin in $rootScope.markersArray) {
                  if($rootScope.markersArray[pin].type == 'instagram') {
                      $rootScope.markersArray[pin].setVisible(true);
                  }
              }
          }
        }

        function clearMarkers() {
          for(var marker in $rootScope.markersArray) {
            $rootScope.markersArray[marker].setMap(null);
          }
          $rootScope.markersArray = [];
        }

        function clearHotspots() {
          for(var hotspotMarker in $rootScope.hotspotArray) {
            $rootScope.hotspotArray[hotspotMarker].setMap(null);
          }
          $rootScope.hotspotArray = [];
        }

        function initHotspot(link_id, redirect_to, extra_params) {
            /** Open hotspot page.
             *      link_id - hotspot link_id.
             *      redirect_to - hotspot redirect_to parameter.
             *      extra_params - params that should be send to hotspot endpoint.
              */
          var redirectTo = redirect_to.split('&');

          //console.log(redirectTo);

          var indexOne = redirectTo.indexOf('all_categories=');
          var indexTwo = redirectTo.indexOf('sentiment_tag= ');
          
          if (indexOne > -1) {
            redirectTo.splice(indexOne, 1);
          }
          redirectTo.splice(indexTwo,1);

          var redirectToString = redirectTo.toString();
          redirectToString = redirectToString.replace(/\,/g,'&');

          redirectToString += '&username=' + $rootScope.userInfo.username + '&api_key=' + $rootScope.userInfo.api_key;

          var data = {
            redirect_to: redirectToString,
            link_id: link_id
          };

          var hotspotUrl = '/hotspot/' + link_id;

          // Extend hotspot link with extra params.
          if (typeof extra_params === 'object') {
            var extraString = "";
            for (var i in extra_params) {
                if (extraString.length > 0) {
                    extraString += "&";
                } else {
                    extraString += "?";
                }
                extraString += i + "=" + extra_params[i];
            }
            hotspotUrl += extraString;
          }

          $http({
            url: _apiUsers + 'api/permalink/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
            method: 'POST',
            data: JSON.stringify(data),
          }).then(function successCallback(response) {
              console.log(response);
              if(response.status == '201') {
                $location.url(hotspotUrl);
                //window.open(hotspotUrl, '_blank');
              }
            }, function errorCallback(response) {
              console.log(response);
              if (response.data.error == 'Record already exists') {
                $location.url(hotspotUrl);
                //window.open(hotspotUrl, '_blank');
              }
              else {
                //error code here
              }
          });
        }

    }]);
})();
