(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('StreamingService', ['$http', '_apiUsers', '$cookies', '$rootScope', function($http, _apiUsers, $cookies, $rootScope) {
      var service = {};

      service.localDateToString = localDateToString;
      service.getStreamingData = getStreamingData;
      service.getEventgData = getEventgData;
      service.checkSort = checkSort;
      service.checkSortByDate = checkSortByDate;
      service.checkSortBySentiment = checkSortByDate;
      service.checkSortBySentimentValue = checkSortBySentimentValue;
      service.checkSearchQuery = checkSearchQuery;
      service.checkSearchHotspotQuery = checkSearchHotspotQuery;
      service.checkCategory = checkCategory;
      service.chechPostType = chechPostType;
      service.getHotspotData = getHotspotData;
      service.getHotspotDataInit = getHotspotDataInit;
      service.checkPlatformPosts = checkPlatformPosts;
      service.getStreamDataInit = getStreamDataInit;
      service.stopStreamDataInit = stopStreamDataInit;
      service.getOpenStreamList = getOpenStreamList;
      service.getAnalyticsData = getAnalyticsData;
      service.getUserId = getUserId;
      service.getRecordedData = getRecordedData;
      service.checkTimestampInit = checkTimestampInit;
      service.checkRecordedTime = checkRecordedTime;
      service.updateCategory = updateCategory;

      return service;

        /**
         *  Convert local Date to string without timezone.
         * @param date - local Date object;
         * @return string;
         */
      function localDateToString(date) {
        if ("undefined" === typeof date || "undefined" === typeof date.getTimezoneOffset)
          return '';

        var offsetMiliseconds = date.getTimezoneOffset() * 60000,
          offsetDate = new Date(date.getTime() - offsetMiliseconds),
          dateString = offsetDate.toISOString();
        return dateString.replace("Z", "");
      }

      function checkSort(date, sentiment) {
        if (sentiment != undefined) {
          if (sentiment == '*' || sentiment == '') return '&order_by='+checkSortByDate(date)+'&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment);
          else return '&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment)+'&order_by='+checkSortByDate(date);
        }
        else {
          return '&order_by='+checkSortByDate(date)+'&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment);
        }
      }

      function checkSortByDate(date) {
        //oldest first
        if (date == undefined) return 'timestamp';
        else return date;
      }

      function checkSortBySentimentValue(sentiment) {
        if (sentiment == 'positive') return '-sentiment';
        else return 'sentiment';
      }

      function checkSortBySentiment(sentiment) {
        //newest first
        if (sentiment == undefined) return '';
        else return sentiment;
      }

      function checkSearchQuery(searchQuery) {
        if (searchQuery != '') {
          var searchResult = searchQuery;
          searchResult = searchResult.toString();
          searchResult = searchResult.replace(/,/g, "+");
          return searchResult;
        }
        else return '';
      }

      function checkSearchHotspotQuery(searchHotspotQuery) {
          var searchHotspotQueryVal = searchHotspotQuery;
          searchHotspotQueryVal = searchHotspotQueryVal.toString();
          searchHotspotQueryVal = searchHotspotQueryVal.replace(/,/g, "+");
          searchHotspotQueryVal = searchHotspotQueryVal.replace(/HOTSPOT/g, "");
          searchHotspotQueryVal = searchHotspotQueryVal.replace(/HOTSPOT+/g, "");
          //searchHotspotQueryVal = searchHotspotQueryVal.substring(8);
          return searchHotspotQueryVal;
      }

      function checkCategory(category) {
        if (category == '') return '';
        else {
          var categoryResult = category;
          categoryResult = categoryResult.toString();
          categoryResult = categoryResult.replace(/ /g, "");
          categoryResult = categoryResult.replace(/,/g, "+");
          return categoryResult;
        }
      }

      function chechPostType(postType) {
        if (postType != undefined) {
          return postType;
        }
        else {
          return '';
        }
      }

      function checkPlatformPosts(twitterPosts, instagramPosts) {
        if (twitterPosts == false && instagramPosts == false) return 'NaN';
        else if (twitterPosts == true && instagramPosts == false) return 'twitter';
        else if (twitterPosts == false && instagramPosts == true) return 'instagram';
        else if (twitterPosts == true && instagramPosts == true) return '';      
      }

      function checkRadiusUnit(radiusUnit) {
        if (radiusUnit == undefined) return 'mi';
        else return radiusUnit;
      }

      function timestampCheck(timestamp) {
        if (timestamp == undefined) {
          var timestampInit = $rootScope.liveStreamTimestamp.slice(0, $rootScope.liveStreamTimestamp.lastIndexOf("."));
          return timestampInit;
        }
        else return timestamp;
      }

      function checkTimestampInit(timestampInit, filterFrom) {
        if (filterFrom != undefined && filterFrom != '') {
          return filterFrom.toISOString();
        }
        else return timestampInit;
      }

      function checkRecordedTime(recordedTime, filterTo) {
        if (filterTo != undefined && filterTo != '') {
          return filterTo.toISOString();
        }
        else return recordedTime;
      }

      function getUserId(callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/user/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }

      function getStreamDataInit(callback, error) {
        var body = {};
        body = {
          "lng": $rootScope.circlePosition.lng(),
          "lat": $rootScope.circlePosition.lat(),
          "radius": $rootScope.radiusValue,
          "uom": $rootScope.radiusUnit,
          "platform": ["twitter"] //Platform which streaming 
        }
        $http({
          method: 'POST',
          url: _apiUsers + 'api/monitoring/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          data: body
        }).then(callback, error);
      }

      function checkStreamStartDate(timestamp) {
        var newTimestamp = timestamp;
        newTimestamp = newTimestamp.slice(0, newTimestamp.lastIndexOf("."));
        return newTimestamp;
      }


      function getStreamingData(timestamp, callback, error) {
        var uom = $rootScope.radiusUnit;
        if ('undefined' !== typeof($rootScope.dashboardCache)) {
            if ('undefined' !== typeof($rootScope.dashboardCache.uom)) {
                uom = $rootScope.dashboardCache.uom;
            }
        }
        $http({
          method: 'GET',
          url: _apiUsers + 'api/realtime/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key+'&lng='+$rootScope.circlePosition.lng()+'&lat='+$rootScope.circlePosition.lat()+'&radius='+$rootScope.radiusValue+'&uom='+ uom +'&timestamp__gte='+timestampCheck(timestamp)+'&stream_started='+checkStreamStartDate($rootScope.liveStreamTimestamp)
        }).then(callback, error);
      }

      function stopStreamDataInit(sessionId, callback, error) {
        $http({
          method: 'DELETE',
          url: _apiUsers + 'api/monitoring/'+sessionId+'/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }

      function getOpenStreamList(callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/monitoring/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }     

      function getEventgData(event, next, dateSort, sentimentSort, searchQuery, category, postType, twitterPosts, instagramPosts, filterFrom, filterTo, callback, error) {
        var url = _apiUsers + 'api/realtime/?format=json&username='
              + $rootScope.userInfo.username
              + '&api_key=' + $rootScope.userInfo.api_key
              + '&event=' + event
              + '&limit=9'
              + '&platform=' + checkPlatformPosts(twitterPosts, instagramPosts)
              + '&offset=' + next
              + checkSort(dateSort, sentimentSort)
              + '&query=' + checkSearchQuery(searchQuery)
              + '&all_categories=' + checkCategory(category)
              + chechPostType(postType);

        if (filterFrom.length > 0)
            url += '&timestamp_db__gte=' + filterFrom;
        if (filterTo.length > 0)
            url += '&timestamp_db__lte=' + filterTo;

        $http({
          method: 'GET',
          url: url
        }).then(callback, error);
      }

      function getHotspotData(hotspotId, callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + hotspotId + "/"
        }).then(callback, error);
      }

      function getHotspotDataInit(lat, lng, radiusValue, radiusUnit, next, dateSort, sentimentSort, searchQuery,
                                  category, postType, twitterPosts, instagramPosts, filterFrom, filterTo,
                                  callback, error) {

        var url = _apiUsers + 'api/realtime/?format=json&username=' + $rootScope.userInfo.username
                + '&api_key='+$rootScope.userInfo.api_key
                + '&no_hotspots=True&platform=' + checkPlatformPosts(twitterPosts, instagramPosts)
                + '&radius=' + radiusValue
                + '&uom=' + checkRadiusUnit(radiusUnit)
                + '&lat=' + lat
                + '&lng=' + lng
                + '&limit=9&offset='+ next
                + checkSort(dateSort,sentimentSort)
                + '&query=' + checkSearchHotspotQuery(searchQuery)
                + '&all_categories=' + checkCategory(category)
                + chechPostType(postType)
                + '&timestamp__gte=' + filterFrom
                + '&timestamp__lte=' + filterTo;
        $http({
          method: 'GET',
          url: url
        }).then(callback, error);
      }

      function getRecordedData(timestampInit, recordedTime, radius, uom, lat, lng, next, dateSort, sentimentSort, postMediaType, searchQuery, twitterPosts, instagramPosts, category, filterFrom, filterTo, callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/realtime/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key+'&lng='+lng+'&lat='+lat+'&radius='+radius+'&uom='+uom+'&timestamp__gte='+checkTimestampInit(timestampInit, filterFrom)+'&timestamp__lte='+checkRecordedTime(recordedTime, filterTo)+'&limit=9&platform='+checkPlatformPosts(twitterPosts, instagramPosts)+'&offset='+next+checkSort(dateSort,sentimentSort)+chechPostType(postMediaType)+'&query='+checkSearchQuery(searchQuery)+'&all_categories='+checkCategory(category)
        }).then(callback, error);
      }

      function getAnalyticsData(event, searchQuery, postType, args, callback, error) {

        function clearField (name) {
            if (value) {
                return "&" + name + "=" + value;
            } else {
                return '';
            }
        }

        var url = _apiUsers
            + 'api/analytics/?format=json&username=' + $rootScope.userInfo.username
            + '&api_key=' + $rootScope.userInfo.api_key
            + chechPostType(postType);

        if (typeof event !== 'undefined')
            url += '&event=' + event;

        if (typeof searchQuery !== 'undefined')
            url += '&query=' + checkSearchQuery(searchQuery);

        for (var i in args) {
            if (i != "callback")
                url += "&" + i + "=" + args[i];
        }

        $http({
            method: 'GET',
            url: url
        }).then(callback, error);
      }

      function updateCategory(postID, category, callback, error) {
          var body = {'category': category};
          $http({
              method: 'PATCH',
              data: JSON.stringify(body),
              url: _apiUsers + 'api/realtime/' + postID
              + '?format=json&username=' + $rootScope.userInfo.username
              + '&api_key=' + $rootScope.userInfo.api_key
          }).then(callback, error);
      }

    }]);
})();