(function () {
    'use strict';

    angular.module("exampleApp")
        .factory('analyticsService', [
            '$rootScope', 'tabService', 'StreamingService',
            function ($rootScope, tabService, StreamingService) {

                var service = {};

                service.getAnalyticsData = function (scp, args) {
                    /* Params:
                        @scp - controller scope;
                        @args - additional arguments for data query;
                     */
                    // Add category extra filter.
                    if (typeof scp[scp.examineTabSettings.categoryClassifierFieldName] === 'object')
                        var categoryClassifierModel = scp[scp.examineTabSettings.categoryClassifierFieldName];
                    else
                        var categoryClassifierModel = [];
                    if (categoryClassifierModel.length > 0) {
                        // Remove spaces from Category names.
                        var categories = categoryClassifierModel.slice(0);
                        for (var i in categories) {
                            categories[i] = categories[i].replace(" ","");
                        }
                        args.all_categories = categories.toString().replace(",",";");
                        delete args.category;
                    }

                    // Add extra filters, defined in scope to search;
                    if (typeof scp.analyticsExtraFiltres !== 'undefined') {
                        for (var i in scp.analyticsExtraFiltres) {
                            args[i] = scp.analyticsExtraFiltres[i];
                        }
                    }

                    var success = function(response) {
                        args.callback(response);
                    };
                    var error = function(response) {
                        args.callback(response);
                    };

                    if (typeof scp[scp.examineTabSettings.searchBarFieldName] === 'object') {
                        var searchModel = scp[scp.examineTabSettings.searchBarFieldName].slice();
                        // remove HOTSPOT keyword.
                        for (var i = 0; i < searchModel.length; i++) {
                            if (searchModel[i] == "HOTSPOT")
                                searchModel.splice(i, 1);
                        }
                    } else {
                        var searchModel = [];
                    }

                    // Rewrite datetime filters.
                    if (typeof scp[scp.examineTabSettings.dateTimeFromFieldName] !== "undefined") {
                        var fromDate = scp[scp.examineTabSettings.dateTimeFromFieldName];

                        if (! fromDate || fromDate.length == 0)
                            return;

                        if (args.timestamp__gte && fromDate < args.timestamp__gte) {
                            return;
                        }
                        try {
                            args.timestamp__gte = fromDate.toISOString().replace("Z", "");
                        } catch(e) {
                            console.log('Cannot convert ' + scp.examineTabSettings.dateTimeFromFieldName + " to ISO string");
                        }
                    }
                    if (typeof scp[scp.examineTabSettings.dateTimeToFieldName] !== "undefined") {
                        var toDate = scp[scp.examineTabSettings.dateTimeToFieldName];

                        if (! toDate || toDate.length == 0)
                            return;

                        if (args.timestamp__lte && toDate > args.timestamp__lte) {
                            return;
                        }
                        try {
                            args.timestamp__lte = toDate.toISOString().replace("Z", "");;
                        } catch(e) {
                            console.log('Cannot convert ' + scp.examineTabSettings.dateTimeToFieldName + " to ISO string");
                        }
                    }

                    StreamingService.getAnalyticsData(scp.eventName, searchModel, scp.postTypeSelect,
                        args, success, error);
                }

                service.addHistogramCanvas = function (wrap) {
                    /* Params
                        @wrap - histogram container <div> element;
                     */

                    // Get prorepties.
                    var canvasID = wrap.attr("id").replace("Wrap", "");
                    var height = 300;

                    // Calculate width.
                    var width = $(window).width() * 0.7;

                    // Build canvas.
                    var canvas = $("<canvas>").attr({id: canvasID});
                    canvas[0].width = width;
                    canvas[0].height = height;

                    // Add canvas to element.
                    canvas.appendTo(wrap);

                    return canvas;
                }

                service.drawStackedHistogram = function(scp, ctx, chartData, suggestedMin) {
                    var defaultOptions = {
                        legend : {
                            display: false,
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                                yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    suggestedMax: 10,
                                    suggestedMin: suggestedMin
                                },
                                stacked: true,
                            }]
                        },
                        legendCallback: function (chart) {
                            var pieData = scp.pieData;
                            var text = [];
                            text.push('<ul class="' + chart.id + '-legend">');
                            for (var i = 0; i < chart.data.datasets.length; i++) {
                                text.push('<li><span style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
                                if (chart.data.datasets[i].label) {
                                    text.push(chart.data.datasets[i].label);
                                    try {
                                        text.push('<span class="legend-count">' + pieData.datasets[0].data[i] + "</span>");
                                    } catch (e) {
                                        text.push('<span class="legend-count">unknown</span>');
                                    }
                                }
                                text.push('</li>');
                            }
                            text.push('</ul>');
                            return text.join('');
                        }
                    };

                    return new Chart(ctx, {
                        type: 'bar',
                        data: chartData,
                        options: defaultOptions
                    });
                }

                service.drawPie = function (scp, pieData) {
                    /*	Draw pieChart with categories distribution
                        Params:
                            @scp - scope;
                            @pieData - pie data;
                     */
                    var _s = scp.examineTabSettings;
                    // Add pie.
                    var ctx = $("#categoryPie")[0].getContext('2d');
                    if (! _s.categoryPie) {
                        _s.categoryPie = new Chart(ctx, {
                            type: 'pie',
                            data: pieData,
                            options: {
                                legend : {
                                    display: false
                                }
                            }
                        });
                    } else {
                        _s.categoryPie.data = pieData;
                        _s.categoryPie.update();
                    }
                }

                service.showInfo = function (scp, status, message) {
                    /*  Show info, loader or data, depend on state.
                        @param scp - controller scope;
                        @param status - same as examineTabSettings.infoStatus;
                        @param message -text that should be shown.
                     */
                    var s = scp.examineTabSettings;

                    if ("number" !== typeof status || status < -1 || status > 1) {
                        console.group('analytics.service.showInfo');
                        console.log("Status is incorrect!");
                        console.groupEnd();
                        return;
                    }

                    if ("undefined" !== typeof message && message.length > 0)
                        $('.examine-show-wrap .pre-info-text').text(message);

                    if (s.infoStatus == -1) {
                        $(".examine-show-wrap .pre-info-container").addClass('hidden');
                    } else if (s.infoStatus == 0) {
                        if (status == 1)
                            return;
                        $(".examine-show-wrap .info-wrap").addClass('hidden');
                    } else {
                        $(".examine-show-wrap .preloader-container").addClass('hidden');
                    }

                    if (status == -1) {
                        $(".examine-show-wrap .pre-info-container").removeClass('hidden');
                    } else if (status == 0) {
                        $(".examine-show-wrap .info-wrap").removeClass('hidden');
                    } else {
                        $(".examine-show-wrap .preloader-container").removeClass('hidden');
                    }

                    s.infoStatus = status;
                }

                service.histogramController = function (attrs) {
                    var scp = attrs.scope;

                    if ("undefined" === typeof attrs.legend)
                        attrs.legend = undefined;

                    if ("undefined" === typeof attrs.suggestedMin)
                        attrs.suggestedMin = 0;

                    if (! scp[attrs.title]) {
                        var ctx = service.addHistogramCanvas(attrs.element)[0].getContext('2d');
                        scp[attrs.title] = service.drawStackedHistogram(attrs.scope, ctx, attrs.data, attrs.suggestedMin);
                    } else {
                        scp[attrs.title].data = attrs.data;
                        scp[attrs.title].options.scales.yAxes[0].ticks.suggestedMin = attrs.suggestedMin;
                        scp[attrs.title].update();
                    }

                    if ("undefined" !== typeof attrs.legend)
                        $("#histogramLegend").html(scp[attrs.title].generateLegend());
                }

                service.examineTab = function (scp, settings) {
                    /*  Setup examine tab on current controller.
                        @param scp - controller scope;
                        @param settings - extra settings;
                     */
                    var self = this;
                    
                    // Add examineTab settings to scope;
                    scp.examineTabSettings = {
                        postsHistogram: false,
                        categoryHistogram: false,
                        categoryPie: false,
                        posts: 0,
                        pieData: {},
                        searchBarFieldName: undefined,
                        categoryClassifierFieldName: undefined,
                        dateTimeFromFieldName: undefined,
                        dateTimetoFieldName: undefined,
                        highestBarCount: 0,

                        // What shown now:
                        //  -1 - info
                        //   0 - data
                        //   1 - loader
                        infoStatus: -1
                    }

                    // Override default settings.
                    if (typeof settings === 'object')
                        for (var i in settings) {
                            scp.examineTabSettings[i] = settings[i];
                        }

                    // Add resize event.
                    var w = $(window);
                    w.on('resize', function () {
                        var width = w.width();
                        $('#categoryPie').css('width', width * 0.20 + "px");
                        $('#postsHistogram').css('width', width * 0.69);
                        $('#categoryHistogram').css('width', width * 0.69);
                    });

                    scp.callExamineView = function(customArgs) {
                        /*  Change tab on click 'Examine'.
                            @param customArgs - arguments that could be passed to custom functions.
                         */
                        tabService.examineView();

                        // Execute user defined onCallExamineView function. Function could return boolean
                        // value. If value will be false, callExamineView would be terminated.
                        if ('function' === typeof scp.examineTabSettings.onCallExamineView) {
                            var r = scp.examineTabSettings.onCallExamineView(customArgs);
                            if ('boolean' === typeof r)
                                if (! r)
                                    return;
                        }

                        service.showInfo(scp, 1);

                        service.getAnalyticsData(scp, {
                            callback: function (response) {

                                // Check for categories data in response. If no, show error.
                                if ("undefined" == response.data.categories) {
                                    console.log("Bad response of analytics data.");
                                    // TODO: Show error.
                                }

                                // If there is no data just wait for new portion for live.
                                if (response.data.categories.length == 0) {
                                    if ("undefined" !== typeof scp.streamDataArray)
                                        service.showInfo(scp, 1);
                                    else
                                        service.showInfo(scp, -1, "No posts here.");
                                    return;
                                }

                                service.showInfo(scp, 0);

                                /*
                                    Prepare data for histogram, pie and label.
                                 */
                                var histogramData = {
                                    posts: [],
                                    classified_posts: []
                                }
                                scp.pieData = {
                                    datasets: [{
                                        data: [],
                                        backgroundColor: [],
                                    }],
                                    labels: []
                                };

                                // Colors used for categories.
                                var colors = ["#FFCC00", "#FF9900", "#FF6600", "#FF3300", "#FFCC33",
                                    "#FFCC66", "#FF9966", "#FF6633", "#CC3300"];

                                // Convert labels.
                                var labels = response.data.histograms.posts.labels,
                                    firstLabel = labels[0],
                                    lastLabel = labels[labels.length - 1],
                                    sliceStartIndex = 11;

                                if (firstLabel.slice(5,10) !== lastLabel.slice(5,10))
                                    sliceStartIndex = 5;
                                else if (firstLabel.slice(0,4) !== lastLabel.slice(0,4))
                                    sliceStartIndex = 0;

                                if (sliceStartIndex > 0) {
                                    for (var i = 0; i < labels.length; i++) {
                                        labels[i] = labels[i].slice(sliceStartIndex, firstLabel.length);
                                    }
                                }

                                // Fill dataSets for all histograms, pieChart and legend.
                                var categories = response.data.categories;

                                for (var i=0; i < categories.length; i++) {

                                    var categoryName = categories[i].name;

                                    var color = "#6b6b6b";
                                    if (categoryName.toLowerCase() !== "generic")
                                        color = colors.pop();

                                    for (var k in histogramData) {
                                        if ("undefined" === typeof histogramData[k].labels) {
                                            histogramData[k] = {
                                                labels: labels,
                                                datasets: [],
                                            }
                                        }

                                        histogramData[k].datasets.push({
                                            borderWidth: 0,
                                            data: response.data.histograms[k].data[i],
                                            backgroundColor: color,
                                            label: categoryName
                                        });
                                    }

                                    scp.pieData.datasets[0].data.push(categories[i].count);
                                    scp.pieData.datasets[0].backgroundColor.push(color);
                                    scp.pieData.labels.push(categoryName);
                                }

                                service.histogramController({
                                    scope: scp,
                                    title: "postsHistogram",
                                    element: $("#postsHistogramWrap"),
                                    data: histogramData.posts,
                                    legend: true
                                });

                                service.drawPie(scp, scp.pieData);

                                service.histogramController({
                                    scope: scp,
                                    title: "categoryHistogram",
                                    element: $("#categoryHistogramWrap"),
                                    data: histogramData.classified_posts,
                                    suggestedMin: response.data.max_posts/50
                                });


                                $("#totalUsers").text(response.data.user_count);
                                $("#totalPosts").text(response.data.post_count);
                            }
                        });
                    }

                    return self;
                }

                service.dateToString = function (date, utc) {
                    /*  Convert date to string.
                     */
                    function _format(d) {
                        if (d < 10) {
                            return "0" + d.toString();
                        }
                        return d;
                    }

                    if (typeof utc === 'undefined' || typeof utc !== 'boolean')
                        utc = true;

                    if (utc) {
                        var string = date.getUTCFullYear() + "-";
                        string += _format(date.getUTCMonth() + 1) + "-";
                        string += _format(date.getUTCDate());
                        string += "T";
                        string += _format(date.getUTCHours()) + ":";
                        string += _format(date.getUTCMinutes()) + ":";
                        string += _format(date.getUTCSeconds());
                    } else {
                        var string = date.getFullYear() + "-";
                        string += _format(date.getMonth() + 1) + "-";
                        string += _format(date.getDate());
                        string += "T";
                        string += _format(date.getHours()) + ":";
                        string += _format(date.getMinutes()) + ":";
                        string += _format(date.getSeconds());
                    }
                    return string;
                }

                return service;
        }]);
})();
