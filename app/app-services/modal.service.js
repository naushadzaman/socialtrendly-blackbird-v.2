(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('modalService', ['$mdDialog', 'ListService', '$rootScope', function($mdDialog, ListService, $rootScope) {
      var service = {};

      service.deleteSavedModal = deleteSavedModal;
      service.datePickerModal = datePickerModal;
      service.saveLocationModal = saveLocationModal;
      service.saveLocationModalTwo = saveLocationModalTwo;
      service.deleteListItem = deleteListItem;
      service.deleteAlertModal = deleteAlertModal;
      service.successCreateAlert = successCreateAlert;
      service.stopStreamDashboardModal = stopStreamDashboardModal;
      service.createListItem = createListItem;
      service.createListSuccess = createListSuccess;
      service.deletePostFromList = deletePostFromList;
      service.saveLocationModalSuccess = saveLocationModalSuccess;
      service.monitoringError = monitoringError;
      service.startMonitoringModal = startMonitoringModal;
      service.stopMonitoringModal = stopMonitoringModal;
      service.streamAlreadyStart = streamAlreadyStart;
      service.annotatePost = annotatePost;
      service.callCreateNewUserList = callCreateNewUserList;
      service.showFullImage = showFullImage;
      service.openInNewTab = openInNewTab;
      service.addInstagramPopUp = addInstagramPopUp;
      service.error = error;

      return service;

      //
      // LISTS MODALS
      //

      function createListItem(ev, $scope) {
            $rootScope.listTitle = '';
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/create-list.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                  if ($rootScope.listTitle.length > 0) {
                    $rootScope.$broadcast('createListTitle', {
                      listTitie: $rootScope.listTitle
                    });
                  }
                  else {
                    $('#createListInput').addClass('error-input');
                  }
                }
            }, function() {
                
            });
        };

      function callCreateNewUserList(ev, $scope) {
            $rootScope.listTitle = '';
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/create-list.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                  if ($rootScope.listTitle.length > 0) {
                    var body = {
                      name: $rootScope.listTitle,
                      objects: ''
                    }
                    ListService.createNewList(body,
                      function(response) {
                          console.log(response);
                          if (response.status == 201) {
                            createListSuccess(' ', $scope);
                            ListService.getListData();
                          }
                          else {
                            createListError(' ', $scope); 
                          }
                      },
                      function(error) {
                          console.log(error);
                          createListError(' ', $scope);
                      }
                    )
                  }
                  else {
                    $('#createListInput').addClass('error-input');
                  }
                }
            }, function() {
                
            });
        };

      function createListSuccess(ev, $scope) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/create-list-success.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                }
            }, function() {
                
            });
        };

      function createListError(ev, $scope) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/create-list-error.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                }
            }, function() {
                
            });
        };

        function annotatePost(ev, $scope) {
          $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/annotate-post.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
          .then(function(answer) {
            if(answer == 'true') {
              annotatePostDescription();
            }
          }, function() {

          });
        };

        function annotatePostDescription(ev, $scope) {
          $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/annotate-post-description.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
          .then(function(answer) {
            if(answer == 'true') {
            }
          }, function() {

          });
        };

      function deleteListItem(ev, $scope, listId) {
          $mdDialog.show({
              controller: DialogController,
              templateUrl: '../views/modals/list-delete.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
              })
          .then(function(answer) {
            if (answer == 'true') {
              $rootScope.$broadcast('deleteList', {
                listId: listId
              });
            }
          }, function() {   

          });
      };

      function deletePostFromList(ev, $scope, postId) {
          $mdDialog.show({
              controller: DialogController,
              templateUrl: '../views/modals/post-delete.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
              })
          .then(function(answer) {
            if (answer == 'true') {
              $rootScope.$broadcast('deletePostFromList', {
                postId: postId
              });
            }
          }, function() {   

          });
      };

      //
      // SAVED LOCATION MODALS
      //

      function deleteSavedModal(ev, $scope, locationId) {
          $mdDialog.show({
              controller: DialogController,
              templateUrl: '../views/modals/delete-saved-location.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose:true,
              fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
              })
          .then(function(answer) {
            if (answer == 'true') {
              $rootScope.$broadcast('deleteSavedLocation', {
                locationId: locationId
              });
            }
          }, function() {   

          });
      };

      //
      // DASHBOARD MODALS
      //

      function datePickerModal(ev, $scope) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../../views/modals/date-picker.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
              })
        .then(function(answer) {
          window.location.href = "mailto:info@socialtrendly.com?subject=Request%20for%20account%20upgrade";
        }, function() {

        });
      };

      function saveLocationModal(ev, $scope) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/save-location-first.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
              })
        .then(function(answer) {
          if (answer == 'yes') {
            $rootScope.$broadcast('stopStreamData', {
              data: true
            });
            saveLocationModalTwo(ev, $scope);
          }
        }, function() {

        });
      };

      function saveLocationModalTwo(ev, $scope) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/save-location-second.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                  $rootScope.$broadcast('saveLocation', {
                    locationTitle: $rootScope.saveLocationTitle
                  });
                  $rootScope.saveLocationTitle = '';
                }
            }, function() {
                
            });
        };

        function saveLocationModalSuccess(ev, $scope) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: '../views/modals/save-location-success.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true
              })
            .then(function(answer) {
                if(answer == 'true') {
                }
            }, function() {
                
            });
        };

      function stopStreamDashboardModal(ev, $scope) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/stop-stream-dashboard.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
        .then(function(answer) {
            if(answer == 'true') {
              $rootScope.$broadcast('stopStreamData', {
                data: 'true'
              });
            }
        }, function() {
            
        });
      };


      function monitoringError(ev, $scope) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/stream-monitoring-error.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
        .then(function(answer) {
            if(answer == 'true') {
            }
        }, function() {
            
        });
      };

      function startMonitoringModal(ev, $scope, locationId, locationTitle) {
        $rootScope.savedLocationTitle = locationTitle;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/start-stream-monitoring.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
        .then(function(answer) {
            if(answer == 'true') {
              $rootScope.savedLocationTitle = '';
              $rootScope.$broadcast('startMonitoringModal', {
                locationId: locationId
              });
            }
        }, function() {
            
        });
      };


      function stopMonitoringModal(ev, $scope, locationId, locationTitle) {
        $rootScope.savedLocationTitle = locationTitle;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/stop-stream-monitoring.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
        .then(function(answer) {
            if(answer == 'true') {
              $rootScope.savedLocationTitle = '';
              $rootScope.$broadcast('stopMonitoringModal', {
                locationId: locationId
              });
            }
        }, function() {
            
        });
      };

      function streamAlreadyStart(ev, $scope, locationId) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '../views/modals/stream-already-started.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
        .then(function(answer) {
            if(answer == 'true') {
              $rootScope.$broadcast('streamAlreadyStart', {
                locationId: locationId
              });
            }
            if (answer == 'false') {
              window.location.href = "mailto:info@socialtrendly.com?subject=Request%20for%20account%20upgrade";
            }
        }, function() {
            
        });
      };

      //
      // ALERT MODALS
      //

      function deleteAlertModal(ev, $scope, callback) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/delete-alert.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        }).then(function (answer) {
          if (answer === 'true') {
              callback();
          }
        }, function() {});
      }


      function successCreateAlert(ev, $scope) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/create-success-alert.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(answer) {
          if(answer == 'true') {
            alert("alert deleted");
          }
        }, function() {

        });
      }; 

      function showFullImage(ev, $scope, image, post) {
        $rootScope.showFullImage = {
          image: image,
          post: post
        };
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/show-full-image.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(answer) {
          if(answer == 'true') {
          }
        }, function() {

        });
      };

      function openInNewTab(ev, $scope, page) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/open-in-new-tab.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(answer) {
          if(answer == 'true') {
            window.open(page, '_blank');
          }
          else {
            $rootScope.$broadcast('stopStreaAndOpenNewTab', {
              page: page
            });
          }
        }, function() {

        });
      }; 

      function addInstagramPopUp(ev, $scope) {
        $rootScope.instAccount.username = '';
        $rootScope.instAccount.pass = '';
        $mdDialog.show({
          controller: DialogController,
          templateUrl: '../views/modals/add-instagram.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(answer) {
          if(answer == 'true') {
            if ($rootScope.instAccount.username != '' && $rootScope.instAccount.pass != '') {
              alert("zalupa");
            }
          }
        }, function() {

        });
      };

      function error(ev, errorMsg) {
        $mdDialog.show({
            controller: DialogController,
            template:
                '<div class="modal-dialog">' +
                '   <div class="modal-content">' +
                '       <div class="modal-body">' +
                '           <div class="body-text">' + errorMsg + '</div>' +
                '       </div>' +
                '       <div class="modal-footer">' +
                '           <button ng-click="cancel()" type="button" class="btn modal-yellow-button">Ok</button>' +
                '       </div>' +
                '   </div>' +
                '</div>',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        }).then(function(answer) {}, function() {});
      }

      //
      // MODALS MAIN CONTROLLER
      //

      function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
          $mdDialog.hide();
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
          $mdDialog.hide(answer);
        };
      }
      
    }]);
})();
