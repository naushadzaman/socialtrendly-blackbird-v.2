(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('AuthenticationService', ['$http', '_apiUsers', '$cookies', '$rootScope', function($http, _apiUsers, $cookies, $rootScope) {
      var service = {};

      service.Login = Login;
      service.Registration = Registration;
      service.SetCredentials = SetCredentials;
      service.RemoveCredentials = RemoveCredentials;
      service.checkLoginedUsers = checkLoginedUsers;

      return service;

      function Login(user, callback, error) {
        $http({
          method: 'POST',
          url: _apiUsers + 'api/user/login/',
          data: JSON.stringify(user)
        }).then(callback, error);
      }

      function Registration(registrationUser, callback, error) {
        $http({
          method: 'POST',
          url: _apiUsers + 'api/user/',
          data: JSON.stringify(registrationUser)
        }).then(callback, error);
      }

      function SetCredentials(username, api_key) {
        // $rootScope.globals = {
        //   currentUser: {
        //     userid: userid,
        //     authdata: authdata,
        //     role: role
        //   }
        //}
        $rootScope.userInfo = {
          username: username,
          api_key: api_key
        }
        //console.log($rootScope.userInfo);

        var cookieExp = new Date();
        cookieExp.setMinutes(cookieExp.getMinutes() + 2880);
        $cookies.putObject('userInfo', $rootScope.userInfo, { expires: cookieExp });
      }

      function RemoveCredentials() {
        $rootScope.userInfo = {};
        $cookies.remove('userInfo');
        $http.defaults.headers.common['Authorization'] = 'Basic ';
      }

      // redirect to login page if not logged in and trying to access a restricted page
      function checkLoginedUsers(location) {
        var restrictedPage = $.inArray(location, ['/login', '/registration', '/approval']) === -1;
        var loggedIn = $rootScope.userInfo.api_key;
        if (restrictedPage && !loggedIn) {
          return false
        } else {
          return true
        }
      }

      // redirect to dashboard if user
      function checkUserRoles() {
        var adminRestrPages = [''];
      }

    }]);

})();
