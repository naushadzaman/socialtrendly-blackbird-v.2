(function () {
  'use strict';

  angular.module("exampleApp")
    .factory('SaveLocationService', ['$http', '_apiUsers', '$cookies', '$rootScope', function($http, _apiUsers, $cookies, $rootScope) {
      var service = {};

      service.saveLocation = saveLocation;
      service.getSavedLocationData = getSavedLocationData;
      service.deleteSavedLocation = deleteSavedLocation;
      service.checkSortList = checkSortList;
      service.startMonitoring = startMonitoring;
      service.stopMonitoring = stopMonitoring;
      service.getSavedLocation = getSavedLocation;
      service.checkSort = checkSort;
      service.checkSortByDate = checkSortByDate;
      service.checkSortBySentimentValue = checkSortBySentimentValue;
      service.checkSortBySentiment = checkSortBySentiment;
      service.checkSearchQuery = checkSearchQuery;
      service.checkCategory = checkCategory;
      service.chechPostType = chechPostType;
      service.checkPlatformPosts = checkPlatformPosts;
      service.checkTimestampFilterFrom = checkTimestampFilterFrom;
      service.checkTimestampFilterTo = checkTimestampFilterTo;

      return service;

      function checkSortList(sort, page) {
        if (page == 'dashboard') {
          if (sort == undefined) return '&order_by=-is_active&order_by=-updated';
          else return '&order_by='+sort;
        }
        else {
         if (sort == undefined) return '&order_by=is_active&order_by=-updated';
         else return '&order_by='+sort;
       }
     }

      function checkSort(date, sentiment) {
        if (sentiment != undefined) {
          if (sentiment == '*' || sentiment == '') return '&order_by='+checkSortByDate(date)+'&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment);
          else return '&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment)+'&order_by='+checkSortByDate(date);
        }
        else {
          return '&order_by='+checkSortByDate(date)+'&sentiment_tag='+checkSortBySentiment(sentiment)+'&order_by='+checkSortBySentimentValue(sentiment);
        }
      }

      function checkSortByDate(date) {
        //oldest first
        if (date == undefined) return 'timestamp';
        else return date;
      }

      function checkSortBySentimentValue(sentiment) {
        if (sentiment == 'positive') return '-sentiment';
        else return 'sentiment';
      }

      function checkSortBySentiment(sentiment) {
        //newest first
        if (sentiment == undefined) return '';
        else return sentiment;
      }

      function checkSearchQuery(searchQuery) {
        if (searchQuery != '') {
          var searchResult = searchQuery;
          searchResult = searchResult.toString();
          searchResult = searchResult.replace(/,/g, "+");
          return searchResult;
        }
        else return '';
      }

      function checkCategory(category) {
        if (category == '') return '';
        else {
          var categoryResult = category;
          categoryResult = categoryResult.toString();
          categoryResult = categoryResult.replace(/ /g, "");
          categoryResult = categoryResult.replace(/,/g, "+");
          return categoryResult;
        }
      }

      function chechPostType(postType) {
        if (postType != undefined) {
          return postType;
        }
        else {
          return '';
        }
      }

      function checkPlatformPosts(twitterPosts, instagramPosts) {
        if (twitterPosts == false && instagramPosts == false) return 'NaN';
        else if (twitterPosts == true && instagramPosts == false) return 'twitter';
        else if (twitterPosts == false && instagramPosts == true) return 'instagram';
        else if (twitterPosts == true && instagramPosts == true) return '';      
      }

      function checkTimestampFilterFrom(savedLocationCreated, filterFrom) {
        if (filterFrom != undefined && filterFrom != '') {
          return '&timestamp__gte='+filterFrom.toISOString();
        }
        else return '';
      }

      function checkTimestampFilterTo(savedLocationCreated, filterTo) {
        if (filterTo != undefined && filterTo != '') {
          return '&timestamp__lte='+filterTo.toISOString();
        }
        else return '';
      }

      function saveLocation(body, callback, error) {
        $http({
          method: 'POST',
          url: _apiUsers + 'api/saved_location/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          data: body
        }).then(callback, error);
      }

      function getSavedLocationData(sort, page, callback, error) {
        $http({
          method: 'GET',
          url: _apiUsers + 'api/saved_location/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key+checkSortList(sort, page)+'&limit=100&order_by=is_active'
        }).then(callback, error);
      }

      function deleteSavedLocation(locationId, callback, error) {
        $http({
          method: 'DELETE',
          url: _apiUsers + 'api/saved_location/'+locationId+'/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key
        }).then(callback, error);
      }

      function startMonitoring(locationId, callback, error) {
        $http({
          method: 'PATCH',          
          url: _apiUsers + 'api/saved_location/'+locationId+'/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            "is_active": true,
            "platform": ["twitter"]
          }
        }).then(callback, error);
      }

      function stopMonitoring(locationId, callback, error) {
        /*var locationID = locationId;
        locationID = locationID.slice(11, locationID.length);*/
        $http({
          method: 'PATCH',          
          url: _apiUsers + 'api/saved_location/'+locationId+'/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key,
          headers: {
            'Content-Type': 'application/json'
          },
          data: {"is_active": false}
        }).then(callback, error);
      }

      function getSavedLocation(locationId, next, dateSort, sentimentSort, postType, twitterPosts, instagramPosts, searchQuery, category, saveLocationDateCreated, filterFrom, filterTo, callback, error) {
        $http({
          method: 'GET',          
          url: _apiUsers + 'api/saved_location/'+locationId+'/?format=json&username='+$rootScope.userInfo.username+'&api_key='+$rootScope.userInfo.api_key+'&limit=9&platform='+checkPlatformPosts(twitterPosts, instagramPosts)+'&offset='+next+checkSort(dateSort,sentimentSort)+chechPostType(postType)+'&query='+checkSearchQuery(searchQuery)+'&all_categories='+checkCategory(category)+checkTimestampFilterFrom(saveLocationDateCreated, filterFrom)+checkTimestampFilterTo(saveLocationDateCreated, filterTo)
        }).then(callback, error);
      }
      

    }]);
})();



