(function () {
    'use strict';
    function serviceInit($rootScope, $http, _apiUsers) {
        var service = {};
        /**
         * Prepare URL for alert endpoint.
         * @param alertID - alert ID. Can be undefined for alert list requests.
         * @return {string}
         */
        function url(alertID) {
            if (typeof alertID !== "undefined") {
                alertID += "/";
            } else {
                alertID = '';
            }
            return _apiUsers + 'api/alert/' + alertID
                + '?format=json&username=' + $rootScope.userInfo.username
                + '&api_key=' + $rootScope.userInfo.api_key;
        }
        /**
         * Get active user alerts.
         * @param callback - success callback function.
         * @param error - error callback function.
         */
        service.get = function(callback, error) {
            $http({
                method: 'GET',
                url: url()
            }).then(callback, error);
        };
        /**
         * Save new alert.
         * @param data - data the need to be send via POST.
         * @param callback - success callback function.
         * @param error - error callback function.
         */
        service.post = function (data, callback, error) {
            $http({
                method: 'POST',
                url: url(),
                data: data
            }).then(callback, error);
        };
        /**
         * Delete alert.
         * @param id - alert id.
         * @param callback - success callback function.
         * @param error - error callback function.
         */
        service.delete = function (id, callback, error) {
            $http({
                method: 'DELETE',
                url: url(id)
            }).then(callback, error);
        };
        return service;
    }
    angular.module("exampleApp")
        .factory('AlertService', ['$rootScope', '$http', '_apiUsers', serviceInit]);
})();