(function () {
	'use strict';

	angular.module("exampleApp")
	.filter('formatDateEvent', function($filter){
    return function(date) {
            return $filter('date')(date, 'MM/dd/yyyy') ;
        }
  })
  .filter('formatTimeEvent', function($filter){
    return function(date){
    	return $filter('date')(date, 'h:mm a');
    }
  })
  .filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase.length > 0) {
        for (var i in phrase) {
        	if (phrase[i] != 'AND' && phrase[i] != 'OR') {
        		text = text.replace(new RegExp('('+phrase[i]+')', 'gi'), '<span class="highlighted">$1</span>');
        	}
        } 
        return $sce.trustAsHtml(text);
      }
      else {return text};
    }
  })
	.controller("eventCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', 'mapService', 'StreamingService','tabService', 'modalService', 'ListService', 'analyticsService', '$timeout', '$sanitize', function($rootScope, $scope, $http, $location, $stateParams, mapService, StreamingService, tabService, modalService, ListService, analyticsService, $timeout, $sanitize) {
		$scope.eventName = $stateParams.eventName;

		$scope.eventErrorMsg = false;
		$scope.eventPreloader = true;
		$scope.noItemsMsg = false;

		$scope.leftBtn = true;
		$scope.rightBtn = false;

		$scope.nextValue = 0;
		$scope.progressBarValue = 0;

		$rootScope.markersArray = [];

		$scope.checkTwitterPins = true;
		$scope.checkInstagramPins = false;

		$scope.socialNewsPopup = false;

		$scope.nextValueCheck = '';

		$scope.paginationPagesArray = [];

		$scope.paginationPageNum = 10;

		$scope.eventMarkersArray = [];

		$scope.paginationShow = false;

		$scope.currentPageNum = 1;

		$scope.timestampDisplayField = 'timestamp_db';

		$scope.categories = ["Violence", "Shooting", "ActiveShooter", "Terrorism", "Disaster", "Generic"];

		// Initialize examine tab on current controller;
        $scope.examineHidePreInfo = true;
        $scope.examineHideInfo = false;

		analyticsService.examineTab($scope, {
			searchBarFieldName: 'searchBarEventModel',
            categoryClassifierFieldName: 'searchCategoryEventModel',
            dateTimeFromFieldName: 'eventDateTimeFromObj',
            dateTimeToFieldName: 'eventDateTimeToObj'
		});

		var postCount;

		var isMapInited = false;
		var mapDrawCheck = true;

		/*(function callMapInit() {
			mapService.mapInitEvent();
		})();*/

		//
		// SELECTIZE INIT
		//

		$scope.searchBarEventModel = [];

		$scope.optionsSearchBarEvent = [
		];

		$scope.searchCategoryEventModel = [];

		$scope.optionsCategoryBarEvent = [
		];

		$scope.configSearchBarEvent = {
			create: true,
			plugins: ['remove_button'],
			valueField: 'value',
			labelField: 'content',
			delimiter: '+',
			onInitialize: function(selectize){
			},
			render: {
				item: function(item, escape) {
					if (item.type == 'category') {
						return '<div class="item">'+
						'<img src="../../dist/images/'+item.content+'.png" class="category-tag-image">'+item.value+
						'</div>';
					}
					else if (item.value == 'OR') {
						return '<div class="item special-tag or">'
						+item.value+
						'</div>';
					}
					else if (item.value == 'AND') {
						return '<div class="item special-tag and">'
						+item.value+
						'</div>';
					}
					else {
						return '<div class="item">'
						+item.value+
						'</div>';
					}
				}
			}
		};

		$scope.getType = function(x) {
        	return typeof x;
      	};
      	$scope.isDate = function(x) {
        	return x instanceof Date;
      	};

		$scope.galleryShowInterval = 3; // Pagination loop interval

		$scope.startGallery = function() {
			$scope.leftBtn = true;
			$timeout(function(){
				$scope.paginationRight();
				$scope.startGallery();
			}, $scope.galleryShowInterval * 1500);	
		}

		if ($location.url().indexOf('loop') > -1) {
			$scope.startGallery();
		}

		$scope.drawPagination = function(postCount) {
			$scope.paginationPages = postCount / 9;
			if ($scope.paginationPages % 1 === 0) {
				$scope.paginationPages = parseInt($scope.paginationPages);
			}
			else {
				$scope.paginationPages = parseInt($scope.paginationPages);
				$scope.paginationPages += 2;
			}
			for(var i = 0; i < $scope.paginationPages; i++) {
				var page = {
					index: i,
					pageNum: i+1
				}
				$scope.paginationPagesArray.push(page);
			}
		}

		$scope.markLastPage = function(lastPage) {
			$timeout(function(){
				$('.pagination-item').removeClass('active');
				$('#pageId-'+lastPage).addClass('active');
			}, 100);
		}

		$scope.goToPage = function(pageNum) {
			$('.pagination-item').removeClass('active');
			$('#pageId-'+pageNum).addClass('active');
			mapDrawCheck = true;
			$scope.nextValue = (pageNum*9)-9;
			$scope.currentPageNum = pageNum;
			mapService.clearHotspots();
			mapService.setDefaultPin();
			$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
			$rootScope.markersArray = [];
			$scope.paginationBar();
			$scope.getEventData();

            if ($scope.tabIndex == 'examine') {
                $scope.callExamineView();
            }
		}

		$scope.paginationPageHighlight = function(nextValue) {
			var nextVal = nextValue/9+1;
			$('.pagination-item').removeClass('active');
			$('#pageId-'+nextVal).addClass('active');

			if($('#pageId-'+nextVal).is(':last-child'))
			{
			    $scope.paginationPageNum += 5;
			}
			if($('#pageId-'+nextVal).is(':first-child') && $scope.paginationPageNum > 10)
			{
			    $scope.paginationPageNum -= 5;
			}
		}

		$scope.sortByDate = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}
		$scope.sortBySentiment = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}
		$scope.resetAll = function($event) {
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.eventSortByDate = '-timestamp';
			$scope.eventSortBySentiment = '*';
			$("#eventSortByDate").select2("val", "");
			$("#eventSortBySentiment").select2("val", "");
			$scope.goToPage(1);
		}
		$scope.sortByPostType = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}
		$scope.gallerySearch = function() {
			$scope.cleanEventMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}

		$scope.paginationBar = function() {
			$scope.progressBarValue = ($scope.nextValue*100)/postCount;
			$scope.progressBarValue = $scope.progressBarValue.toFixed(1);
			if($scope.progressBarValue > 100) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.zoomToPostPin = function(LatLng, mapName) {
			mapService.zoomToPostPin(LatLng, mapName);
		}
		$scope.zoomToMap = function(mapName) {
			mapService.zoomToMap(mapName);
		}

		$scope.showPinOnMap = function(postID, mapName) {
			mapService.showPinOnMap(postID, mapName);
		}

		$scope.drawMarkers = function() {
			for (var item in $scope.eventData) {
				mapService.drawMarker('mapEvent', 'circleEvent', $scope, $scope.eventData[item].location, $scope.eventData[item].platform, $scope.eventData[item].category, $scope.eventData[item].post_id, $scope.eventData[item].content, $scope.eventData[item].user_profile_image_url, $scope.eventData[item].user_name, $scope.eventData[item].media_url);
			}
		}
		$scope.drawHotspots = function() {
			mapService.clearHotspots();
			for (var hotspot in $scope.eventHotspots) {
				mapService.drawHotspots('mapEvent', 'circleEvent', $scope.eventHotspots[hotspot].location, $scope.eventHotspots[hotspot].doc_count.total, $scope.eventHotspots[hotspot].doc_count.platform.twitter, $scope.eventHotspots[hotspot].doc_count.platform.instagram, $scope.eventHotspots[hotspot].key, $scope.eventHotspots[hotspot].avg_sentiment.value, $scope.eventHotspots[hotspot].doc_count.category, $scope.eventHotspots[hotspot].top_category, $scope.eventHotspots[hotspot].redirect_to);
			}
		}

		$scope.showLeftBtn = function(previousValue) {
			if (previousValue != null) $scope.leftBtn = false;
			else {$scope.leftBtn = true; $scope.progressBarValue = 0;}
		}
		$scope.showRightBtn = function(totalCount) {
			if (totalCount > 9) $scope.rightBtn = false;
			else {$scope.rightBtn = true; $scope.paginationShow = false;}
		}

		$scope.noPostsMsgCheck = function(postCount) {
			if (postCount == 0) {
				$scope.noPostsMsg = true;
				$scope.paginationShow = false;
				$scope.cleanEventMarkers();
		    	mapService.clearMarkers();
				mapService.clearHotspots();
			}
			else {
				$scope.noPostsMsg = false;
				$scope.paginationShow = true;
			}
		}

		$scope.onePageCheck = function(postCount) {
			if(postCount <= 9 && postCount != 0) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.checkLastPage = function(lastPage) {
			if (lastPage == null) {
				$scope.rightBtn = true;
				$scope.progressBarValue = 100;
			}
		}

		$scope.timestampFilter = function() {
			$scope.cleanEventMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.filterPopup = !$scope.filterPopup;
			$scope.goToPage(1);
		}

		$scope.getEventData = function() {

			var timestampGte = StreamingService.localDateToString($scope.eventDateTimeFromObj),
                timestampLte = StreamingService.localDateToString($scope.eventDateTimeToObj);

			StreamingService.getEventgData($scope.eventName, $scope.nextValue,
				$scope.eventSortByDate, $scope.eventSortBySentiment,
				$scope.searchBarEventModel, $scope.searchCategoryEventModel,
				$scope.postTypeSelect, $scope.checkTwitterPins, $scope.checkInstagramPins,
				timestampGte, timestampLte,
				function(response) {
					console.log(response);
					if(isMapInited == false) {
						mapService.mapInitEvent(response.data.event.lat, response.data.event.lng, response.data.event.radius, response.data.event.uom, response.data.event.zoom);
						$scope.eventLocationName = response.data.event.name;
						$scope.drawPagination(response.data.meta.total_count);
						if(response.data.event.name == 'San Bernardino Shooting') $scope.eventDate = '2015-02-15T14:27:31.299692+00:00';
						else $scope.eventDate = response.data.event.created;

						$scope.getListData();
					}
					isMapInited = true;

					$scope.onePageCheck(response.data.meta.total_count);

					$scope.noPostsMsgCheck(response.data.meta.total_count);

					$scope.showLeftBtn(response.data.meta.previous);
					$scope.showRightBtn(response.data.meta.total_count);

					if (response.data.meta.total_count > 90) {
						$scope.drawPagination(response.data.meta.total_count);
					}

					// Hack that show timestamp for old events and timestamp_db to new events.
                    if (response.data.objects.length > 0) {
						var o = response.data.objects[2],
						zone = moment.tz.zone(o.timezone),
                        ts = new Date(o.timestamp),
						ts_db = new Date(o.timestamp_db),
						offset = zone.parse(ts),
						difference = Math.abs(ts - ts_db)/36e5*60;
						if (Math.abs(offset) == difference) {
							if ((offset > 0 && ts < ts_db) || (offset < 0 && ts > ts_db)) {
                                $scope.timestampDisplayField = 'timestamp';
                            }
                        }
					}

					postCount = response.data.meta.total_count;
					$scope.eventPreloader = false;
					$scope.eventData = response.data.objects;
					$scope.eventHotspots = response.data.hotspots;

					$scope.eventErrorMsg = false;

					$scope.nextValueCheck = response.data.meta.next;
					$scope.checkLastPage($scope.nextValueCheck);

					$scope.drawMarkers();
					if(mapDrawCheck == true) {
						$scope.drawHotspots();
					}
				},
				function(error) {
					console.log(error);
					$scope.eventPreloader = false;
					$scope.eventErrorMsg = true;
					$scope.rightBtn = true;
				}
				)
		}

		$scope.getEventData();

		$scope.paginationRight = function() {
			if ($scope.nextValueCheck != null) {
				mapDrawCheck = true;
				$scope.nextValue += 9;
				//mapService.clearHotspots();
				mapService.setDefaultPin();
				$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getEventData();
			}
		}
		$scope.paginationLeft = function() {
			if($scope.nextValue > 0) {
				mapDrawCheck = false;
				$scope.nextValue -= 9;
				mapService.setDefaultPin();
				$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getEventData();
			}
		}

	$scope.cleanEventMarkers = function() {
		for(var marker in $scope.eventMarkersArray) {
			$scope.eventMarkersArray[marker].setMap(null);
		}
		$scope.eventMarkersArray = [];
	}

    //Show/Hide Twiiter/Instagram pins on map
    $scope.displayTwitterPins = function($event) {
    	$scope.cleanEventMarkers();
    	mapService.clearMarkers();
    	mapService.clearHotspots();
    	$scope.nextValue = 0;
		$scope.progressBarValue = 0;
		$scope.paginationPageNum = 10;
		$scope.searchCategoryEventModel = [];
    	$scope.goToPage(1);
    }
    $scope.displayInstagramPins = function($event) {
    	$scope.cleanEventMarkers();
    	mapService.clearMarkers();
    	mapService.clearHotspots();
    	$scope.nextValue = 0;
		$scope.progressBarValue = 0;
		$scope.paginationPageNum = 10;
		$scope.searchCategoryEventModel = [];
    	$scope.goToPage(1);
    }

    $scope.chooseCategory = function($event) {
    	$scope.cleanEventMarkers();
    	mapService.clearMarkers();
		mapService.clearHotspots();
		$rootScope.markersArray = [];
    	$scope.nextValue = 0;
		$scope.progressBarValue = 0;
		$scope.paginationPageNum = 10;

		if ($event != undefined) {
			var categoryValue = $event.currentTarget.innerText;
	    	categoryValue = categoryValue.replace(/ /g, "");

	    	$scope.showCategorySelector = !$scope.showCategorySelector;
	    	$scope.optionsCategoryBarEvent.push({value: $event.currentTarget.innerText, content: categoryValue, type: 'category'});
	    	$scope.searchCategoryEventModel.push($event.currentTarget.innerText);
		}

    	$scope.goToPage(1);
    }

	if ($scope.tabIndex == 'examine') {
		$scope.callExamineView();
	}

	//
	// 	MODAL FUNCTIONS
	//

	$scope.callDatePickerModal = function($event) {
		modalService.datePickerModal($event, $scope);
	}

	$scope.annotatePost = function($event) {
		modalService.annotatePost($event, $scope);
	}

	//
	// GLOBAL KEYDOWN FUNCTION
	//

	$rootScope.keyPress = function(keyCode) {
		if (keyCode == 39) { // check if "Right Arrow" key
			$scope.paginationRight();
		}
		else if (keyCode == 37) { // check if "Left Arrow" key
			$scope.paginationLeft();
		}
	}

	//
    // TAB FUNCTIONS
    //

    $scope.callSplitView = function(){
    	tabService.splitView();
    	mapService.mapUpdate('event', 'mapEvent');
    }

    $scope.callMapShowFull = function(){
    	tabService.mapShowFull();
    	mapService.mapUpdate('event', 'mapEvent');
    }

    $scope.callGalleryShowFull = function(){
    	tabService.galleryShowFull();
    }

    $scope.callChangeTab = function(tab) {
    	tabService.changeTab(tab);
    }

    var initNewUserList = false;
	$scope.createNewUserList = function($event) {
    	if ($scope.userListData.length == 0) {
    		modalService.callCreateNewUserList($event, $scope);
    	}
    }

    $scope.getListData = function() {
        ListService.getListData($scope.sortModel,
            function(response) {
                console.log(response);
                $rootScope.userListData = response.data.objects;
                if (initNewUserList == true) {
                	if (response.data.objects.length == 0) {
	                	$scope.createNewUserList();
	                }
                }
                initNewUserList = true;
            },
            function(error) {
                console.log(error);
            }
        )
    }

    $scope.addPostToList = function(postId, listId) {
        if (listId != null) {
            $rootScope.postsId.push(postId);
            ListService.addPostToList(postId, listId,
                function(response) {
                    console.log(response);
                },
                function(error) {
                    console.log(error);
                }
            )
        }
    }

    $scope.showFullImg = function($event, image, post) {
    	modalService.showFullImage($event, $scope, image, post);
    }

    $(document).on("click", ".view-hotspot-btn", function() {
      sessionStorage.removeItem('hotspotCategory');
      var hotspotIdValue = $(this).attr('hotspotId');
      var redirectToVal = $(this).attr('redirectToValue');
      sessionStorage.setItem('hotspotCategory', JSON.stringify($scope.searchCategoryEventModel));
      mapService.initHotspot(hotspotIdValue, redirectToVal);
    });

    $(document).on("click", ".map-show-full-img", function() {
      var image = $(this).next().attr("src");
      modalService.showFullImage('', $scope, image);
    });

    $(document).on("click", ".pagination-list li:nth-child(1)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(2)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(3)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	}
    });

    $(document).on("click", ".pagination-list li:nth-child(10)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(9)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(8)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });

    // Change category
    var hideCategoryListHandler = function () {
        $(".change-category-list").hide();
        $(document).off("click", hideCategoryListHandler);
    };

    $(document).on("click", ".post-category .category-text, .post-category .change-category", function(event) {
        hideCategoryListHandler();
        $(this).parent().parent().find('.change-category-list').show();
        event.stopPropagation();

        $(document).on("click", hideCategoryListHandler);
    });

	$(document).on("click", ".change-category-list-item", function(event) {
		event.stopPropagation();
		var postID = $(this).attr('post-id');
		var category = $(this).html();
		var callback = function () {
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.goToPage($scope.currentPageNum);
		};
		StreamingService.updateCategory(postID, category, callback, callback);
		hideCategoryListHandler();
	});

  }]);

})();

