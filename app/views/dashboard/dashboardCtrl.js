(function () {
  'use strict';

  angular.module("exampleApp")
    .filter('formatDate', function($filter){
    return function(date) {
            /*var timeToLocal = date;  
            timeToLocal = timeToLocal.replace(/T/g, " ");
            timeToLocal = timeToLocal + ' UTC';
            timeToLocal = new Date(timeToLocal);

            var timeToLocalFormated = (
              timeToLocal.getFullYear() + '-' +
              ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
              ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
              ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
              ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
              ("00" + timeToLocal.getSeconds()).slice(-2)
            );*/
            return $filter('date')(date, 'MM/dd/yyyy') ;
        }
    })
    .filter('formatTime', function($filter){
      return function(date){
        /*var timeToLocal = date;  
        timeToLocal = timeToLocal.replace(/T/g, " ");
        timeToLocal = timeToLocal + ' UTC';
        timeToLocal = new Date(timeToLocal);

        var timeToLocalFormated = (
          timeToLocal.getFullYear() + '-' +
          ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
          ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
          ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
          ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
          ("00" + timeToLocal.getSeconds()).slice(-2)
        );*/
        return $filter('date')(date, 'h:mm a');
      }
    })
    .filter('unique', function() {
       return function(collection, keyname) {
          var output = [], 
              keys = [];

          angular.forEach(collection, function(item) {
              var key = item[keyname];
              if(keys.indexOf(key) === -1) {
                  keys.push(key);
                  output.push(item);
              }
          });

          return output;
       };
    })
    .filter('removeSymbols', function () {
        return function (text) {
            var str = text.replace('&amp;amp', '');
            return str;
        };
    })
      .controller("dashboardCtrl", ['$rootScope', '$scope','$timeout', '$http', 'NgMap', 'mapService', 'modalService', 'tabService', 'StreamingService', '$mdDialog', 'ListService', 'analyticsService', 'SaveLocationService', '$sanitize', '$state', '$location', '$window', function($rootScope, $scope, $timeout, $http, NgMap, mapService, modalService, tabService, StreamingService, $mdDialog, ListService, analyticsService, SaveLocationService, $sanitize, $state, $location, $window) {
    	
        /*(function reinitDasboard() {
            if ($rootScope.history.length > 1) {
                location.reload();
            }
        })();*/

        $rootScope.streamToggleCheck = true;
        $rootScope.lastIndex = 1;

        $scope.streamDataArray = [];
        $scope.galleryPreloaderWrap = false;
        $scope.galleryPreInfo = false;
        $scope.galleryPreInfoError = false;
        $scope.checkTwitterPins = true;
        $scope.checkInstagramPins = false;
        $scope.showMoveRadius = false;

        $scope.socialNewsPopup = false;

        $scope.radiusMoreFive = false;

        $scope.dataLoadingCheck = false;
        $scope.excededStreaming = false;

        $scope.activeUserSession = [];
        $scope.checkRepeatArray = [];

        var firstRequers = true;
        var intervalStreamData;

        $scope.stopStreamDataInterval = false;
        var stopStreamDataIntervalNoPosts;

        $scope.preloaderBottom = false;

        $scope.postsFeedArray = [];
        $scope.hotspotsFeedArray = [];

        //
        //  MAP FUNCTIONS
        //

        (function callMapInit() {
            mapService.mapInit();
            $rootScope.setRadiusDisabled = false;
        })();

        /*(function callCleanDefault() {
            mapService.cleanDefaultMapValue();
        })();*/


        $scope.getUserId = function() {
           StreamingService.getUserId(
                function(response) {
                    console.log(response);
                    var userIdArray = response.data.objects;
                    for (var id in userIdArray) {
                        if (userIdArray[id].api_key == $rootScope.userInfo.api_key) {
                            $scope.userId = userIdArray[id].id;
                            $scope.stopAllActiveSessions();
                        } 
                    }
                },
                function(error) {
                    console.log(error);
               }
            ) 
        }   

        $scope.stopAllActiveSessions = function() {
            //Open stream session
            StreamingService.getOpenStreamList(
                function(response) {
                    console.log('active sessions');
                    console.log(response);
                    if (response.data.objects.length > 0) {
                        var sessionsArray = response.data.objects;
                        for (var id in sessionsArray) {
                            var ownerId = sessionsArray[id].owner;
                            ownerId = ownerId.replace(/[^0-9\.]+/g, "");
                            if (ownerId == $scope.userId) {
                                if (sessionsArray[id].note.length == 0) {
                                    $scope.activeUserSession.push(sessionsArray[id]);
                                }
                            }
                        }
                        $scope.stopUserIdActiveSessions();
                    }
                    else {
                        console.log("No active session");
                    }
                },
                function(error) {
                    console.log(error);
               }
            )
        }

        $scope.stopUserIdActiveSessions = function() {
            /*for (var item in $scope.activeUserSession) {
                if ($scope.activeUserSession[item].note.length > 0) {
                    $scope.activeUserSession.splice(0,1);
                }
            }*/
            if ($scope.activeUserSession.length > 0) {
                StreamingService.stopStreamDataInit($scope.activeUserSession[0].name,
                    function(response) {
                        $scope.activeUserSession.splice(0,1);
                        $scope.stopUserIdActiveSessions();
                    },
                    function(error) {
                        console.log(error);
                   }
                )
            }
            else {
                console.log("Session cleared.")
            }
        }

        $scope.callCircleActive = function() {
            $scope.galleryPreloaderWrap = true;
            $scope.galleryPreInfo = true;
            $scope.showMoveRadius = true;
            $scope.galleryPreInfoError = false;
            $scope.radiusMoreFive = false;
            $rootScope.minusDisabled = true;
            $rootScope.plusDisabled = true;
            mapService.circleActive('mapDashboard', 'circleDashboard');
        }

        $scope.radiusMapMinus = function() {
            mapService.radiusMinus();
        }
        $scope.radiusMapPlus = function() {
            mapService.radiusPlus();
        }

        $scope.findLocation = function() {
            NgMap.getMap().then(function(map) {
                var input = (document.getElementById('locationNameInput'));
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);
                autocomplete.addListener('place_changed', function() {
                    var place = autocomplete.getPlace();
                    $rootScope.circlePosition = place.geometry.location;
                    mapService.circleUpdate(place.geometry.location);
                    map.setCenter(place.geometry.location);
                    mapService.getAddress($rootScope.circlePosition.lat(), $rootScope.circlePosition.lng());
                });
            });
        }

        //
        // MODALS SERVICE FUNCTIONS
        //

        $scope.callSaveLocationModal = function($event) {
            modalService.saveLocationModalTwo($event, $scope);
        }

        $scope.callDatePickerModal = function($event) {
            modalService.datePickerModal($event, $scope);
        }

        $scope.annotatePost = function($event) {
            modalService.annotatePost($event, $scope);
        }

        //
        // TAB FUNCTIONS
        //

        $scope.callSplitView = function(){
            tabService.splitView();
            mapService.mapUpdate('dashboard', 'mapDashboard');
        }

        $scope.callMapShowFull = function(){
            tabService.mapShowFull();
            mapService.mapUpdate('dashboard', 'mapDashboard');
        }

        $scope.callGalleryShowFull = function(){
            tabService.galleryShowFull();
        }

        $scope.callChangeTab = function(tab) {
            tabService.changeTab(tab);
        }

        var initNewUserList = false;
        $scope.createNewUserList = function($event) {
            if ($scope.userListData.length == 0) {
                modalService.callCreateNewUserList($event, $scope);
            }
        }

         $scope.getListData = function() {
            ListService.getListData($scope.sortModel,
                function(response) {
                    console.log(response);
                    $rootScope.userListData = response.data.objects;
                    if (initNewUserList == true) {
                        if (response.data.objects.length == 0) {
                            $scope.createNewUserList();
                        }
                    }
                    initNewUserList = true;
                },
                function(error) {
                    console.log(error);
                }
            )
        }

        $scope.addPostToList = function(postId, listId) {
            if (listId != null) {
                $rootScope.postsId.push(postId);
                ListService.addPostToList(postId, listId,
                    function(response) {
                        console.log(response);
                    },
                    function(error) {
                        console.log(error);
                    }
                )
            }
        }

        $scope.recordedDisabled = true;
        $scope.getRecordedInitTrue = function() {
            var recordedInit = localStorage.getItem('recordedInit');
            if (recordedInit != null) {
                $scope.recordedDisabled = false;
            } 
        }
        $scope.getRecordedInitTrue();
        
        $scope.setRecordedInit = function(timestamp) {
            var recordedInit = localStorage.getItem('recordedInit');
            if (recordedInit == null) {
                NgMap.getMap("mapDashboard").then(function(map) {
                    $scope.zoomVal = map.getZoom();
                    
                    var initTimestamp = timestamp;
                    initTimestamp = initTimestamp.slice(0, initTimestamp.lastIndexOf("."));
                    var dateInit = {
                        initTimestamp: initTimestamp,
                        radius: $rootScope.radiusValue,
                        uom: $rootScope.radiusUnit,
                        lng: $rootScope.circlePosition.lng(),
                        lat: $rootScope.circlePosition.lat(),
                        zoom: $scope.zoomVal,
                        location_name: $rootScope.addressName
                    }
                    localStorage.setItem('recordedInit', JSON.stringify(dateInit));  
                });  
            }
            else {
                NgMap.getMap("mapDashboard").then(function(map) {
                    $scope.zoomVal = map.getZoom();
                    
                    localStorage.removeItem('recordedInit');
                    var initTimestamp = timestamp;
                    initTimestamp = initTimestamp.slice(0, initTimestamp.lastIndexOf("."));
                    var dateInit = {
                        initTimestamp: initTimestamp,
                        radius: $rootScope.radiusValue,
                        uom: $rootScope.radiusUnit,
                        lng: $rootScope.circlePosition.lng(),
                        lat: $rootScope.circlePosition.lat(),
                        zoom: $scope.zoomVal,
                        location_name: $rootScope.addressName
                    }
                    localStorage.setItem('recordedInit', JSON.stringify(dateInit));  
                });
            } 
            setTimeout(function() {
                $scope.getRecordedInitTrue();
            }, 100); 
        }


        $scope.getStreamDataInit = function() {
            $scope.stopStreamDataInterval = true;
            StreamingService.getStreamDataInit(
                function(response) {
                    $scope.galleryPreloaderWrap = false;
                    $scope.getListData();
                    $scope.streamSessionId = response.data.name;
                    $rootScope.liveStreamTimestamp = response.data.created;
                    console.log(response);
                    stopStreamDataIntervalNoPosts = true;
                    $scope.dataLoadingCheck = true;
                    setAnalyticsReloadInterval();
                    $scope.checkRepeatArray = [];
                    $scope.getStreamData();
                    $scope.setRecordedInit(response.data.created);
                },
                function(error) {   
                    console.log(error);
                    $scope.stopStreamDataInterval = false;
                    $scope.galleryPreloaderWrap = false;
                    clearInterval(intervalStreamData);
                    $scope.clearAnalyticsReloadInterval();
                    $scope.streamDataArray = [];
                    clearAllMarkers();
                    mapService.clearHotspots();
                    $rootScope.markersArray = [];
                    mapService.circleDefault('mapDashboard', 'circleDashboard');
                    $scope.showMoveRadius = false;
                    $rootScope.minusDisabled = false;
                    $rootScope.plusDisabled = false;
                    $scope.checkRepeatArray = [];
                    $scope.radiusLimitError = error.data.error.slice(0, error.data.error.indexOf('!'));
                    if (error.data.error.indexOf('You cannot monitor radius larger than') > -1) $scope.radiusMoreFive = true;
                    else if (error.data.error.indexOf("You have reached your limit of active sessions") > -1) $scope.excededStreaming = true;
                    else $scope.galleryPreInfoError = true;
                }
            )
        }

        $scope.drawedHotspots = [];
        $scope.drawHotspots = function() {
            for (var hotspotDrawed in $scope.drawedHotspots) {
                for (var hotspotNoDrawed in $rootScope.hotspotArray) {
                    if ($scope.drawedHotspots[hotspotDrawed].key == $rootScope.hotspotArray[hotspotNoDrawed].id) {
                        $rootScope.hotspotArray[hotspotNoDrawed].setMap(null);
                    }
                }
            }
            // Separator
            for (var hotspot in $scope.dasboardHotspots) {              
                mapService.drawHotspots('mapDashboard', 'circle', $scope.dasboardHotspots[hotspot].location, $scope.dasboardHotspots[hotspot].doc_count.total, $scope.dasboardHotspots[hotspot].doc_count.platform.twitter, $scope.dasboardHotspots[hotspot].doc_count.platform.instagram, $scope.dasboardHotspots[hotspot].key, $scope.dasboardHotspots[hotspot].avg_sentiment.value, '', $scope.dasboardHotspots[hotspot].top_category, $scope.dasboardHotspots[hotspot].redirect_to);
                $scope.drawedHotspots.push($scope.dasboardHotspots[hotspot]);
            }
        }

        $scope.radiusInMeters = function(radius, unit) {
            if (unit == 'mi') {
                return radius * 1609.34; 
            }
            else {
                return radius * 0,3048; 
            }
        }

        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        $scope.getStreamData = function() {
            console.log("Stream data");
            if ($scope.stopStreamDataInterval == true) {
                if ($rootScope.streamToggleCheck == true) {
                    StreamingService.getStreamingData($scope.timestamp,
                        function(response) {
                            console.log(response);

                            if (response.data.objects.length > 0) {

                                //  Reduce streamDataArray length;
                                if ($scope.streamDataArray.length > 500) {
                                    var indexSlice = parseInt($scope.streamDataArray.length * 0.25);
                                    $scope.streamDataArray.splice($scope.streamDataArray.length - indexSlice, indexSlice);
                                }

                                $scope.galleryPreloaderWrap = false;
                                
                                var tempData = response.data.objects;
                                var streamData = tempData.slice().reverse();
                                var index = 0;

                                $scope.dasboardHotspots = response.data.hotspots;

                                // Count of post duplicates in current response.
                                var duplicatesCount = 0;

                                // New posts from response.
                                var newPosts = [];

                                var i = streamData.length;

                                while (i--) {
                                    var post = streamData[i];

                                    // Check if current post in the search radius.
                                    var distance = getDistanceFromLatLonInKm($rootScope.circlePosition.lat(),
                                        $rootScope.circlePosition.lng(), post.location[1], post.location[0]);
                                    var radius = $scope.radiusInMeters($rootScope.radiusValue, $rootScope.radiusUnit);
                                    if (radius < distance) {
                                        streamData.splice(i, 1);
                                        continue;
                                    }

                                    // Indicate that current post is duplicate.
                                    var isDuplicate = false;

                                    // Find duplicates and remove it from streamData.
                                    for (var j = 0; j < $scope.checkRepeatArray.length; j++) {
                                        var _post = $scope.checkRepeatArray[j];
                                        if (_post.user_screen_name == post.user_screen_name
                                            && _post.timestamp == post.timestamp) {
                                            duplicatesCount++;
                                            streamData.splice(i, 1);
                                            isDuplicate = true;
                                            break;
                                        }
                                    }

                                    // Keeps last 10 unique posts. Keep the post if it is unique.
                                    if (!isDuplicate) {
                                        if ($scope.checkRepeatArray.length > 9)
                                            $scope.checkRepeatArray.shift();
                                        $scope.checkRepeatArray.push(post);
                                    }
                                }

                                // It there are new posts in streamData, display then on them on map
                                // draw hotspots and change scope timestamp.
                                if (streamData.length > 0) {
                                    /*for (var i=0; i < streamData.length; i++) {
                                        var data = streamData[i];


                                        // Add data to global streamDataArray.
                                        $scope.streamDataArray.unshift(data);

                                        // Draw marker on the map.
                                        mapService.drawMarker('mapDashboard', 'circle', $scope, data.location,
                                            data.platform, data.category, data.post_id, data.content,
                                            data.user_profile_image_url, data.user_name, data.media_url);
                                    }*/

                                    var index = 0;
                                    var drawFeed = function () {
                                        if ($scope.stopStreamDataInterval == true) {
                                            if (index < streamData.length) {
                                                $scope.streamDataArray.unshift(streamData[index]);

                                                // Draw marker on the map.
                                                mapService.drawMarker('mapDashboard', 'circle', $scope, streamData[index].location,
                                                streamData[index].platform, streamData[index].category, streamData[index].post_id, streamData[index].content,
                                                streamData[index].user_profile_image_url, streamData[index].user_name, streamData[index].media_url);

                                                index++;
                                                setTimeout(function() { 
                                                    drawFeed();
                                                }, 700);
                                            }
                                        }
                                        else return;
                                    };
                                    drawFeed();

                                    if ($scope.dasboardHotspots.length > 0) {
                                        $scope.drawHotspots();
                                    }

                                    $scope.timestamp = streamData[streamData.length - 1].timestamp;
                                }

                                // Restart streamData.
                                $timeout(function (){
                                    $scope.getStreamData();
                                }, 2000);


                            } else {
                                if (stopStreamDataIntervalNoPosts == true) {
                                    if ($rootScope.streamToggleCheck) {
                                        $scope.galleryPreloaderWrap = true;
                                        setTimeout(function(){
                                            $scope.getStreamData();
                                        }, 2000); 
                                        console.log("No posts here");
                                    }
                                }
                                else {
                                    return;
                                }
                            }
                        },
                        function(error) {
                            console.log(error);
                            $scope.radiusMoreFive = false;
                            $scope.galleryPreloaderWrap = false;
                            clearInterval(intervalStreamData);
                            $scope.clearAnalyticsReloadInterval();
                            $scope.streamDataArray = [];
                            clearAllMarkers();
                            mapService.clearHotspots();
                            $rootScope.markersArray = [];
                            mapService.circleDefault('mapDashboard', 'circleDashboard');
                            $scope.showMoveRadius = false;
                            $scope.galleryPreInfoError = true;
                            $rootScope.minusDisabled = false;
                            $rootScope.plusDisabled = false;
                       }
                    )
                }
            }
            else return;
        }

        $scope.activeMoveRadius = function() {
            modalService.stopStreamDashboardModal();
        }

        function clearAllMarkers() {
            for (var marker in $rootScope.markersArray) {
                $rootScope.markersArray[marker].setMap(null);
            }
        }

        $scope.stopStreamData = function() {
            $scope.stopStreamDataInterval = false;
            clearAllMarkers();
            mapService.clearHotspots();
            clearInterval(intervalStreamData);
            $scope.clearAnalyticsReloadInterval();
            $scope.dataLoadingCheck = false;
            stopStreamDataIntervalNoPosts = false;
            $scope.galleryPreloaderWrap = false;
            $scope.galleryPreInfo = false;
            $scope.galleryPreloaderWrap = false;
            $rootScope.minusDisabled = false;
            $rootScope.plusDisabled = false;
            mapService.circleDefault('mapDashboard', 'circleDashboard');
            $scope.showMoveRadius = false;
            $rootScope.dashboardCache.stream = false;
            $scope.streamDataArray = [];
            $scope.checkRepeatArray = [];
            $rootScope.markersArray = [];
            StreamingService.stopStreamDataInit($scope.streamSessionId,
                function(response) {
                    console.log(response);
                    if (response.status == 204) {
                        console.log("Successfully stopped");
                    }
                },
                function(error) {
                    console.log(error);
                    if (error.status == 404) {
                        console.log("Successfully stopped");
                    }
               }
            )
            $scope.stopAllActiveSessions();
        }        

        $scope.$on('stopStreamData', function(event, data) {
            $scope.stopStreamData();
        })

        $scope.$on('saveLocation', function(event, locationTitle) {
            $scope.saveLocation(locationTitle.locationTitle);
        })

        $scope.goToRecorded = function() {
            $scope.preloaderBottom = true;
            
            NgMap.getMap("mapDashboard").then(function(map) {
                $scope.zoomVal = map.getZoom();

                $scope.postsFeedArray = $scope.postsFeedArray.concat($scope.streamDataArray);
                $scope.hotspotsFeedArray = $scope.hotspotsFeedArray.concat($scope.drawedHotspots);

                var streamDataObj = {
                    posts: $scope.postsFeedArray,
                    hotspots: $scope.hotspotsFeedArray
                }

                $rootScope.dashboardCache = {
                    radius: $rootScope.radiusValue,
                    uom: $rootScope.radiusUnit,
                    cached: true,
                    center: $rootScope.circlePosition,
                    stream: $scope.stopStreamDataInterval, 
                    zoom: $scope.zoomVal,
                    stream_started: $rootScope.liveStreamTimestamp
                }

                sessionStorage.setItem('streamCachedData', JSON.stringify(streamDataObj));
            });

            if ($scope.stopStreamDataInterval == true) {
                $scope.clearAnalyticsReloadInterval();
                StreamingService.stopStreamDataInit($scope.streamSessionId,
                    function(response) {
                        console.log(response);
                        if (response.status == 204) {
                            $scope.dataLoadingCheck = false;
                            stopStreamDataIntervalNoPosts = false;
                            $scope.stopStreamDataInterval = false;
                            $scope.stopAllActiveSessions();
                            clearInterval(intervalStreamData);

                            var recordedInitTimestamp = JSON.parse(localStorage.getItem('recordedInit'));
                            var localTime = new Date();
                            localTime = localTime.toISOString();
                            localTime = localTime.slice(0, localTime.lastIndexOf("."));
                            var urlLink = '*timestamp__gte='+recordedInitTimestamp.initTimestamp+'&timestamp__lte='+localTime+'&radius='+recordedInitTimestamp.radius+'&uom='+recordedInitTimestamp.uom+'&lat='+recordedInitTimestamp.lat+'&lng='+recordedInitTimestamp.lng+'&zoom='+recordedInitTimestamp.zoom+'&location='+recordedInitTimestamp.location_name+'&query='+ '' + '&all_categories'+ '';
                            $location.path('/recorded' + urlLink);
                        }
                    },
                    function(error) {
                        console.log(error);
                        if (error.status == 404) {
                            $scope.dataLoadingCheck = false;
                            stopStreamDataIntervalNoPosts = false;
                            $scope.stopStreamDataInterval = false;
                            $scope.stopAllActiveSessions();
                            clearInterval(intervalStreamData);

                            var recordedInitTimestamp = JSON.parse(localStorage.getItem('recordedInit'));
                            var localTime = new Date();
                            localTime = localTime.toISOString();
                            localTime = localTime.slice(0, localTime.lastIndexOf("."));
                            var urlLink = '*timestamp__gte='+recordedInitTimestamp.initTimestamp+'&timestamp__lte='+localTime+'&radius='+recordedInitTimestamp.radius+'&uom='+recordedInitTimestamp.uom+'&lat='+recordedInitTimestamp.lat+'&lng='+recordedInitTimestamp.lng+'&zoom='+recordedInitTimestamp.zoom+'&location='+recordedInitTimestamp.location_name+'&query='+ '' + '&all_categories'+ '';
                            $location.path('/recorded' + urlLink);
                        }
                   }
                )
            }
            else {
                var recordedInitTimestamp = JSON.parse(localStorage.getItem('recordedInit'));
                var localTime = new Date();
                localTime = localTime.toISOString();
                localTime = localTime.slice(0, localTime.lastIndexOf("."));

                var posLat = recordedInitTimestamp.lat.toString();
                var posLng = recordedInitTimestamp.lng.toString();

                posLat = posLat.replace(/\./g, ":");
                posLng = posLng.replace(/\./g, ":");
                var urlLink = '*timestamp__gte='+recordedInitTimestamp.initTimestamp+'&timestamp__lte='+localTime+'&radius='+recordedInitTimestamp.radius+'&uom='+recordedInitTimestamp.uom+'&lat='+posLat+'&lng='+posLng+'&zoom='+recordedInitTimestamp.zoom+'&location='+recordedInitTimestamp.location_name+'&query='+ '' + '&all_categories'+ '';
                $location.path('/recorded' + urlLink);
            }
        }

        $scope.drawCachedHotspot = function(data) {
            var hotspots = data.hotspots;

            for (var hotspot in hotspots) {
                mapService.drawHotspots('mapDashboard', 'circle', hotspots[hotspot].location, hotspots[hotspot].doc_count.total, hotspots[hotspot].doc_count.platform.twitter, hotspots[hotspot].doc_count.platform.instagram, hotspots[hotspot].key, hotspots[hotspot].avg_sentiment.value, '', hotspots[hotspot].top_category, hotspots[hotspot].redirect_to);
            }
            sessionStorage.removeItem('streamCachedData');
            $scope.getStreamDataInit();
        }

        $scope.drawCachedFeed = function(data) {
            var posts = data.posts;
            for (var item in posts) {
                $scope.streamDataArray.push(posts[item]);
                mapService.drawMarker('mapDashboard', 'circle', $scope, posts[item].location, posts[item].platform, posts[item].category, posts[item].post_id, posts[item].content, posts[item].user_profile_image_url, posts[item].user_name, posts[item].media_url);
            }
            // Add first 10 posts to checkRepeatArray to avoid duplicates.
            if ($scope.checkRepeatArray.length == 0) {
                $scope.checkRepeatArray = posts.slice(0, 9);
            }
            $scope.drawCachedHotspot(data);
        }

        $scope.callActiveStream = function() {
            if ($rootScope.dashboardCache.stream == true) {
                $scope.galleryPreloaderWrap = true;
                $rootScope.setRadiusDisabled = false;
                $scope.galleryPreInfo = true;
                $scope.showMoveRadius = true;
                $scope.galleryPreInfoError = false;
                $scope.radiusMoreFive = false;
                mapService.circleActive('mapDashboard', 'circleDashboard');
                $rootScope.minusDisabled = true;
                $rootScope.plusDisabled = true;
                $scope.preloaderBottom = false;
                setTimeout(function(){
                    var streamCachedData = sessionStorage.getItem('streamCachedData');
                    streamCachedData = JSON.parse(streamCachedData);
                    $scope.drawCachedFeed(streamCachedData);
                }, 1000);
            }
            else {
                //radius data
                $rootScope.radiusValue = 5;
                $rootScope.radiusUnit = 'mi';
                $scope.getUserId();
            }
        }
    
        $scope.callActiveStream();

        $scope.saveLocation = function(locationTitle) {
            NgMap.getMap("mapDashboard").then(function(map) {
                $scope.zoomVal = map.getZoom();
                var body = {
                    note: locationTitle,
                    lng: $rootScope.circlePosition.lng(),
                    lat: $rootScope.circlePosition.lat(),
                    zoom: $scope.zoomVal,
                    radius: $rootScope.radiusValue,
                    uom: $rootScope.radiusUnit,
                    location_name: $rootScope.addressName
                }
                SaveLocationService.saveLocation(body,
                    function(response) {
                        if(response.status == 201) {
                            modalService.saveLocationModalSuccess();
                        }
                    },
                    function(error) {
                        console.log(error);
                   }
                )
            });
        }

        // Start stream if no monitoring
        $scope.startStreamFromSavedLocation = function(locationId) {
            var locId = locationId;
            SaveLocationService.startMonitoring(locationId,
                function(response) {
                    console.log(response);
                    if (response.data.stream_entries.objects.length > 0) {
                        $timeout(function (){
                            $scope.getStreamData();
                        }, 2000);
                    }
                    else {
                        if (stopStreamDataIntervalNoPosts == true) {
                            if ($rootScope.streamToggleCheck) {
                                $scope.galleryPreloaderWrap = true;
                                setTimeout(function(){
                                    $scope.getStreamData();
                                }, 2000); 
                                console.log("No posts here");
                            }
                        }
                        else {
                            return;
                        }
                    }
                },
                function(error) {
                    console.log(error);
                    $scope.radiusMoreFive = false;
                    $scope.galleryPreloaderWrap = false;
                    clearInterval(intervalStreamData);
                    $scope.clearAnalyticsReloadInterval();
                    $scope.streamDataArray = [];
                    clearAllMarkers();
                    mapService.clearHotspots();
                    $rootScope.markersArray = [];
                    mapService.circleDefault('mapDashboard', 'circleDashboard');
                    $scope.showMoveRadius = false;
                    $scope.galleryPreInfoError = true;
                    $rootScope.minusDisabled = false;
                    $rootScope.plusDisabled = false;
                    if(error.data.error.indexOf('You cannot monitor radius larger than') > -1) {
                        modalService.monitoringError('', $scope);
                    }
                    if(error.data.error.indexOf('You have reached your limit of active sessions') > -1) {
                        modalService.streamAlreadyStart('', $scope, locationId);
                    }
               }
            )
        };

        $scope.startStramFromLocation = function() {
            var savedLocationData = localStorage.getItem('savedLocation');
            if (savedLocationData != null) {
                $scope.savedLoc = JSON.parse(savedLocationData);
                localStorage.removeItem('savedLocation');
                console.log('savedLoc', $scope.savedLoc);
                if ($scope.savedLoc.is_active == true) {
                    $rootScope.dashboardCache = {
                        radius: $scope.savedLoc.radius,
                        uom: $scope.savedLoc.uom,
                        center: { lat: $scope.savedLoc.center.lat, lng: $scope.savedLoc.center.lng},
                        zoom: $scope.savedLoc.zoom
                    };
                    $rootScope.radiusUnit = $scope.savedLoc.uom;
                    $rootScope.radiusValue = $scope.savedLoc.radius;
                    $scope.timestamp = new Date().toISOString();
                    $scope.stopStreamDataInterval = true;
                    stopStreamDataIntervalNoPosts = true;
                    $rootScope.liveStreamTimestamp = $scope.timestamp;
                    $scope.callCircleActive();
                    $scope.setRecordedInit($scope.timestamp);
                    setTimeout(function(){
                        $scope.getStreamData();
                        setAnalyticsReloadInterval();
                        $scope.getRecordedInitTrue();
                    }, 1000);
                }
                else {
                    $rootScope.dashboardCache = {
                        radius: $scope.savedLoc.radius,
                        uom: $scope.savedLoc.uom,
                        center: { lat: $scope.savedLoc.center.lat, lng: $scope.savedLoc.center.lng},
                        zoom: $scope.savedLoc.zoom
                    }
                    $rootScope.radiusValue = $scope.savedLoc.radius;
                    $scope.timestamp = new Date().toISOString();
                    $scope.stopStreamDataInterval = true;
                    stopStreamDataIntervalNoPosts = true;
                    $rootScope.liveStreamTimestamp = $scope.timestamp;
                    $scope.callCircleActive();
                    $scope.setRecordedInit($scope.timestamp);
                    setTimeout(function(){
                        $scope.startStreamFromSavedLocation($scope.savedLoc.locationId);
                        setAnalyticsReloadInterval();
                        $scope.getRecordedInitTrue();
                    }, 1000);
                }
            }
        }
        $scope.startStramFromLocation();

        $scope.zoomToPostPin = function(LatLng, mapName) {
            mapService.zoomToPostPin(LatLng, mapName);
        }
        $scope.zoomToMap = function(mapName) {
            mapService.zoomToMap(mapName);
        }

        $scope.showPinOnMap = function(postID, mapName) {
            mapService.showPinOnMap(postID, mapName);
        }

        //Show/Hide Twiiter/Instagram pins on map
        $scope.displayTwitterPins = function($event) {
            mapService.displayTwitterPins($event, $scope);
        }
        $scope.displayInstagramPins = function($event) {
            mapService.displayInstagramPins($event, $scope);
        }

        $scope.toggleStreamChange = function() {
            if($rootScope.streamToggleCheck == true) {
                $(".gallery-wrap").animate({scrollTop: 0}, 300);
                $rootScope.streamToggleCheck = true;
                $rootScope.showNewPostsButton = false;
                $scope.getStreamData();
            }
        }

        $scope.showFullImg = function($event, image, post) {
            modalService.showFullImage($event, $scope, image, post);
        }

        $scope.openInNewTab = function(page) {
            if ($scope.stopStreamDataInterval == true) {
                modalService.openInNewTab('', $scope, page);
            }
            else {
                $location.url(page);
            }
        }

        $scope.$on('stopStreaAndOpenNewTab', function(event, data) {
            $scope.stopStreamData();
            setTimeout(function(){
               var page = data.page;
               $location.url(page);
            }, 1000);
        })
        

        //
        //  Examine Tab
        //

        // Initialize examine tab on current controller.
        analyticsService.examineTab($scope, {

            /*  Function that allow load examine tab only if stream started
                or dashboardCache not empty.
             */
            onCallExamineView: function (customArgs) {

                // Check if function called by timer or by switching the tab.
                try {
                    var byTimer = customArgs.byTimer;
                } catch (e) {
                    var byTimer = false;
                }

                // If timer already set and user manually call examine tab,
                //  do nothing.
                if ('undefined' !== typeof $scope.analyticsReloadInterval) {
                    if (byTimer) {
                        return true;
                    } else {
                        if ($scope.examineTabSettings.infoStatus >= 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    // Activate examine tab if return from hotspot page
                    // or recorded page back to dashboard.

                    if ($rootScope.dashboardCache.stream) {
                        $scope.analyticsExtraFiltres.lng = $rootScope.dashboardCache.center.lng();
                        $scope.analyticsExtraFiltres.lat = $rootScope.dashboardCache.center.lat();
                        $scope.analyticsExtraFiltres.radius = $rootScope.dashboardCache.radius;
                        $scope.analyticsExtraFiltres.uom = $rootScope.dashboardCache.uom;

                        return setAnalyticsReloadInterval("recorded");
                    } else {
                        // Prevent default Examine tab loading;
                        return false;
                    }
                }
            }
        });

        // Define extra filters for analytics data.
        $scope.analyticsExtraFiltres = {};

        // Define reload interval and functions.
        $scope.analyticsReloadInterval;

        function setAnalyticsReloadInterval(type) {
            /*  Reload analytics every 10 seconds.
                @params type - live or recorder, default live.
             */
            if (typeof $scope.analyticsReloadInterval !== 'undefined'
                    || typeof $scope.callExamineView === 'undefined')
                return;

            if ("undefined" === typeof type)
                type = "live";

            // Show loader.
            analyticsService.showInfo($scope, 1);

            $scope.analyticsExtraFiltres.lng = $rootScope.circlePosition.lng();
            $scope.analyticsExtraFiltres.lat = $rootScope.circlePosition.lat();
            $scope.analyticsExtraFiltres.radius = $rootScope.radiusValue;
            $scope.analyticsExtraFiltres.uom = $rootScope.radiusUnit;
            updateAnalyticsTimestamp(type);

            $scope.analyticsReloadInterval = setInterval(function () {
                updateAnalyticsTimestamp(type);

                if ($scope.tabIndex == 'examine')
                    $scope.callExamineView({byTimer: true});
            }, 10000); // Each 10 secs.
        }

        $scope.clearAnalyticsReloadInterval = function () {
            clearInterval($scope.analyticsReloadInterval);
            $scope.analyticsReloadInterval = undefined;
            $scope.examineHidePreInfo = false;
            $scope.examineHideInfo = true;
            analyticsService.showInfo($scope, -1);
        }

        function updateAnalyticsTimestamp(type) {
            /*  Set timestamp__gte and timestamp__lte params.
                @params type - live or recorder, default live.
            */
            if ("undefined" === typeof type)
                type = "live";

            var now = new Date();

            if (type == "recorded") {
                if ("undefined" !== typeof $rootScope.dashboardCache.stream_started
                        && $rootScope.dashboardCache.stream_started)
                    var gte = $rootScope.dashboardCache.stream_started;
            } else {
                var recordedInit = localStorage.getItem('recordedInit');
                if (recordedInit) {
                    var init = JSON.parse(recordedInit);
                    var gte = init.initTimestamp;
                }
                if ("undefined" === typeof gte)
                    var gte = $rootScope.liveStreamTimestamp;
            }

            $scope.analyticsExtraFiltres.timestamp__lte =
                analyticsService.dateToString(now);
            $scope.analyticsExtraFiltres.timestamp__gte = gte;

            // Check if live stream start lesser than 50 mins ago, than add
            // extended_bounds_min parameter.
            var fiftyMinsBefore = new Date(now.getUTCFullYear(), now.getUTCMonth(),
                now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(),
                now.getUTCSeconds());
            fiftyMinsBefore.setMinutes(fiftyMinsBefore.getMinutes() - 50);

            var diff = new Date(gte).getTime() - fiftyMinsBefore.getTime();

            if (diff > 0) {
                $scope.analyticsExtraFiltres.extended_bounds_min =
                    analyticsService.dateToString(fiftyMinsBefore, false);
            }
        }

        $scope.mapTabInit = function() {
            if ($rootScope.saveNewLocation == true) {
                clearAllMarkers();
                $scope.stopStreamData();
                $rootScope.saveNewLocation = false;
                $rootScope.setRadiusDisabled = true;
                $rootScope.tabIndex = 'map';
                tabService.mapShowFull();
            }
            else {
                //$rootScope.setRadiusDisabled = false;
                $rootScope.tabIndex = 'split-view';
                tabService.splitView();
            }
        }
        $scope.mapTabInit();

        $scope.goToHotspot = function(hotspotIdValue, redirectToVal) {
            $scope.preloaderBottom = true;
            
            NgMap.getMap("mapDashboard").then(function(map) {
                $scope.zoomVal = map.getZoom();

                $scope.postsFeedArray = $scope.postsFeedArray.concat($scope.streamDataArray);
                $scope.hotspotsFeedArray = $scope.hotspotsFeedArray.concat($scope.drawedHotspots);

                var streamDataObj = {
                    posts: $scope.postsFeedArray,
                    hotspots: $scope.hotspotsFeedArray
                }

                $rootScope.dashboardCache = {
                    radius: $rootScope.radiusValue,
                    uom: $rootScope.radiusUnit,
                    cached: true,
                    center: $rootScope.circlePosition,
                    stream: $scope.stopStreamDataInterval, 
                    zoom: $scope.zoomVal,
                    stream_started: $rootScope.liveStreamTimestamp
                }

                sessionStorage.setItem('streamCachedData', JSON.stringify(streamDataObj));
            });

            if ($scope.stopStreamDataInterval == true) {
                StreamingService.stopStreamDataInit($scope.streamSessionId,
                    function(response) {
                        console.log(response);
                        if (response.status == 204) {
                            $scope.dataLoadingCheck = false;
                            stopStreamDataIntervalNoPosts = false;
                            $scope.stopStreamDataInterval = false;
                            $scope.stopAllActiveSessions();
                            clearInterval(intervalStreamData);
                            $scope.clearAnalyticsReloadInterval();
                            
                            var extra_params = {};
                            if ($rootScope.liveStreamTimestamp)
                              extra_params.timestamp__gte = $rootScope.liveStreamTimestamp;
                            mapService.initHotspot(hotspotIdValue, redirectToVal, extra_params);
                        }
                    },
                    function(error) {
                        console.log(error);
                        if (error.status == 404) {
                            $scope.dataLoadingCheck = false;
                            stopStreamDataIntervalNoPosts = false;
                            $scope.stopStreamDataInterval = false;
                            $scope.stopAllActiveSessions();
                            clearInterval(intervalStreamData);
                            $scope.clearAnalyticsReloadInterval();

                            var extra_params = {};
                            if ($rootScope.liveStreamTimestamp)
                              extra_params.timestamp__gte = $rootScope.liveStreamTimestamp;
                            mapService.initHotspot(hotspotIdValue, redirectToVal, extra_params);
                        }
                   }
                )
            }
            else {
                // Add stream start time as timestamp__gte extra parameter.
                var extra_params = {};
                if ($rootScope.liveStreamTimestamp)
                  extra_params.timestamp__gte = $rootScope.liveStreamTimestamp;
                mapService.initHotspot(hotspotIdValue, redirectToVal, extra_params);
            }
        }

        $(document).on("click", ".view-hotspot-btn", function() {
          var hotspotIdValue = $(this).attr('hotspotId');
          var redirectToVal = $(this).attr('redirectToValue');

          $scope.goToHotspot(hotspotIdValue,redirectToVal);
        });

        $(document).on("click", ".map-show-full-img", function() {
          var image = $(this).next().attr("src");
          modalService.showFullImage('', $scope, image);
        });

        var scrollTopOffset = false;
        $('.gallery-wrap').bind('mousewheel',function(event) {
            var scrollpx = parseInt($('.gallery-wrap').scrollTop());
            if (event.originalEvent.wheelDelta > 0) {
                if (scrollpx < 50) {
                    if (scrollTopOffset == true) {
                        $scope.getStreamData(); //start stream
                        scrollTopOffset = false;
                    }
                }
            }
            else {
                scrollTopOffset = true; 
            }
        });
    }]);
})();
