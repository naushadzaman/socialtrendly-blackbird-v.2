(function () {
  'use strict';

  angular.module("exampleApp")
	.filter('formatDate', function($filter){
	    return function(date) {
	            return $filter('date')(date, 'MM/dd/yyyy') ;
	        }
	})
	.filter('formatTime', function($filter){
	    return function(date){
	        return $filter('date')(date, 'h:mm a');
	    }
	})
	.filter('reverse', function() {
      return function(items) {
        return items.slice().reverse();
      }
    })
    .controller("savedLocationsCtrl", ['$rootScope','$scope', 'modalService', '$mdDialog', 'SaveLocationService', '$location', function($rootScope, $scope, modalService, $mdDialog, SaveLocationService, $location) {
    	$rootScope.checkInit = false;

    	$scope.saveLocationShow = false;
    	$scope.noLocations = false;
    	$scope.preloader = true;

    	$scope.preloaderBottom = false;

        $rootScope.dashboardCache.stream = false;

        $scope.isActiveStream = {
            locationId: '',
            is_active: false
        };

    	$scope.checkSavedLocationsCount = function(locationCount) {
    		if (locationCount.length > 0) {
    			$scope.preloader = false;
    			$scope.saveLocationShow = true;
    			$scope.noLocations = false;
    		}
    		else {
    			$scope.saveLocationShow = false;
				$scope.preloader = false;
				$scope.noLocations = true;
    		}
    	}

        $scope.checkActiveStream = function() {
            for (var item in $scope.savedLocationList) {
                if ($scope.savedLocationList[item].is_active == true) {
                    $scope.isActiveStream = {
                        locationId: $scope.savedLocationList[item].id,
                        is_active: true
                    };
                    return;
                }
                else {
                    $scope.isActiveStream = {
                        locationId: '',
                        is_active: false
                    };
                }
            }
        }

    	$scope.getSavedLocationData = function() {
			SaveLocationService.getSavedLocationData($scope.sortModel, 'locations',
                function(response) {
                    console.log(response);
                    $scope.checkSavedLocationsCount(response.data.objects);
                    $scope.savedLocationList = response.data.objects;
                    $scope.checkActiveStream();
                    console.log($scope.savedLocationList);
                },
                function(error) {
                    $scope.preloaderBottom = false;
                    console.log(error);
               }
            )
    	}

    	$scope.getSavedLocationData();

    	$scope.callDeleteSavedLocation = function($event, locationId) {
    		modalService.deleteSavedModal($event, $scope, locationId);
    	}

    	$scope.$on('deleteSavedLocation', function(event, data) {
            $scope.deleteSavedLocation(data.locationId);
        })

    	$scope.deleteSavedLocation = function(locationId) {
    		$scope.preloaderBottom = true;
    		SaveLocationService.stopMonitoring(locationId,
                function(response) {
                	console.log(response);
                    if(response.status == 202) {
                    	SaveLocationService.deleteSavedLocation(locationId,
			                function(response) {
			                    if(response.status == 204) {
			                    	$scope.preloaderBottom = false;
			                    	$scope.getSavedLocationData();
			                    }
			                },
			                function(error) {
                                $scope.preloaderBottom = false;
			                    console.log(error);
			               }
			            )
                    }
                },
                function(error) {
                    $scope.preloaderBottom = false;
                    console.log(error);
               }
            )

    	}

    	$scope.sortByDateSavedLocation = function() {
	        $scope.sortModel = $scope.sortByDateSavedLoc;
	        $scope.getSavedLocationData();
	    }
	    $scope.sortByNameSavedLocation = function() {
	        $scope.sortModel = $scope.sortByNameSavedLoc;
	        $scope.getSavedLocationData();
	    }

	    $scope.resetAll = function($event) { 
			$scope.sortByDateSavedLoc = 'created';
	        $scope.sortByNameSavedLoc = '-note';
	        $scope.sortModel = undefined;
	        $("#savedLocationSortDate").select2("val", "");
	        $("#savedLocationSortName").select2("val", "");
	        $scope.getSavedLocationData();
		}

        $scope.callStartMonitoring = function($event, locationId, locationTitle) {
            modalService.startMonitoringModal($event, $scope, locationId, locationTitle);
            /*if ($scope.isActiveStream.is_active == false) {
                modalService.startMonitoringModal($event, $scope, locationId, locationTitle);
            }
            else {
                modalService.streamAlreadyStart('', $scope, locationId);
            }*/
        }

        $scope.$on('startMonitoringModal', function(event, data) {
            $scope.startMonitoring(data.locationId);
        })

		$scope.startMonitoring = function(locationId) { 
            $scope.preloaderBottom = true;
            SaveLocationService.startMonitoring(locationId,
                function(response) {
                    console.log(response);
                    if(response.status == 202) {
                        $scope.preloaderBottom = false;
                        $scope.getSavedLocationData();
                    }
                },
                function(error) {
                    console.log(error);
                    if(error.data.error.indexOf('You cannot monitor radius larger than') > -1) {
                        $scope.preloaderBottom = false;
                        modalService.monitoringError('', $scope);
                    }
                    if(error.data.error.indexOf('You have reached your limit of active sessions') > -1) {
                        modalService.streamAlreadyStart('', $scope, locationId);
                        $scope.preloaderBottom = false;
                    }
                    if (error.data.error.indexOf('A rule with this value already exists') > -1) {
                        //alert("Here");
                    }
                    $scope.preloaderBottom = false; 
               }
            )
		}

        $scope.streamAlreadyStart = function(locationId) {
            $scope.preloaderBottom = true;           
            SaveLocationService.stopMonitoring($scope.isActiveStream.locationId,
                function(response) {
                    if(response.status == 202) {
                        SaveLocationService.startMonitoring(locationId,
                            function(response) {
                                if(response.status == 202) {
                                    $scope.preloaderBottom = false;
                                    $scope.getSavedLocationData();
                                }
                            },
                            function(error) {
                                console.log(error);
                                if(error.data.error_message.indexOf('You cannot monitor radius larger than') > -1) {
                                    $scope.preloaderBottom = false;
                                    modalService.monitoringError('', $scope);
                                    $scope.getSavedLocationData();
                                }
                                else {
                                   $scope.preloaderBottom = false; 
                                }
                           }
                        )
                    }
                },
                function(error) {
                    $scope.preloaderBottom = false;
                    console.log(error);
               }
            )
        }

        $scope.$on('streamAlreadyStart', function(event, data) {
            $scope.streamAlreadyStart(data.locationId);
        })


        $scope.callStopMonitoring = function($event, locationId, locationTitle) {
            modalService.stopMonitoringModal($event, $scope, locationId, locationTitle);
        }

        $scope.$on('stopMonitoringModal', function(event, data) {
            $scope.stopMonitoring(data.locationId);
        })

		$scope.stopMonitoring = function(locationId) {
			$scope.preloaderBottom = true;
			SaveLocationService.stopMonitoring(locationId,
                function(response) {
                    if(response.status == 202) {
                    	$scope.preloaderBottom = false;
                    	$scope.getSavedLocationData();
                    }
                },
                function(error) {
                    $scope.preloaderBottom = false;
                    console.log(error);
               }
            )
		}

        //Update each 5 sec
        $scope.updateSavedLocationFeed = function() {
            setInterval(function(){
                if ($location.path() == '/locations') {
                    $scope.getSavedLocationData();
                }
            }, 5000);
        }
        $scope.updateSavedLocationFeed();

        $scope.goToLive = function(radius, uom, lat, lng, zoom, id, active) {
            $scope.goToLiveObj = {
                radius: radius,
                uom: uom,
                center: { lat: lat, lng: lng},
                zoom: zoom,
                locationId: id,
                is_active: active
            }
            localStorage.setItem('savedLocation', JSON.stringify($scope.goToLiveObj));
            $location.path('/dashboard');
        }

        $scope.goToRecorded = function(id) {
            $location.path('/locations/' + id);
        }

    }]);
})();
