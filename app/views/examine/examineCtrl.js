(function () {
  'use strict';

  angular.module("exampleApp")
    .controller("examineCtrl", ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
    	$scope.showLessButton = false;
    	$scope.showMoreButton = true;
    	$scope.showLessButtonLinks = false;
    	$scope.showMoreButtonLinks = true;

    	$rootScope.streamToggleCheck = false;
    }]);
})();
