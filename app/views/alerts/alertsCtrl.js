(function () {
  'use strict';

  angular.module("exampleApp")
      .filter('localDateTime', function($filter){
          return function(date) {
              var localDateTime = moment.utc(date).toDate();
              return moment(localDateTime).format('DD/MM/YYYY hh:mmA');
          }
      })
    .controller("alertsCtrl", ['$rootScope', '$scope', 'modalService', 'AlertService', 'SaveLocationService', function($rootScope, $scope, modalService, AlertService, SaveLocationService) {
    	$scope.menuCheckbox = false;
    	$scope.menuLeftCheckbox = false;

    	//ALERT PAGE MODELS
    	$scope.showAlertsList = true;
    	$scope.showAlertPickList = false;
    	$scope.showAddAlertTitle = false;
    	$scope.showAlertOptions = false;

        $scope.showNoAlerts = false;
        $scope.showAlertsPreloader = true;
        $scope.showAlerts = false;

        $rootScope.dashboardCache.stream = false;

    	$scope.disableNext = true;

    	// New task scope
        function initNewAlertParametres() {
            $scope.newAlertTitle = '';
            $scope.newAlertIntervalNumber = null;
            $scope.newAlertIntervalType = null;
            $scope.newAlertPostsNumber = null;
            $scope.newAlertEmail = null;
            $scope.newAlertKeywords = [];
            $scope.newAlertCategories = [];
            $scope.keywordsShow = false;
            $scope.hotspotsShow = false;
            $scope.categoryShow = false;
        }
        initNewAlertParametres();

    	$scope.myOptions = [
		// {id: 1, title: 'Spectrometer'},
		// {id: 2, title: 'Star Chart'},
		// {id: 3, title: 'Laser Pointer'}
		];

		$scope.myConfig = {
			create: true,
			plugins: ['remove_button'],
			valueField: 'id',
			labelField: 'title',
			delimiter: ',',
			onInitialize: function (selectize) {
			}
		};

		$scope.chooseCategory = function (el) {
			var item = el.currentTarget;
            var categoryValue = $(item).attr('category-value');
			if ($(item).hasClass('active-item')) {
				$(item).removeClass('active-item');
                var i = $scope.newAlertCategories.length;
                while(i--) {
                    var category = $scope.newAlertCategories[i];
                    if (category == categoryValue) {
                        $scope.newAlertCategories.splice(i, 1);
                    }
                }
			}
			else {
				$(item).addClass('active-item');
                $scope.newAlertCategories.push(categoryValue);
			}
		};

		$scope.callDeleteAlertModal = function (alertID, $event) {
            var callback = function () {
                var onError = function () {
                    modalService.error($event, "Ops! Something wrong. Couldn't remove Alert.")
                };
                if (alertID) {
                    var onSuccess = function () {
                        $scope.userAlerts = [];
                        $scope.showAlerts = false;
                        $scope.showAlertsPreloader = true;
                        $scope.showNoAlerts = false;
                        getAlerts();
                        modalService.error($event, "Alert successfully removed!");
                    };
                    AlertService.delete(alertID, onSuccess, onError)
                } else {
                    onError();
                }
            };
            modalService.deleteAlertModal($event, $scope, callback);
        };

		function clearFields () {
            initNewAlertParametres();
        }

        $scope.callSuccessCreateAlert = function ($event) {
			if (! $scope.newAlertIntervalNumber || parseInt($scope.newAlertIntervalNumber) < 1) {
				console.log($scope.newAlertIntervalNumber);
                modalService.error($event, 'Interval should be greater then 0.')
			} else if (! $scope.newAlertIntervalType) {
                modalService.error($event, 'You should check interval type.')
            } else if (! $scope.newAlertPostsNumber || parseInt($scope.newAlertPostsNumber) < 1) {
                modalService.error($event, 'Number of posts should be greater then 0.')
            } else if (! $scope.newAlertEmail || $scope.newAlertEmail.length == 0) {
                modalService.error($event, 'You should insert email to notify.')
			} else {
                var data = {
                	title: $scope.newAlertTitle,
					interval: parseInt($scope.newAlertIntervalNumber) * parseInt($scope.newAlertIntervalType),
					posts: parseInt($scope.newAlertPostsNumber),
					notify_to: $scope.newAlertEmail,
					location: $scope.select2,
					active: 'true',
                    query: '',
                    categories: ''
				};

                for (var i=0; i < $scope.newAlertKeywords.length; i++) {
                    if (data.query.length > 0) {
                        data.query += "+";
                    }
                    data.query += encodeURIComponent($scope.newAlertKeywords[i]);
                }

                for (var i=0; i < $scope.newAlertCategories.length; i++) {
                    if (data.categories.length > 0) {
                        data.categories += "+";
                    }
                    data.categories += $scope.newAlertCategories[i];
                }

                var onSuccess = function () {
                    modalService.successCreateAlert($event, $scope);
                    $scope.showAlertsList = true;
                    $scope.showAlertPickList = false;
                    $scope.showAddAlertTitle = false;
                    $scope.showAlertOptions = false;
                    clearFields();
                    getAlerts();
                };
				var onError = function () {
                    modalService.error($event, 'Oops! Something wrong, cannot save Alert!')
                };
				AlertService.post(data, onSuccess, onError);
            }
        };

		// Get alerts and display.
        $scope.userAlerts = [];
        $scope.savedLocations = [];
        $scope.selectedLocation;

		function getSavedLocations(callback) {
            SaveLocationService.getSavedLocationData('note', '',
                function (response) {
                    console.log('saved locations response:', response);
                    $scope.savedLocations = response.data.objects;
                    callback();
                },
                function (error) {
                    console.log(error);
                }
            )
		}

		function getAlerts () {
            AlertService.get(
                function (response) {
                    console.log('alerts response', response);
                    // Get saved location and replace data.location with SavedLocation details.
                    getSavedLocations(function () {
                    	var alerts = [];
                        var _alerts = response.data.objects;
                    	for (var i=0; i < _alerts.length; i++) {
                    		var alert = _alerts[i],
                    			location = false;
                            for (var j=0; j < $scope.savedLocations.length; j++) {
                            	var loc = $scope.savedLocations[j];
								if (loc.resource_uri == alert.location) {
									location = loc;
									break;
								}
                            }
                            if (location) {
                            	alert.location = location;
                            	alerts.push(alert);
							}
                    	}
                        $scope.showAlertsPreloader = false;
                        if (alerts.length === 0) {
                            $scope.showNoAlerts = true;
                            $scope.showAlerts = false;
                        } else {
                            $scope.showNoAlerts = false;
                            $scope.showAlerts = true;
                        }
                        $scope.userAlerts = alerts;
                    });

                },
                function (error) {
                    console.log(error);
                    $scope.userAlerts = [];
                    $scope.showNoAlerts = true;
                    $scope.showAlertsPreloader = false;
                    $scope.showAlerts = false;
                }
            );
        }
        getAlerts();
        setInterval(function() {
            getAlerts();
        }, 10000);


		$scope.onSelectLocation = function () {
            if ($scope.select2.length > 0) {
            	$scope.disableNext = false;
			}
        }
		$scope.onPickLocation = function () {
            for (var j=0; j < $scope.savedLocations.length; j++) {
                var loc = $scope.savedLocations[j];
                if (loc.resource_uri == $scope.select2) {
                    $scope.selectedLocation = loc;
                    break;
                }
            }
            $scope.showAlertsList = false;
            $scope.showAlertPickList = false;
            $scope.showAddAlertTitle = true;
            $scope.showAlertOptions = false;
            $('#addressLine, #addressLine2').text($scope.selectedLocation.note);
            $('#locationTime, #locationTime2').text(new Date($scope.selectedLocation.created).toLocaleString());
        };
		$scope.onSetName = function () {
            $scope.showAlertsList = false;
            $scope.showAlertPickList = false;
            $scope.showAddAlertTitle = false;
            $scope.showAlertOptions = true;
		}
    }]);
})();
