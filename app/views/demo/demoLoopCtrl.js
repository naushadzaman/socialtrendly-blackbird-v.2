(function () {
  'use strict';

  angular.module("exampleApp")
    .controller("demoLoopCtrl", ['$rootScope','$scope', 'modalService', '$mdDialog', 'SaveLocationService', function($rootScope, $scope, modalService, $mdDialog, SaveLocationService) {
    	
    	$rootScope.dashboardCache.stream = false;

    }]);
})();
