(function () {
  'use strict';

  angular.module("exampleApp")
    .controller("demoCtrl", ['$rootScope','$scope', 'modalService', '$mdDialog', 'SaveLocationService', function($rootScope, $scope, modalService, $mdDialog, SaveLocationService) {
    	
    	$rootScope.dashboardCache.stream = false;

    }]);
})();
