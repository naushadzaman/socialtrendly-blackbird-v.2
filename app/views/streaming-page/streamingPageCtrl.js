(function () {
  'use strict';

  angular.module("exampleApp")
  	.filter('formatDate', function($filter){
	    return function(date) {
	            return $filter('date')(date, 'MM/dd/yyyy') ;
	        }
	})
	.filter('formatTime', function($filter){
	    return function(date){
	        return $filter('date')(date, 'h:mm a');
	    }
	})
	.filter('reverse', function() {
      return function(items) {
        return items.slice().reverse();
      }
    })
    .controller("streamingPageCtrl", ['$rootScope','$scope', 'modalService', '$mdDialog', 'SaveLocationService', 'ListService', '$location', function($rootScope, $scope, modalService, $mdDialog, SaveLocationService, ListService, $location) {
    	
    	$scope.savedLocationPreloader = true;
    	$scope.savedLocation = false;
    	$scope.noLocations = false;

    	$scope.listsPreloader = true;
    	$scope.savedList = false;
    	$scope.noLists = false;

        $rootScope.dashboardCache.stream = false;

    	$scope.checkSavedLocationsCount = function(locationCount) {
    		if (locationCount.length > 0) {
    			$scope.savedLocationPreloader = false;
    			$scope.savedLocation = true;
    			$scope.noLocations = false;
    		}
    		else {
    			$scope.savedLocation = false;
				$scope.savedLocationPreloader = false;
				$scope.noLocations = true;
    		}
    	}

    	$scope.getSavedLocationData = function() {
			SaveLocationService.getSavedLocationData($scope.sortModel, 'dashboard',
                function(response) {
                    console.log(response);
                    $scope.checkSavedLocationsCount(response.data.objects);
                    $scope.savedLocationList = response.data.objects;
                },
                function(error) {
                    $scope.savedLocation = false;
					$scope.savedLocationPreloader = false;
					$scope.noLocations = true;
               }
            )
    	}
    	$scope.getSavedLocationData();


    	$scope.chechListData = function(listData) {
	        if (listData > 0) {
	            $scope.listsPreloader = false;
		    	$scope.savedList = true;
		    	$scope.noLists = false;
	        }
	        else {
	            $scope.listsPreloader = false;
		    	$scope.savedList = false;
		    	$scope.noLists = true;
	        }
	    }

    	$scope.getListData = function() {
	        ListService.getListData($scope.sortModel,
	            function(response) {
	                console.log(response);
	                $scope.listsData = response.data.objects;
	                $scope.chechListData(response.data.meta.total_count);
	            },
	            function(error) {
	                $scope.listsPreloader = false;
			    	$scope.savedList = false;
			    	$scope.noLists = true;
	            }
	        )
	    }
	    $scope.getListData();

	    $scope.createNewLocation = function() {
	    	$rootScope.saveNewLocation = true;
	    	$location.path('/dashboard');   
	    }

	    $scope.goToLive = function(radius, uom, lat, lng, zoom, id, active) {
            $scope.goToLiveObj = {
                radius: radius,
                uom: uom,
                center: { lat: lat, lng: lng},
                zoom: zoom,
                locationId: id,
                is_active: active
            }
            localStorage.setItem('savedLocation', JSON.stringify($scope.goToLiveObj));
            $location.path('/dashboard');
        }

    }]);
})();
