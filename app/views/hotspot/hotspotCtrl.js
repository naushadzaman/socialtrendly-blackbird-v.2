(function () {
	'use strict';

	angular.module("exampleApp")
	.filter('formatDate', function($filter){
		return function(date) {
			if (typeof date === 'undefined')
				return;
			/*
			var timeToLocal = date;  
	        timeToLocal = timeToLocal.replace(/T/g, " ");
	        timeToLocal = timeToLocal + ' UTC';
	        timeToLocal = new Date(timeToLocal);

	        var timeToLocalFormated = (
	          timeToLocal.getFullYear() + '-' +
	          ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
	          ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
	          ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
	          ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
	          ("00" + timeToLocal.getSeconds()).slice(-2)
	        );*/
			return $filter('date')(date, 'MM/dd/yyyy') ;
		}
	})
	.filter('formatTime', function($filter){
		return function(date){
			/*var timeToLocal = date;  
	        timeToLocal = timeToLocal.replace(/T/g, " ");
	        timeToLocal = timeToLocal + ' UTC';
	        timeToLocal = new Date(timeToLocal);

	        var timeToLocalFormated = (
	          timeToLocal.getFullYear() + '-' +
	          ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
	          ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
	          ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
	          ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
	          ("00" + timeToLocal.getSeconds()).slice(-2)
	        );*/
			return $filter('date')(date, 'h:mm a');
		}
	})
	.filter('highlight', function($sce) {
		return function(text, phrase) {
			if (phrase.length > 0) {
				for (var i in phrase) {
					if (phrase[i] != 'AND' && phrase[i] != 'OR') {
						text = text.replace(new RegExp('('+phrase[i]+')', 'gi'), '<span class="highlighted">$1</span>');
					}
				} 
				return $sce.trustAsHtml(text);
			}
			else {return text};
		}
	})

	.controller("hotspotCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$window', 'mapService', 'StreamingService','tabService', 'modalService', 'ListService', 'analyticsService', '$timeout', function($rootScope, $scope, $http, $location, $stateParams, $window, mapService, StreamingService, tabService, modalService, ListService, analyticsService, $timeout) {

		$scope.hotspotId = $stateParams.hotspotId;

		$scope.eventErrorMsg = false;
		$scope.eventPreloader = true;
		$scope.noItemsMsg = false;

		$scope.leftBtn = true;
		$scope.rightBtn = false;

		$scope.nextValue = 0;
		$scope.progressBarValue = 0;

		$rootScope.markersArray = [];

		$scope.checkTwitterPins = true;
		$scope.checkInstagramPins = false;

		$scope.socialNewsPopup = false;

		$scope.nextValueCheck = '';

		$scope.paginationPagesArray = [];

		$scope.hotspotMarkersArray = [];

		$scope.paginationPageNum = 10;

		$scope.paginationShow = false;

		var postCount;
		var isMapInited = false;
		var hotspotLocation;
		var mapDrawCheck = true;

		$scope.searchBarHotspotModel = [];
		$scope.searchBarHotspotModel.push('HOTSPOT');

		$scope.optionsSearchBarHotspot = [];
		$scope.optionsSearchBarHotspot.push({value: 'HOTSPOT', content: 'HOTSPOT', type: 'hotspot'});

		$scope.searchCategoryHotspotModel = [];
		$scope.optionsCategoryBarHotspot = [];

		$scope.hotspotTimestampGte = '';
        $scope.hotspotTimestampLte = '';

        // Get hotspot start and end datetime from URL params:
		// 	&timestamp__gte=...&timestamp__lte=...
        var requestParams = $location.search();
        if (typeof requestParams.timestamp__gte !== 'undefined')
        	$scope.hotspotTimestampGte = requestParams.timestamp__gte;
        if (typeof requestParams.timestamp__lte !== 'undefined')
            $scope.hotspotTimestampLte = requestParams.timestamp__lte;

		// var hotspotCategory = "Shooting";

		// $scope.optionsCategoryBarHotspot.push({value: hotspotCategory, content: hotspotCategory.replace(/ /g, ""), type: 'category'});
		// $scope.searchCategoryHotspotModel.push(hotspotCategory);

		$scope.configSearchBarHotspot = {
			create: true,
			plugins: ['remove_button'],
			valueField: 'value',
			labelField: 'content',
			delimiter: '+',
			onInitialize: function(selectize){
			},
			render: {
				item: function(item, escape) {
					if (item.type == 'category') {
						return '<div class="item">'+
						'<img src="../../dist/images/'+item.content+'.png" class="category-tag-image">'+item.value+
						'</div>';
					}
					else if (item.value == 'OR') {
						return '<div class="item special-tag or">'
						+item.value+
						'</div>';
					}
					else if (item.value == 'AND') {
						return '<div class="item special-tag and">'
						+item.value+
						'</div>';
					}
					else if (item.type == 'hotspot') {
						return '<div class="item hotspot-tag negative">HOTSPOT <span class="hotspot-id">'
						+$scope.hotspotId+
						'</span></div>';
					}
					else {
						return '<div class="item">'
						+item.value+
						'</div>';
					}				      	
				}
			}
		};

		// $scope.configSearchBarHotspot = {
		// 	create: true,
		// 	plugins: ['remove_button'],
		// 	valueField: 'value',
		// 	labelField: 'content',
		// 	delimiter: '+',
		// 	onInitialize: function(selectize){
		// 	},
		// 	render: {
		// 		item: function(item, escape) {
		// 			if (item.type == 'category') {
		// 				return '<div class="item">'+
		// 				'<img src="../../dist/images/'+item.content+'.png" class="category-tag-image">'+item.value+
		// 				'</div>';
		// 			}
		// 		}
		// 	}
		// };

		function stringToJSON(string) {
			var query_string = {};
			var query = string;
			var vars = query.split("&");
			for (var i=0;i<vars.length;i++) {
				var pair = vars[i].split("=");
				if (typeof query_string[pair[0]] === "undefined") {
					query_string[pair[0]] = decodeURIComponent(pair[1]);
				} else if (typeof query_string[pair[0]] === "string") {
					var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
					query_string[pair[0]] = arr;
				} else {
					query_string[pair[0]].push(decodeURIComponent(pair[1]));
				}
			} 
			return query_string;
		};

		$scope.drawPagination = function(postCount) {
			$scope.paginationPages = postCount / 9;
			if ($scope.paginationPages % 1 === 0) {
				$scope.paginationPages = parseInt($scope.paginationPages);
			}
			else {
				$scope.paginationPages = parseInt($scope.paginationPages);
				$scope.paginationPages += 2;
			}
			for(var i = 0; i < $scope.paginationPages; i++) {
				var page = {
					index: i,
					pageNum: i+1
				}
				$scope.paginationPagesArray.push(page);
			}
		}

		$scope.markLastPage = function(lastPage) {
			$timeout(function(){
				$('.pagination-item').removeClass('active');
				$('#pageId-'+lastPage).addClass('active');
			}, 100);
		}

		$scope.goToPage = function(pageNum) {
			$('.pagination-item').removeClass('active');
			$('#pageId-'+pageNum).addClass('active');
			mapDrawCheck = true;
			$scope.nextValue = (pageNum*9)-9;
			mapService.clearHotspots();
			mapService.setDefaultPin();
			$scope.hotspotMarkersArray = $scope.hotspotMarkersArray.concat($rootScope.markersArray);
			$rootScope.markersArray = [];
			$scope.paginationBar();
			$scope.getHotspotData();

            if ($scope.tabIndex == 'examine') {
                $scope.callExamineView();
            }
		}

		$scope.paginationPageHighlight = function(nextValue) {
			var nextVal = nextValue/9+1;
			$('.pagination-item').removeClass('active');
			$('#pageId-'+nextVal).addClass('active');

			if($('#pageId-'+nextVal).is(':last-child'))
			{
			    $scope.paginationPageNum += 5;
			}
			if($('#pageId-'+nextVal).is(':first-child') && $scope.paginationPageNum > 10)
			{
			    $scope.paginationPageNum -= 5;
			}
		}

		$scope.callSplitView = function(){
			tabService.splitView();
			mapService.mapUpdate('hotspot', 'mapHotspot');
		}

		$scope.callMapShowFull = function(){
			tabService.mapShowFull();
			mapService.mapUpdate('hotspot', 'mapHotspot');
		}

		$scope.callGalleryShowFull = function(){
			tabService.galleryShowFull();
		}

		$scope.callChangeTab = function(tab) {
			tabService.changeTab(tab);
		}

		$scope.zoomToPostPin = function(LatLng, mapName) {
			mapService.zoomToPostPin(LatLng, mapName);
		}
		$scope.zoomToMap = function(mapName) {
			mapService.zoomToMap(mapName);
		}

		$scope.showPinOnMap = function(postID, mapName) {
			mapService.showPinOnMap(postID, mapName);
		}

		$scope.onePageCheck = function(postCount) {
			if(postCount <= 9 && postCount != 0) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.noPostsMsgCheck = function(postCount) {
			if (postCount == 0) {
				$scope.noPostsMsg = true;
				$scope.leftBtn = true;
				$scope.paginationShow = false;
				$scope.cleanHotspotMarkers();
		    	mapService.clearMarkers();
				mapService.clearHotspots();
			}
			else {
				$scope.noPostsMsg = false;
				$scope.paginationShow = true;
			}
		}

		$scope.showLeftBtn = function(previousValue, postCount) {
			if (previousValue != null) $scope.leftBtn = false;
			else {$scope.leftBtn = true; $scope.progressBarValue = 0;}
		}
		$scope.showRightBtn = function(totalCount) {
			if (totalCount > 9) $scope.rightBtn = false;
			else {$scope.rightBtn = true; $scope.paginationShow = false;}
		}

		$scope.drawMarkers = function() {
			for (var item in $scope.eventData) {
				mapService.drawMarker('mapHotspot', 'circleHotspot', $scope, $scope.hotspotData[item].location, $scope.hotspotData[item].platform, $scope.hotspotData[item].category, $scope.hotspotData[item].post_id, $scope.hotspotData[item].content, $scope.hotspotData[item].user_profile_image_url, $scope.hotspotData[item].user_name, $scope.hotspotData[item].media_url);
			}
		}

		$scope.paginationBar = function() {
			$scope.progressBarValue = ($scope.nextValue*100)/postCount;
			$scope.progressBarValue = $scope.progressBarValue.toFixed(1);
			if($scope.progressBarValue > 100) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.paginationRight = function() {
			if ($scope.nextValueCheck != null) {
				mapDrawCheck = true;
				$scope.nextValue += 9;
				mapService.clearHotspots();
				mapService.setDefaultPin();
				$scope.hotspotMarkersArray = $scope.hotspotMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getHotspotData();
			}
		}
		$scope.paginationLeft = function() {
			if($scope.nextValue > 0) {
				mapDrawCheck = false;
				$scope.nextValue -= 9;
				mapService.setDefaultPin();
				$scope.hotspotMarkersArray = $scope.hotspotMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getHotspotData();
			}
		}

		$scope.sortByDate = function() {
			$scope.cleanHotspotMarkers();
	    	mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1); 
		}
		$scope.sortBySentiment = function() {
			$scope.cleanHotspotMarkers();
	    	mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0; 
			$scope.paginationPageNum = 10;
			$scope.goToPage(1); 
		}
		$scope.sortByPostType = function() {
			$scope.cleanHotspotMarkers();
	    	mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0; 
			$scope.paginationPageNum = 10;
			$scope.goToPage(1); 
		}

		$scope.gallerySearch = function() {
			if ($scope.searchBarHotspotModel.indexOf('HOTSPOT') > -1) {
				mapService.clearMarkers();
				mapService.clearHotspots();
				$scope.nextValue = 0;
				$scope.progressBarValue = 0; 
				$scope.paginationPageNum = 10;
				$scope.goToPage(1); 
			}
			else {
				if ($rootScope.history.length > 1) {
					$location.path($rootScope.history[$rootScope.history.length-2]);
				}
				else {
					$location.path('/dashboard');
				}
			}
		}

		$scope.chooseCategory = function($event) {
			$scope.cleanHotspotMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$rootScope.markersArray = [];
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			if ($event != undefined) {
				var categoryValue = $event.currentTarget.innerText;
				categoryValue = categoryValue.replace(/ /g, "");
				$scope.showCategorySelector = !$scope.showCategorySelector;
				$scope.optionsCategoryBarHotspot.push({value: $event.currentTarget.innerText, content: categoryValue, type: 'category'});
				$scope.searchCategoryHotspotModel.push($event.currentTarget.innerText);
			}
			$scope.goToPage(1); 
		}

		$scope.checkLastPage = function(lastPage) {
			if (lastPage == null) {
				$scope.rightBtn = true;
				$scope.progressBarValue = 100;
			}
		}

		function checkHotspotUnit(radiusUnit) {
			if (radiusUnit == undefined) return 'mi';
        	else return radiusUnit;
		}

		function checkSortByDate(date) {
	      //oldest first
	      if (date == undefined) return '-timestamp';
	      else return date;
	    }

	    $scope.timestampFilter = function() {
			$scope.cleanHotspotMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.filterPopup = !$scope.filterPopup;
			$scope.goToPage(1);
		}

		$scope.getHotspotData = function() {
			if (isMapInited == false) {
				StreamingService.getHotspotData($scope.hotspotId,
					function(response) {
                        console.log('Call /' + $scope.hotspotId + '/. Response:', response);

						$scope.eventErrorMsg = false;

                        if (typeof response.data.meta.request_params !== 'undefined') {
                            hotspotLocation = response.data.meta.request_params;
                        } else if (response.data.meta.next) {
                            hotspotLocation = stringToJSON(response.data.meta.next);
						} else {
							console.log("Cannot get event location from server!");
							return;
						}

						if ($scope.hotspotTimestampGte.length == 0
								&& typeof (hotspotLocation.timestamp__gte) !== 'undefined')
                            $scope.hotspotTimestampGte = hotspotLocation.timestamp__gte;

                        if ($scope.hotspotTimestampLte.length == 0
                            && typeof (hotspotLocation.timestamp__lte) !== 'undefined')
                            $scope.hotspotTimestampLte = hotspotLocation.timestamp__lte;

						mapService.mapInitHotspot(hotspotLocation.lat, hotspotLocation.lng,
							hotspotLocation.radius, checkHotspotUnit(hotspotLocation.uom), 14
						);
						initExamineTab(hotspotLocation);

						$scope.drawPagination(response.data.meta.total_count);

						$scope.getHotspotData();

						$scope.getListData();
					},
					function(error) {
						console.log(error);
						$scope.eventPreloader = false;
						$scope.eventErrorMsg = true;
						$scope.rightBtn = true;
					}
				)
				isMapInited = true;			
			}
			else {
				try {
                    var timestampGte = $scope.hotspotDateTimeFromObj.toISOString().replace("Z", "");
                } catch (e) {
                    var timestampGte = $scope.hotspotTimestampGte;
				}
                try {
                    var timestampLte = $scope.hotspotDateTimeToObj.toISOString().replace("Z", "");
                } catch (e) {
                    var timestampLte = $scope.hotspotTimestampLte;
                }
				StreamingService.getHotspotDataInit(
					hotspotLocation.lat, hotspotLocation.lng, hotspotLocation.radius,
					hotspotLocation.uom, $scope.nextValue, checkSortByDate($scope.hotspotSortByDate),
					$scope.hotspotSortBySentiment, $scope.searchBarHotspotModel, $scope.searchCategoryHotspotModel,
					$scope.postTypeSelect, $scope.checkTwitterPins, $scope.checkInstagramPins,
                    timestampGte, timestampLte,
					function(response) {
						console.log(response);
						$scope.eventPreloader = false;
						$scope.hotspotData = response.data.objects;
						$scope.hotspotInfo = response.data.meta.next;

						$scope.onePageCheck(response.data.meta.total_count);

						$scope.noPostsMsgCheck(response.data.meta.total_count);

						$scope.showLeftBtn(response.data.meta.previous);
						$scope.showRightBtn(response.data.meta.total_count);

						postCount = response.data.meta.total_count;
						$scope.eventPreloader = false;
						$scope.eventData = response.data.objects;
						$scope.eventHotspots = response.data.hotspots;

						if (response.data.meta.total_count > 90) {
							$scope.drawPagination(response.data.meta.total_count);
						}

						$scope.nextValueCheck = response.data.meta.next;
						$scope.checkLastPage($scope.nextValueCheck);

						$scope.drawMarkers();
					},
					function(error) {
						console.log(error);
						$scope.eventPreloader = false;
						$scope.eventErrorMsg = true;
						$scope.rightBtn = true;
					}
				)
			}
		}

		$scope.getHotspotCategory = function() {
			var hotspotCategory = sessionStorage.getItem('hotspotCategory');
			if (hotspotCategory != null) {
				hotspotCategory = JSON.parse(hotspotCategory);
				console.log(hotspotCategory);
				$scope.searchCategoryHotspotModel = hotspotCategory;
				for (var cat in hotspotCategory) {
					$scope.optionsCategoryBarHotspot.push({value: hotspotCategory[cat], content: hotspotCategory[cat], type: 'category'});
				}
				sessionStorage.removeItem('hotspotCategory');
				setTimeout(function(){
					$scope.getHotspotData();
				},100);
			}
			else {
				$scope.getHotspotData();
			}
		}
		$scope.getHotspotCategory();


		//
    // MODALS SERVICE FUNCTIONS
    //

    $scope.annotatePost = function($event) {
    	modalService.annotatePost($event, $scope);
    }

		$scope.callDatePickerModal = function($event) {
			modalService.datePickerModal($event, $scope);
		}

		$scope.cleanHotspotMarkers = function() {
			for(var marker in $scope.hotspotMarkersArray) {
				$scope.hotspotMarkersArray[marker].setMap(null);
			}
			$scope.hotspotMarkersArray = [];
		}

		//Show/Hide Twiiter/Instagram pins on map
		$scope.displayTwitterPins = function($event) {
			$scope.cleanHotspotMarkers();
			mapService.clearMarkers();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.searchCategoryHotspotModel = [];
			$scope.goToPage(1); 
		}
		$scope.displayInstagramPins = function($event) {
			$scope.cleanHotspotMarkers();
			mapService.clearMarkers();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.searchCategoryHotspotModel = [];
			$scope.goToPage(1); 
		}

		$rootScope.keyPress = function(keyCode) {
			if (keyCode == 39) { // check if "Right Arrow" key
				$scope.paginationRight();
		}
			else if (keyCode == 37) { // check if "Left Arrow" key
				$scope.paginationLeft();
		}
	}

	var initNewUserList = false;
	$scope.createNewUserList = function($event) {
    	if ($scope.userListData.length == 0) {
    		modalService.callCreateNewUserList($event, $scope);
    	}
    }

	$scope.getListData = function() {
        ListService.getListData($scope.sortModel,
            function(response) {
                console.log(response);
                $rootScope.userListData = response.data.objects;
                if (initNewUserList == true) {
                	if (response.data.objects.length == 0) {
	                	$scope.createNewUserList();
	                }
                }
                initNewUserList = true;
            },
            function(error) {
                console.log(error);
            }
        )
    }

    $scope.addPostToList = function(postId, listId) {
        if (listId != null) {
            $rootScope.postsId.push(postId);
            ListService.addPostToList(postId, listId,
                function(response) {
                    console.log(response);
                },
                function(error) {
                    console.log(error);
                }
            )
        }
    }

    $scope.showFullImg = function($event, image, post) {
    	modalService.showFullImage($event, $scope, image, post);
    }

    $(document).on("click", ".map-show-full-img", function() {
      var image = $(this).next().attr("src");
      modalService.showFullImage('', $scope, image);
    });

    $(document).on("click", ".pagination-list li:nth-child(1)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	} 
    });
    $(document).on("click", ".pagination-list li:nth-child(2)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	} 
    });
    $(document).on("click", ".pagination-list li:nth-child(3)", function() {
    	if ($scope.paginationPageNum > 10) {
    		$scope.paginationPageNum -= 1;
    	} 
    });

    $(document).on("click", ".pagination-list li:nth-child(10)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(9)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });
    $(document).on("click", ".pagination-list li:nth-child(8)", function() {
    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
    		$scope.paginationPageNum += 1;
    	}
    });

	// Initialize examine tab on current controller;
    var examineTabIsInitialized = false;
    function initExamineTab(nextQueryParams) {
        $scope.examineHidePreInfo = true;
        $scope.examineHideInfo = false;
		if (examineTabIsInitialized)
			return;

		// Live data.
		var now = new Date();

		$scope.analyticsExtraFiltres = {
			timestamp__gte: $scope.hotspotTimestampGte,
			timestamp__lte: $scope.hotspotTimestampLte
		}

		if ($scope.hotspotTimestampLte.length == 0)
            $scope.analyticsExtraFiltres.timestamp__lte = analyticsService.dateToString(now, 'UTC');

		// Check if live stream start lesser than 50 mins ago, than add
		// extended_bounds_min parameter.
		if ($scope.hotspotTimestampGte.length > 0) {
			var fiftyMinsBefore = new Date(now.getUTCFullYear(), now.getUTCMonth(),
				now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(),
				now.getUTCSeconds());
			fiftyMinsBefore.setMinutes(fiftyMinsBefore.getMinutes() - 50);

			var diff = new Date($scope.hotspotTimestampGte).getTime() - fiftyMinsBefore.getTime();
			if (diff > 0) {
				$scope.analyticsExtraFiltres.extended_bounds_min =
					analyticsService.dateToString(fiftyMinsBefore, false);
			}
		}

		$scope.analyticsExtraFiltres.lng = nextQueryParams.lng;
		$scope.analyticsExtraFiltres.lat = nextQueryParams.lat;
		$scope.analyticsExtraFiltres.radius = nextQueryParams.radius;
		$scope.analyticsExtraFiltres.uom = checkHotspotUnit(nextQueryParams.uom);
	}
	analyticsService.examineTab($scope, {
        searchBarFieldName: 'searchBarHotspotModel',
        categoryClassifierFieldName: 'searchCategoryHotspotModel',
        dateTimeFromFieldName: 'hotspotDateTimeFromObj',
        dateTimeToFieldName: 'hotspotDateTimeToObj'
	});

	if ($scope.tabIndex == 'examine') {
		$scope.callExamineView();
	}
}]);

})();

