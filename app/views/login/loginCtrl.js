(function () {
  'use strict';

  angular.module("exampleApp")
  .controller("loginCtrl", ['$rootScope', '$scope', '$http', 'AuthenticationService', '$location', 'modalService', function($rootScope, $scope, $http, AuthenticationService, $location, modalService) {

  	$scope.showLoadingMessage = false;
  	$scope.showErrorMessage = false;
  	$scope.showRegistrationWindow = false;
  	$scope.showLoginWindow = true;
  	$scope.showApprovalWindow = false;
  	$scope.showRedirectMessage = false;
    $scope.showInvalidMessage = false;
  	$scope.showLevel = false;
  	$scope.showLoadingRegMessage = false;

    $scope.passwordStrengthValue = '';

    $scope.user = {
      username: "",
      password: ""
    };

    $scope.registrationUser = {
    	first_name: "",
    	last_name: "",
    	email: "",
     password: "",
     company_name: ""
   };

   (function initController() {
        //reset login status
        AuthenticationService.RemoveCredentials();
      })();

      $scope.login = function() {
        if ($scope.user.username != '' && $scope.user.password != '') {
         $scope.showLoadingMessage = true;
         AuthenticationService.Login($scope.user,
           function(response) {
             console.log(response);
             // response status check changed
             if ( response.status == '200' ) {
               AuthenticationService.SetCredentials($scope.user.username, response.data.api_key);
               $scope.showLoadingMessage = false;
               $scope.showRedirectingMessage = true;
               $location.path('/info');
             }
             $scope.showLoadingMessage = false;
           },
           function(error) {
            if ( error.status == '401') {
              $scope.showLoadingMessage = false;
              $scope.showInvalidMessage = true;
            }
            else {
              $scope.showLoadingMessage = false;
              $scope.showErrorMessage = true;
            }
            console.log(error);
          })
       }
       else {
        $scope.showLoadingMessage = false;
        $scope.showErrorMessage = true;
      }
    }

    $scope.registration = function() {
    	$scope.showLoadingRegMessage = true;
    	AuthenticationService.Registration($scope.registrationUser,
    		function(response) {
    			console.log(response);
    			if (response.status == '201') {
    				$scope.showLoadingRegMessage = false;
    				$location.path('/approval');
          		}
        	},
        	function(error) {
        	$scope.showLoadingRegMessage = false;
         	$scope.showErrorMessage = true;
         	console.log(error);
       })
    }

    $scope.hideMessages = function() {
      $scope.showLoadingMessage = false;
      $scope.showErrorMessage = false;
      $scope.showInvalidMessage = false;
    }

   $scope.hideStrengthLevel = function() {
      if ($scope.registrationUser.password == null) {
        $scope.showLevel = false;
      }
      else {
        $scope.showLevel = true;
      }
   }

   $scope.clearInput = function() {
     $scope.registrationUser.first_name = null;
     $scope.registrationUser.last_name = null;
     $scope.registrationUser.email = null;
   }

   $scope.passwordStrength = function(pass) {
     var score = 0;
     if (!pass)
       return score;
	    // award every unique letter until 5 repetitions
	    var letters = new Object();
	    for (var i=0; i<pass.length; i++) {
       letters[pass[i]] = (letters[pass[i]] || 0) + 1;
       score += 5.0 / letters[pass[i]];
     }
	    // bonus points for mixing it up
	    var variations = {
       digits: /\d/.test(pass),
       lower: /[a-z]/.test(pass),
       upper: /[A-Z]/.test(pass),
       nonWords: /\W/.test(pass),
     }
     var variationCount = 0;
     for (var check in variations) {
       variationCount += (variations[check] == true) ? 1 : 0;
     }
     score += (variationCount - 1) * 10;
     if(score > 100) {
      score = 100;
    }
    score = parseInt(score);
    if (score > 80) {
     $scope.passwordStrengthValue = "excellent";
   }
   else if (score > 60){
     $scope.passwordStrengthValue = "strong";
   }
   else if (score > 40){
     $scope.passwordStrengthValue = "good";
   }
   else if (score > 20){
     $scope.passwordStrengthValue = "medium";
   }
   else if (score > 1){
     $scope.passwordStrengthValue = "weak";
   }
 }

}]);
})();
