(function () {
  'use strict';

  angular.module("exampleApp")
  .filter('formatDate', function($filter){
        return function(date) {
                return $filter('date')(date, 'MM/dd/yyyy') ;
            }
    })
    .filter('formatTime', function($filter){
        return function(date){
            return $filter('date')(date, 'h:mm a');
        }
    })
    .filter('reverse', function() {
      return function(items) {
        return items.slice().reverse();
      }
    })
    .filter('reverse', function() {
      return function(items) {
        return items.slice().reverse();
      }
    })
    .controller("listDetailedCtrl", ['$rootScope', '$scope', '$http', '$stateParams', 'ListService', 'modalService', function($rootScope, $scope, $http, $stateParams, ListService, modalService) {

    	$scope.listId = $stateParams.listId;
    	$scope.itemList = false;
    	$scope.itemListPreloader = true;
    	$scope.noItemMsg = false;

        $scope.preloaderActive = false;

    	$scope.checkList = function(itemList) {
    		if (itemList.length > 0) {
    			$scope.itemList = true;
    			$scope.itemListPreloader = false;
    			$scope.noItemMsg = false;
    			$scope.postsCount = itemList.length;
                $scope.preloaderActive = false;
    		}
    		else {
    			$scope.itemList = false;
    			$scope.itemListPreloader = false;
    			$scope.noItemMsg = true;
    			$scope.postsCount = 0;
                $scope.preloaderActive = false;
    		}
    	}

    	$scope.getPostsId = function() {
            $scope.postsId = [];
    		for (var postId in $scope.listItemFull) {
    			$scope.postsId.push($scope.listItemFull[postId].post_id);
    		}
    	}

    	$scope.getListDataFull = function() {
    		ListService.getListDataFull($scope.listId,
	            function(response) {
	                console.log(response);
	                $scope.checkList(response.data.objects);
	                $scope.listName = response.data.name;
	                $scope.listItemFull = response.data.objects;

	                $scope.getPostsId();
	            },
	            function(error) {
	                console.log(error);
	                $scope.itemList = false;
    				$scope.itemListPreloader = false;
    				$scope.noItemMsg = true;
	            }
	        )
    	}
    	$scope.getListDataFull();

    	$scope.callDeletePostFromList = function($event, postId) {
	        modalService.deletePostFromList($event, $scope, postId);
	    }

	    $scope.$on('deletePostFromList', function(event, postId) {
	        $scope.deletePostFromList(postId.postId);
	    })

	    $scope.deletePostFromList = function(postId) {
	    	$scope.preloaderActive = true;
            console.log($scope.postsId);
            console.log(postId);
            for (var post in $scope.postsId) {
                if ($scope.postsId[post] == postId) {
                    $scope.postsId.splice(post, 1);
                }
            }
            console.log($scope.postsId);
    		ListService.deletePostFromList($scope.listId, $scope.postsId,
	            function(response) {
                    console.log(response);
	                $scope.getListDataFull();
	            },
	            function(error) {
	                console.log(error);
                    $scope.itemListPreloader = false;
	            }
	        )
    	}

        $scope.printPage = function() {
            window.print();
        }
    	
    }]);
})();
