(function () {
  'use strict';

  angular.module("exampleApp")
    .controller("registerCtrl", ['$scope', '$http', 'UserService', 'AuthenticationService', '$location', function($scope, $http, UserService, AuthenticationService, $location) {

    // MODEL FOR USER:
    $scope.user = {
      firstName: "",
      email: "",
      username: "",
      password: ""
    };

    (function initController() {
        // reset login status
        AuthenticationService.RemoveCredentials();
    })();

    $scope.register = function() {
      $scope.dataLoading = true;
      UserService.Create($scope.user).then(function(data) {
        if (data.success) {
          $location.path('/login');
        }
        $scope.dataLoading = false;
      })
    }
    }]);

})();
