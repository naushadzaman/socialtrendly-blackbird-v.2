(function () {
	'use strict';

	angular.module("exampleApp")
	.filter('formatDateRecorded', function($filter){
    return function(date) {
    		/*var timeToLocal = date;  
    		timeToLocal = timeToLocal.replace(/T/g, " ");
    		timeToLocal = timeToLocal + ' UTC';
	        timeToLocal = new Date(timeToLocal);

			var timeToLocalFormated = (
			  timeToLocal.getFullYear() + '-' +
			  ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
			  ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
			  ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
			  ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
			  ("00" + timeToLocal.getSeconds()).slice(-2)
			);*/
	        return $filter('date')(date, 'MM/dd/yyyy') ;
        }
  })
  .filter('formatTimeRecorded', function($filter){
    return function(date){
    	/*var timeToLocal = date;  
		timeToLocal = timeToLocal.replace(/T/g, " ");
		timeToLocal = timeToLocal + ' UTC';
        timeToLocal = new Date(timeToLocal);

		var timeToLocalFormated = (
		  timeToLocal.getFullYear() + '-' +
		  ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
		  ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
		  ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
		  ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
		  ("00" + timeToLocal.getSeconds()).slice(-2)
		);*/
    	return $filter('date')(date, 'h:mm a');
    }
  })
  .filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase.length > 0) {
        for (var i in phrase) {
        	if (phrase[i] != 'AND' && phrase[i] != 'OR') {
        		text = text.replace(new RegExp('('+phrase[i]+')', 'gi'), '<span class="highlighted">$1</span>');
        	}
        } 
        return $sce.trustAsHtml(text);
      }
      else {return text};
    }
  })
	.controller("recordedCtrl", ['$rootScope', '$scope', '$http', 'mapService', 'tabService', 'modalService', 'StreamingService', 'ListService', 'analyticsService', '$window', function($rootScope, $scope, $http, mapService, tabService, modalService, StreamingService, ListService, analyticsService, $window) {
		
		$rootScope.tabIndex = 'split-view';

		tabService.splitView();

		(function callCleanDefault() {
			mapService.cleanDefaultMapValue();
		})();

		$scope.checkTwitterPins = true;
		$scope.checkInstagramPins = false;

		$scope.eventErrorMsg = false;
		$scope.eventPreloader = true;
		$scope.noItemsMsg = false;

		$scope.leftBtn = true;
		$scope.rightBtn = false;

		$scope.nextValue = 0;
		$scope.progressBarValue = 0;

		$rootScope.markersArray = [];

		$scope.checkTwitterPins = true;
		$scope.checkInstagramPins = false;

		$scope.socialNewsPopup = false;

		$scope.nextValueCheck = '';

		$scope.paginationPagesArray = [];

		$scope.paginationPageNum = 10;

		$scope.eventMarkersArray = [];

		$scope.paginationShow = false;

        $scope.currentPageNum = 1;

        $scope.categories = ["Violence", "Shooting", "ActiveShooter", "Terrorism", "Disaster", "Generic"];

		var postCount;

		var isMapInited = false;
		var mapDrawCheck = true;

		//
		// SELECTIZE INIT
		//

		$scope.searchbarModelRecorded = [];

		$scope.optionsSearchBarRecorded = [];

		$scope.searchCategoryRecordedModel = [];

		$scope.optionsCategoryBarRecorded = [
		];

		$scope.configSearchbarRecorded = {
			create: true,
			plugins: ['remove_button'],
			valueField: 'value',
			labelField: 'content',
			delimiter: ',',
			onInitialize: function(selectize){
			},
			render: {
		      item: function(item, escape) {
		      	if (item.type == 'category') {
		      		return '<div class="item">'+
		        	'<img src="../../dist/images/'+item.value+'.png" class="category-tag-image">'+item.value+
		        	'</div>';
		      	}
		      	else {
		      		return '<div class="item">'
		        		+item.value+
		        	'</div>';
		      	}				      	
		      }
		    }
		};

		//
		//	Category selector functionality
		//

		var initNewUserList = false;
		$scope.createNewUserList = function($event) {
	    	if ($scope.userListData.length == 0) {
	    		modalService.callCreateNewUserList($event, $scope);
	    	}
	    }

		$scope.getListData = function() {
	        ListService.getListData($scope.sortModel,
	            function(response) {
	                console.log(response);
	                $rootScope.userListData = response.data.objects;
	                if (initNewUserList == true) {
	                	if (response.data.objects.length == 0) {
		                	$scope.createNewUserList();
		                }
	                }
	                initNewUserList = true;
	            },
	            function(error) {
	                console.log(error);
	            }
	        )
	    }

	    $scope.addPostToList = function(postId, listId) {
	        if (listId != null) {
	            $rootScope.postsId.push(postId);
	            ListService.addPostToList(postId, listId,
	                function(response) {
	                    console.log(response);
	                },
	                function(error) {
	                    console.log(error);
	                }
	            )
	        }
	    }

	    function stringToJSON(string) {
			var query_string = {};
			var query = string;
			var vars = query.split("&");
			for (var i=0;i<vars.length;i++) {
				var pair = vars[i].split("=");
				if (typeof query_string[pair[0]] === "undefined") {
					query_string[pair[0]] = decodeURIComponent(pair[1]);
				} else if (typeof query_string[pair[0]] === "string") {
					var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
					query_string[pair[0]] = arr;
				} else {
					query_string[pair[0]].push(decodeURIComponent(pair[1]));
				}
			} 
			return query_string;
		}

	    $scope.onePageCheck = function(postCount) {
			if(postCount <= 9 && postCount != 0) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.showLeftBtn = function(previousValue) {
			if (previousValue != null) $scope.leftBtn = false;
			else {$scope.leftBtn = true; $scope.progressBarValue = 0;}
		}
		$scope.showRightBtn = function(totalCount) {
			if (totalCount > 9) $scope.rightBtn = false;
			else {$scope.rightBtn = true; $scope.paginationShow = false;}
		}

		$scope.noPostsMsgCheck = function(postCount) {
			if (postCount == 0) {
				$scope.noPostsMsg = true;
				$scope.paginationShow = false;
				$scope.cleanEventMarkers();
		    	mapService.clearMarkers();
				mapService.clearHotspots();
			}
			else {
				$scope.noPostsMsg = false;
				$scope.paginationShow = true;
			}
		}	

		$scope.cleanEventMarkers = function() {
			for(var marker in $scope.eventMarkersArray) {
				$scope.eventMarkersArray[marker].setMap(null);
			}
			$scope.eventMarkersArray = [];
		}

		$scope.drawPagination = function(postCount) {
			$scope.paginationPages = postCount / 9;
			if ($scope.paginationPages % 1 === 0) {
				$scope.paginationPages = parseInt($scope.paginationPages);
			}
			else {
				$scope.paginationPages = parseInt($scope.paginationPages);
				$scope.paginationPages += 2;
			}
			for(var i = 0; i < $scope.paginationPages; i++) {
				var page = {
					index: i,
					pageNum: i+1
				}
				$scope.paginationPagesArray.push(page);
			}
		}

		$scope.paginationBar = function() {
			$scope.progressBarValue = ($scope.nextValue*100)/postCount;
			$scope.progressBarValue = $scope.progressBarValue.toFixed(1);
			if($scope.progressBarValue > 100) {
				$scope.progressBarValue = 100;
			}
		}

		$scope.markLastPage = function(lastPage) {
			$timeout(function(){
				$('.pagination-item').removeClass('active');
				$('#pageId-'+lastPage).addClass('active');
			}, 100);
		}

		$scope.goToPage = function(pageNum) {
			$('.pagination-item').removeClass('active');
			$('#pageId-'+pageNum).addClass('active');
			mapDrawCheck = true;
			$scope.nextValue = (pageNum*9)-9;
            $scope.currentPageNum = pageNum;
			mapService.clearHotspots();
			mapService.setDefaultPin();
			$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
			$rootScope.markersArray = [];
			$scope.paginationBar();
			$scope.getRecordedData();

            if ($scope.tabIndex == 'examine') {
                $scope.callExamineView();
            }
		}

		$scope.paginationPageHighlight = function(nextValue) {
			var nextVal = nextValue/9+1;
			$('.pagination-item').removeClass('active');
			$('#pageId-'+nextVal).addClass('active');

			if($('#pageId-'+nextVal).is(':last-child'))
			{
			    if ($scope.paginationPageNum < $scope.paginationPages-1) {
		    		$scope.paginationPageNum += 5;
		    	}
			}
			if($('#pageId-'+nextVal).is(':first-child') && $scope.paginationPageNum > 10)
			{
			    $scope.paginationPageNum -= 5;
			}
		}

		function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        $scope.radiusInMeters = function(radius, unit) {
            if (unit == 'mi') {
                return radius * 1609.34; 
            }
            else {
                return radius * 0,3048; 
            }
        }


		$scope.drawMarkers = function() {
			for (var item in $scope.recordedData) {
				var distance = 1000 * getDistanceFromLatLonInKm($scope.circleData.lat, $scope.circleData.lng, $scope.recordedData[item].location[1], $scope.recordedData[item].location[0]);
	            var radius = $scope.radiusInMeters($scope.circleData.radius, $scope.circleData.uom);
	            if (radius >= distance) {
	                mapService.drawMarker('mapRecorded', 'circleRecorded', $scope, $scope.recordedData[item].location, $scope.recordedData[item].platform, $scope.recordedData[item].category, $scope.recordedData[item].post_id, $scope.recordedData[item].text, $scope.recordedData[item].user_profile_image_url, $scope.recordedData[item].user_name, $scope.recordedData[item].media_url);
	            }
			}
		}
		$scope.drawHotspots = function() {
			mapService.clearHotspots();
			for (var hotspot in $scope.recordedHotspot) {
				var distance = 1000 * getDistanceFromLatLonInKm($scope.circleData.lat, $scope.circleData.lng, $scope.recordedHotspot[hotspot].location[1], $scope.recordedHotspot[hotspot].location[0]);
	            var radius = $scope.radiusInMeters($scope.circleData.radius, $scope.circleData.uom);
	            if (radius >= distance) {
	            	mapService.drawHotspots('mapRecorded', 'circleRecorded', $scope.recordedHotspot[hotspot].location, $scope.recordedHotspot[hotspot].doc_count.total, $scope.recordedHotspot[hotspot].doc_count.platform.twitter, $scope.recordedHotspot[hotspot].doc_count.platform.instagram, $scope.recordedHotspot[hotspot].key, $scope.recordedHotspot[hotspot].avg_sentiment.value, $scope.recordedHotspot[hotspot].doc_count.category, $scope.recordedHotspot[hotspot].top_category, $scope.recordedHotspot[hotspot].redirect_to);
	            }
			}
		}

		$scope.zoomToPostPin = function(LatLng, mapName) {
			mapService.zoomToPostPin(LatLng, mapName);
		}
		$scope.zoomToMap = function(mapName) {
			mapService.zoomToMap(mapName);
		}

		$scope.showPinOnMap = function(postID, mapName) {
			mapService.showPinOnMap(postID, mapName);
		}

		$scope.checkLastPage = function(lastPage) {
			if (lastPage == null) {
				$scope.rightBtn = true;
				$scope.progressBarValue = 100;
			}
		}

		$scope.sortByDate = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}
		$scope.sortBySentiment = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}

		$scope.sortByPostType = function() {
			$scope.cleanEventMarkers();
    		mapService.clearMarkers();
    		mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}

		$scope.gallerySearch = function() {
			$scope.cleanEventMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.goToPage(1);
		}

		$scope.timestampFilter = function() {
			$scope.cleanEventMarkers();
			mapService.clearMarkers();
			mapService.clearHotspots();
			$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.filterPopup = !$scope.filterPopup;
			$scope.goToPage(1);
		}

		$scope.checkSortByDate = function(date) {
			if (date == undefined) return '-timestamp';
        	else return date;
		}

		$scope.getRecordedData = function(initTime, nowTime, radius, uom, lat, lng, zoom) {
			StreamingService.getRecordedData($scope.initRecordedData.initTimestamp, $scope.initRecordedData.initDate, $scope.initRecordedData.initRadius, $scope.initRecordedData.initUom, $scope.initRecordedData.initLat, $scope.initRecordedData.initLng, $scope.nextValue, $scope.checkSortByDate($scope.eventSortByDate), $scope.eventSortBySentiment, $scope.postTypeSelect, $scope.searchbarModelRecorded, $scope.checkTwitterPins, $scope.checkInstagramPins, $scope.searchCategoryRecordedModel, $scope.eventDateTimeFromObj, $scope.eventDateTimeToObj,
                function(response) {
                    console.log(response);
                    if (isMapInited == false) {
                    	$scope.circleData = {
							radius: radius,
							uom: uom,
							lat: lat,
							lng: lng
						}
                    	mapService.mapInitRecorded(lat, lng, radius, uom, zoom);
                    	$scope.drawPagination(response.data.meta.total_count);
                    	$scope.getListData();
                    }
                    isMapInited = true;

                    $scope.eventPreloader = false;
                    $scope.eventErrorMsg = false;

                    postCount = response.data.meta.total_count;

                    $scope.onePageCheck(response.data.meta.total_count);

					$scope.noPostsMsgCheck(response.data.meta.total_count);

					$scope.showLeftBtn(response.data.meta.previous);
					$scope.showRightBtn(response.data.meta.total_count);

					if (response.data.meta.total_count > 90) {
						$scope.drawPagination(response.data.meta.total_count);
					}

					$scope.recordedData = response.data.objects;
					$scope.recordedHotspot = response.data.hotspots;

					$scope.nextValueCheck = response.data.meta.next;
					$scope.checkLastPage($scope.nextValueCheck);

					$scope.drawMarkers();
					if(mapDrawCheck == true) {
						$scope.drawHotspots();
					}
                },
                function(error) {   
                    console.log(error);
                    $scope.eventPreloader = false;
					$scope.eventErrorMsg = true;
					$scope.rightBtn = true;
               }
            )
		}

		function initTimestampToLocal(date) {
			var timeToLocal = date;  
    		timeToLocal = timeToLocal.replace(/T/g, " ");
    		timeToLocal = timeToLocal + ' UTC';
	        timeToLocal = new Date(timeToLocal);

			var timeToLocalFormated = (
			  timeToLocal.getFullYear() + '-' +
			  ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
			  ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
			  ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
			  ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
			  ("00" + timeToLocal.getSeconds()).slice(-2)
			);

			return timeToLocalFormated;
		}

		function checkQuery(query) {
			if (query != '') {
				var queryVal = query.split(':');
				$scope.searchbarModelRecorded = queryVal;
				for (var i in $scope.searchbarModelRecorded) {
					$scope.optionsSearchBarRecorded.push({value: $scope.searchbarModelRecorded[i], content: $scope.searchbarModelRecorded[i], type: ''});
				}
				return queryVal;
			}
			else return '';
		}

		function checkCategoryQuery(query) {
			if (query != 'undefined') {
				if (query != '') {
					var queryValCat = query.split(':');
					$scope.searchCategoryRecordedModel = queryValCat;
					for (var i in $scope.searchCategoryRecordedModel) {
						$scope.optionsCategoryBarRecorded.push({value: $scope.searchCategoryRecordedModel[i], content: $scope.searchCategoryRecordedModel[i], type: 'category'});
					}
					return queryValCat;
				}
				else return '';
			}
			else return '';
		}

		$scope.getCurrentDate = function() {
			var urlString = $window.location.href;
			urlString = urlString.slice(urlString.indexOf('*')+1, urlString.length);
			urlString = stringToJSON(urlString);

			var posLat = urlString.lat.toString();
			var posLng = urlString.lng.toString();

			posLat = posLat.replace(/\:/g,'.');
			posLng = posLng.replace(/\:/g,'.');


			$scope.initRecordedData = {
				initTimestamp: urlString.timestamp__gte,
				initDate: urlString.timestamp__lte,
				initRadius: parseFloat(urlString.radius),
				initUom: urlString.uom,
				initLat: parseFloat(posLat), 
				initLng: parseFloat(posLng),
				zoom: parseFloat(urlString.zoom),
				location: urlString.location_name,
				query: checkQuery(urlString.query),
				categories: checkCategoryQuery(urlString.all_categories)
			}
			$scope.eventDateTimeFrom = initTimestampToLocal($scope.initRecordedData.initTimestamp);
			$('.filter.from').css('pointer-events', 'none');
			localStorage.setItem("liveStreamTimestampInit", $scope.initRecordedData.initTimestamp);
			$scope.getRecordedData($scope.initRecordedData.initTimestamp, $scope.initRecordedData.initDate, $scope.initRecordedData.initRadius, $scope.initRecordedData.initUom, $scope.initRecordedData.initLat, $scope.initRecordedData.initLng, $scope.initRecordedData.zoom);
		}

		$scope.paginationRight = function() {
			if ($scope.nextValueCheck != null) {
				mapDrawCheck = true;
				$scope.nextValue += 9;
				//mapService.clearHotspots();
				mapService.setDefaultPin();
				$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getRecordedData();
			}
		}
		$scope.paginationLeft = function() {
			if($scope.nextValue > 0) {
				mapDrawCheck = false;
				$scope.nextValue -= 9;
				mapService.setDefaultPin();
				$scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
				$rootScope.markersArray = [];
				$scope.paginationPageHighlight($scope.nextValue);
				$scope.paginationBar();
				$scope.getRecordedData();
			}
		}

		$scope.displayTwitterPins = function($event) {
	    	$scope.cleanEventMarkers();
	    	mapService.clearMarkers();
	    	mapService.clearHotspots();
	    	$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.searchCategoryEventModel = [];
	    	$scope.goToPage(1);
	    }
	    $scope.displayInstagramPins = function($event) {
	    	$scope.cleanEventMarkers();
	    	mapService.clearMarkers();
	    	mapService.clearHotspots();
	    	$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;
			$scope.searchCategoryEventModel = [];
	    	$scope.goToPage(1);
	    }

		$scope.getCurrentDate();

		$scope.chooseCategory = function($event) {
	    	$scope.cleanEventMarkers();
	    	mapService.clearMarkers();
			mapService.clearHotspots();
			$rootScope.markersArray = [];
	    	$scope.nextValue = 0;
			$scope.progressBarValue = 0;
			$scope.paginationPageNum = 10;

			if ($event != undefined) {
				var categoryValue = $event.currentTarget.innerText;
		    	categoryValue = categoryValue.replace(/ /g, "");

		    	$scope.showCategorySelector = !$scope.showCategorySelector;
		    	$scope.optionsCategoryBarRecorded.push({value: $event.currentTarget.innerText, content: categoryValue, type: 'category'});
		    	$scope.searchCategoryRecordedModel.push($event.currentTarget.innerText);
			}

	    	$scope.goToPage(1);
	    }

	    $scope.showFullImg = function($event, image, post) {
	    	modalService.showFullImage($event, $scope, image, post);
	    }

		//
	    // TAB FUNCTIONS
	    //

	    $scope.callSplitView = function(){
	    	tabService.splitView();
	    	mapService.mapUpdate('recorded', 'mapRecorded');
	    }

	    $scope.callMapShowFull = function(){
	    	tabService.mapShowFull();
	    	mapService.mapUpdate('recorded', 'mapRecorded');
	    }

	    $scope.callGalleryShowFull = function(){
	    	tabService.galleryShowFull();
	    }
	    
	    $scope.callChangeTab = function(tab) {
	    	tabService.changeTab(tab);
	    }

	    $rootScope.keyPress = function(keyCode) {
			if (keyCode == 39) { // check if "Right Arrow" key
				$scope.paginationRight();
			}
			else if (keyCode == 37) { // check if "Left Arrow" key
				$scope.paginationLeft();
			}
		}

	    //
	    // 	MODAL FUNCTIONS
	    //
	    
	    $scope.callDatePickerModal = function($event) {
	    	modalService.datePickerModal($event, $scope);
	    }
	    
	    $scope.annotatePost = function($event) {
	    	modalService.annotatePost($event, $scope);
	    }

	    $(document).on("click", ".view-hotspot-btn", function() {
	      sessionStorage.removeItem('hotspotCategory');
	      var hotspotIdValue = $(this).attr('hotspotId');
	      var redirectToVal = $(this).attr('redirectToValue');
	      sessionStorage.setItem('hotspotCategory', JSON.stringify($scope.searchCategoryRecordedModel));
	      mapService.initHotspot(hotspotIdValue, redirectToVal);
	    });

	    $(document).on("click", ".map-show-full-img", function() {
	      var image = $(this).next().attr("src");
	      modalService.showFullImage('', $scope, image);
	    });

	    $(document).on("click", ".pagination-list li:nth-child(1)", function() {
	    	if ($scope.paginationPageNum > 10) {
	    		$scope.paginationPageNum -= 1;
	    	}
	    });
	    $(document).on("click", ".pagination-list li:nth-child(2)", function() {
	    	if ($scope.paginationPageNum > 10) {
	    		$scope.paginationPageNum -= 1;
	    	}
	    });
	    $(document).on("click", ".pagination-list li:nth-child(3)", function() {
	    	if ($scope.paginationPageNum > 10) {
	    		$scope.paginationPageNum -= 1;
	    	}
	    });

	    $(document).on("click", ".pagination-list li:nth-child(10)", function() {
	    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
	    		$scope.paginationPageNum += 1;
	    	}
	    });
	    $(document).on("click", ".pagination-list li:nth-child(9)", function() {
	    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
	    		$scope.paginationPageNum += 1;
	    	}
	    });
	    $(document).on("click", ".pagination-list li:nth-child(8)", function() {
	    	if ($scope.paginationPageNum < $scope.paginationPages-1) {
	    		$scope.paginationPageNum += 1;
	    	}
	    });

        //
        //  Examine Tab
        //

        // Initialize examine tab on current controller;
        analyticsService.examineTab($scope, {
            searchBarFieldName: 'searchbarModelRecorded',
            categoryClassifierFieldName: 'searchCategoryRecordedModel',
            dateTimeFromFieldName: 'eventDateTimeFromObj',
            dateTimeToFieldName: 'eventDateTimeToObj'
        });

        // Define extra filters for analytics data.
        $scope.analyticsExtraFiltres = {};
		
        function updateAnalyticsExtraFilters() {
			/*  Set timestamp__gte, timestamp__gte and location params to
				aggregation queries.
			 * */
            var now = new Date();

            if ("undefined" !== typeof $rootScope.dashboardCache.stream_started
                && $rootScope.dashboardCache.stream_started) {
                var gte = $rootScope.dashboardCache.stream_started;
        	}
            if ("undefined" === typeof gte) {
            	var recordedInit = localStorage.getItem('recordedInit');
            	if (recordedInit) {
                    var init = JSON.parse(recordedInit);
                    var gte = init.initTimestamp;
                }
            }
            if ("undefined" === typeof gte)
				var gte = $scope.initRecordedData.initTimestamp;

            // Check if live stream start lesser than 50 mins ago, than add
            // extended_bounds_min parameter.
            var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(),
                now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(),
                now.getUTCSeconds());
            var diff = new Date(gte).getTime() - now_utc.getTime() - 1000;
            if (diff <= 0) {
                var _d = new Date();
                _d.setMinutes(now_utc.getMinutes() - 50);
                gte = analyticsService.dateToString(_d);
            }

            $scope.analyticsExtraFiltres = {
                timestamp__lte: analyticsService.dateToString(now),
                timestamp__gte: $scope.initRecordedData.initTimestamp,
                extended_bounds_min: gte,
                lng: $scope.initRecordedData.initLng,
                lat: $scope.initRecordedData.initLat,
                radius: $scope.initRecordedData.initRadius,
                uom: $scope.initRecordedData.initUom
            }
        }

        updateAnalyticsExtraFilters();

        // Change category
        var hideCategoryListHandler = function () {
            $(".change-category-list").hide();
            $(document).off("click", hideCategoryListHandler);
        };

        $(document).on("click", ".post-category .category-text, .post-category .change-category", function(event) {
            hideCategoryListHandler();
            $(this).parent().parent().find('.change-category-list').show();
            event.stopPropagation();

            $(document).on("click", hideCategoryListHandler);
        });

        $(document).on("click", ".change-category-list-item", function(event) {
            event.stopPropagation();
            var postID = $(this).attr('post-id');
            var category = $(this).html();
            var callback = function () {
                mapService.clearMarkers();
                mapService.clearHotspots();
                $scope.goToPage($scope.currentPageNum);
            };
            StreamingService.updateCategory(postID, category, callback, callback);
            hideCategoryListHandler();
        });

  }]);
	
})();
