(function () {
  'use strict';

  angular.module("exampleApp")
    .filter('formatDate', function($filter){
        return function(date) {
                return $filter('date')(date, 'MM/dd/yyyy') ;
            }
    })
    .filter('formatTime', function($filter){
        return function(date){
            return $filter('date')(date, 'h:mm a');
        }
    })
    .filter('reverse', function() {
      return function(items) {
        return items.slice().reverse();
      }
    })
    .controller('listsCtrl', ['$rootScope', '$scope', '$http', 'modalService', 'ListService', function($rootScope, $scope, $http, modalService, ListService) {
    	$scope.myModel = 1;

        $scope.listPreloader = true;
        $scope.listMenuShow = false;
        $scope.listNoListMsg = false;
        $scope.listsItemList = false;

        $scope.sortModel = 'created';

        $rootScope.dashboardCache.stream = false;

  	//
    // MODALS SERVICE FUNCTIONS
    //

    $scope.callCreateListItem = function($event) {
        modalService.createListItem($event, $scope);
    }

    $scope.calldeleteListItem = function($event, listId) {
    	modalService.deleteListItem($event, $scope, listId);
    }

    $scope.callDatePickerModal = function($event) {
    	modalService.datePickerModal($event, $scope);
    }

    $scope.sortByDate = function() {
        $scope.sortModel = $scope.sortByDateListModel;
        $scope.getListData();
    }
    $scope.sortByName = function() {
        $scope.sortModel = $scope.sortByNameListModel;
        $scope.getListData();
    }

    $scope.resetAll = function($event) { 
        $scope.sortByDateListModel = 'created';
        $scope.sortByNameListModel = 'name';
        $scope.sortModel = 'created';
        $scope.listSearchModel = '';
        $("#listSortByDate").select2("val", "");
        $("#listSortByName").select2("val", "");
        $scope.getListData();
    }

    $scope.chechListData = function(listData) {
        if (listData > 0) {
            $scope.listPreloader = false;
            $scope.listMenuShow = true;
            $scope.listsItemList = true;
            $scope.listNoListMsg = false;
        }
        else {
            $scope.listsItemList = false;
            $scope.listPreloader = false;
            $scope.listMenuShow = false;
            $scope.listNoListMsg = true;
        }
    }

    $scope.$on('createListTitle', function(event, data) {
        $scope.createListTitle();
    })
    $scope.createListTitle = function() {
        var body = {
            name: $rootScope.listTitle,
            objects: ''
        }
        ListService.createNewList(body,
            function(response) {
                console.log(response);
                if (response.status == 201) {
                   modalService.createListSuccess(' ', $scope);
                   $scope.getListData(); 
                }
                else {
                    modalService.createListError(' ', $scope); 
                }
            },
            function(error) {
                console.log(error);
                modalService.createListError(' ', $scope);
            }
        )
    }

    $scope.getListData = function() {
        ListService.getListData($scope.sortModel,
            function(response) {
                console.log(response);
                $scope.listsData = response.data.objects;

                $scope.chechListData(response.data.meta.total_count);
            },
            function(error) {
                console.log(error);

                $scope.listsItemList = false;
                $scope.listPreloader = false;
                $scope.listMenuShow = false;
                $scope.listNoListMsg = true;
            }
        )
    }
    $scope.getListData();

    $scope.$on('deleteList', function(event, listId) {
        $scope.deleteList(listId.listId);
    })

    $scope.deleteList = function(listId) {
        ListService.deleteList(listId,
            function(response) {
                $scope.getListData();
            },
            function(error) {
                console.log(error);
            }
        )
    }

    }]);
})();
