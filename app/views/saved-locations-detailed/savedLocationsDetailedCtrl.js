(function () {
    'use strict';

    angular.module("exampleApp")
    .filter('formatDate', function($filter){
    return function(date) {
            /*var timeToLocal = date;  
            timeToLocal = timeToLocal.replace(/T/g, " ");
            timeToLocal = timeToLocal + ' UTC';
            timeToLocal = new Date(timeToLocal);

            var timeToLocalFormated = (
              timeToLocal.getFullYear() + '-' +
              ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
              ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
              ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
              ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
              ("00" + timeToLocal.getSeconds()).slice(-2)
            );*/
            return $filter('date')(date, 'MM/dd/yyyy') ;
        }
  })
  .filter('formatTime', function($filter){
    return function(date){
        /*var timeToLocal = date;  
        timeToLocal = timeToLocal.replace(/T/g, " ");
        timeToLocal = timeToLocal + ' UTC';
        timeToLocal = new Date(timeToLocal);

        var timeToLocalFormated = (
          timeToLocal.getFullYear() + '-' +
          ("00" + (timeToLocal.getMonth() + 1)).slice(-2) + "-" + 
          ("00" + timeToLocal.getDate()).slice(-2) + 'T' +
          ("00" + timeToLocal.getHours()).slice(-2) + ":" + 
          ("00" + timeToLocal.getMinutes()).slice(-2) + ":" + 
          ("00" + timeToLocal.getSeconds()).slice(-2)
        );*/
        return $filter('date')(date, 'h:mm a');
    }
  })
  .filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase.length > 0) {
        for (var i in phrase) {
            if (phrase[i] != 'AND' && phrase[i] != 'OR') {
                text = text.replace(new RegExp('('+phrase[i]+')', 'gi'), '<span class="highlighted">$1</span>');
            }
        } 
        return $sce.trustAsHtml(text);
      }
      else {return text};
    }
  })
  
    .controller("savedLocationsDetailedCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', 'mapService', 'StreamingService','tabService', 'modalService', 'ListService', '$timeout', 'SaveLocationService', 'analyticsService', function($rootScope, $scope, $http, $location, $stateParams, mapService, StreamingService, tabService, modalService, ListService, $timeout, SaveLocationService, analyticsService) {

        $scope.locationId = $stateParams.locationId;

        $scope.eventErrorMsg = false;
        $scope.eventPreloader = true;
        $scope.noItemsMsg = false;

        $scope.leftBtn = true;
        $scope.rightBtn = false;

        $scope.nextValue = 0;
        $scope.progressBarValue = 0;

        $rootScope.markersArray = [];

        $scope.checkTwitterPins = true;
        $scope.checkInstagramPins = false;

        $scope.socialNewsPopup = false;

        $scope.nextValueCheck = '';

        $scope.paginationPagesArray = [];

        $scope.paginationPageNum = 10;

        $scope.eventMarkersArray = [];

        $scope.paginationShow = false;

        $scope.postsHistogram = false;
        $scope.categoryHistogram = false;
        $scope.categoryPie = false;
        $scope.histogramOptions = {
            posts: 0,
            pieData: {}
        };

        $scope.currentPageNum = 1;

        $scope.categories = ["Violence", "Shooting", "ActiveShooter", "Terrorism", "Disaster", "Generic"];


        var postCount;
        var isMapInited = false;
        var mapDrawCheck = true;

        /*(function callMapInit() {
            mapService.mapInitEvent();
        })();*/

        //
        // SELECTIZE INIT
        //

        $scope.searchBarSaveLocModel = [];

        $scope.optionsSearchBarSaveLoc = [
        ];

        $scope.searchCategorySaveLocModel = [];

        $scope.optionsCategoryBarSaveLoc = [
        ];

        $scope.configSearchBarSaveLoc = {
            create: true,
            plugins: ['remove_button'],
            valueField: 'value',
            labelField: 'content',
            delimiter: '+',
            onInitialize: function(selectize){
            },
            render: {
                item: function(item, escape) {
                    if (item.type == 'category') {
                        return '<div class="item">'+
                        '<img src="../../dist/images/'+item.content+'.png" class="category-tag-image">'+item.value+
                        '</div>';
                    }
                    else if (item.value == 'OR') {
                        return '<div class="item special-tag or">'
                        +item.value+
                        '</div>';
                    }
                    else if (item.value == 'AND') {
                        return '<div class="item special-tag and">'
                        +item.value+
                        '</div>';
                    }
                    else {
                        return '<div class="item">'
                        +item.value+
                        '</div>';
                    }                       
                }
            }
        };

        $scope.sortByDate = function() {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;
            $scope.goToPage(1); 
        }
        $scope.sortBySentiment = function() {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0; 
            $scope.paginationPageNum = 10;
            $scope.goToPage(1);
        }
        $scope.resetAll = function($event) { 
            $scope.nextValue = 0;
            $scope.progressBarValue = 0; 
            $scope.paginationPageNum = 10;
            $scope.savedLocationSortByDate = '-timestamp';
            $scope.savedLocationSortBySentiment = '*';
            $("#eventSortByDate").select2("val", "");
            $("#eventSortBySentiment").select2("val", "");
            $scope.goToPage(1);
        }
        $scope.sortByPostType = function() {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;
            $scope.goToPage(1); 
        }
        $scope.gallerySearch = function() {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0; 
            $scope.paginationPageNum = 10;
            $scope.goToPage(1);
        }

        $scope.paginationBar = function() {
            $scope.progressBarValue = ($scope.nextValue*100)/postCount;
            $scope.progressBarValue = $scope.progressBarValue.toFixed(1);
            if($scope.progressBarValue > 100) {
                $scope.progressBarValue = 100;
            }
        }

        $scope.zoomToPostPin = function(LatLng, mapName) {
            mapService.zoomToPostPin(LatLng, mapName);
        }
        $scope.zoomToMap = function(mapName) {
            mapService.zoomToMap(mapName);
        }

        $scope.showPinOnMap = function(postID, mapName) {
            mapService.showPinOnMap(postID, mapName);
        }

        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        $scope.radiusInMeters = function(radius, unit) {
            if (unit == 'mi') {
                return radius * 1.6 * 1000; 
            }
            else {
                return radius * 0.3048; 
            }
        } 

        $scope.drawMarkers = function() {
            for (var item in $scope.saveLocationData) {
                var distance = 1000 * getDistanceFromLatLonInKm($scope.locationData.lat, $scope.locationData.lng, $scope.saveLocationData[item].location[1], $scope.saveLocationData[item].location[0]);
                var radius = $scope.radiusInMeters($scope.locationData.radius, $scope.locationData.uom);
                if (radius >= distance) {
                    mapService.drawMarker('mapSaveLocation', 'circleSavedLocation', $scope, $scope.saveLocationData[item].location, $scope.saveLocationData[item].platform, $scope.saveLocationData[item].category, $scope.saveLocationData[item].post_id, $scope.saveLocationData[item].content, $scope.saveLocationData[item].user_profile_image_url, $scope.saveLocationData[item].user_name, $scope.saveLocationData[item].media_url);
                }
                console.log(radius + ' ' + distance);
            }
        }
        $scope.drawHotspots = function() {
            mapService.clearHotspots();
            for (var hotspot in $scope.saveLocationHotspots) {
                mapService.drawHotspots('mapSaveLocation', 'circleSavedLocation', $scope.saveLocationHotspots[hotspot].location, $scope.saveLocationHotspots[hotspot].doc_count.total, $scope.saveLocationHotspots[hotspot].doc_count.platform.twitter, $scope.saveLocationHotspots[hotspot].doc_count.platform.instagram, $scope.saveLocationHotspots[hotspot].key, $scope.saveLocationHotspots[hotspot].avg_sentiment.value, $scope.saveLocationHotspots[hotspot].doc_count.category, $scope.saveLocationHotspots[hotspot].top_category, $scope.saveLocationHotspots[hotspot].redirect_to);
            }
        }

        $scope.checkLastPage = function(lastPage) {
            if (lastPage == null) {
                $scope.rightBtn = true;
                $scope.progressBarValue = 100;
            }
        }

        $scope.drawPagination = function(postCount) {
            $scope.paginationPages = postCount / 9;
            if ($scope.paginationPages % 1 === 0) {
                $scope.paginationPages = parseInt($scope.paginationPages);
            }
            else {
                $scope.paginationPages = parseInt($scope.paginationPages);
                $scope.paginationPages += 2;
            }
            for(var i = 0; i < $scope.paginationPages; i++) {
                var page = {
                    index: i,
                    pageNum: i+1
                }
                $scope.paginationPagesArray.push(page);
            }
        }

        $scope.markLastPage = function(lastPage) {
            $timeout(function(){
                $('.pagination-item').removeClass('active');
                $('#pageId-'+lastPage).addClass('active');
            }, 100);
        }

        $scope.goToPage = function(pageNum) {
            $('.pagination-item').removeClass('active');
            $('#pageId-'+pageNum).addClass('active');
            mapDrawCheck = true;
            $scope.nextValue = (pageNum*9)-9;
            mapService.clearHotspots();
            mapService.setDefaultPin();
            $scope.currentPageNum = pageNum;
            $scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
            $rootScope.markersArray = [];
            $scope.paginationBar();
            $scope.savedLocation();

            if ($scope.tabIndex == 'examine') {
                $scope.callExamineView();
            }
        }

        $scope.paginationPageHighlight = function(nextValue) {
            var nextVal = nextValue/9+1;
            $('.pagination-item').removeClass('active');
            $('#pageId-'+nextVal).addClass('active');

            if($('#pageId-'+nextVal).is(':last-child'))
            {
                $scope.paginationPageNum += 5;
            }
            if($('#pageId-'+nextVal).is(':first-child') && $scope.paginationPageNum > 10)
            {
                $scope.paginationPageNum -= 5;
            }
        }

        $scope.showLeftBtn = function(previousValue) {
            if (previousValue != null) $scope.leftBtn = false;
            else {$scope.leftBtn = true; $scope.progressBarValue = 0;}
        }
        $scope.showRightBtn = function(totalCount) {
            if (totalCount > 9) $scope.rightBtn = false;
            else {$scope.rightBtn = true; $scope.paginationShow = false;}
        }

        $scope.noPostsMsgCheck = function(postCountLength) {
            if (postCountLength == 0) {
                $scope.noPostsMsg = true;
                $scope.paginationShow = false;
                $scope.cleanEventMarkers();
                mapService.clearMarkers();
                mapService.clearHotspots();
            }
            else {
                $scope.noPostsMsg = false;
                $scope.paginationShow = true;
            }
        }

        $scope.onePageCheck = function(postCount) {
            if(postCount <= 9 && postCount != 0) {
                $scope.progressBarValue = 100;
            }
        }

        $scope.timestampFilter = function() {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;
            $scope.filterPopup = !$scope.filterPopup;
            $scope.goToPage(1);
        }

        $scope.checkSortByDate = function(date) {
            if (date == undefined) return '-timestamp';
            else return date;
        }      

        $scope.savedLocation = function() {
            SaveLocationService.getSavedLocation($scope.locationId, $scope.nextValue, $scope.checkSortByDate($scope.savedLocationSortByDate), $scope.savedLocationSortBySentiment, $scope.postTypeSelect, $scope.checkTwitterPins, $scope.checkInstagramPins, $scope.searchBarSaveLocModel, $scope.searchCategorySaveLocModel, $scope.locationDate, $scope.eventDateTimeFromObj, $scope.eventDateTimeToObj,
                function(response) {
                    console.log(response);
                    if(isMapInited == false) {
                        $scope.isStramActive = response.data.is_active;
                        mapService.mapInitSavedLocation(response.data.lat, response.data.lng, response.data.radius, response.data.uom, response.data.zoom);
                        $scope.eventLocationName = response.data.note;
                        $scope.locationDate = response.data.created;
                        $scope.drawPagination(response.data.stream_entries.meta.total_count);
                        $scope.getListData();
                        $scope.locationData = {
                            lat: response.data.lat,
                            lng: response.data.lng,
                            radius: response.data.radius,
                            uom: response.data.uom,
                            zoom: response.data.zoom,
                            created: response.data.created
                        }
                        updateAnalyticsExtraFilters();

                    }
                    isMapInited = true;

                    postCount = response.data.stream_entries.meta.total_count;
                    
                    $scope.onePageCheck(response.data.stream_entries.meta.total_count);
                
                    $scope.noPostsMsgCheck(response.data.stream_entries.meta.total_count);

                    $scope.showLeftBtn(response.data.stream_entries.meta.previous);
                    $scope.showRightBtn(response.data.stream_entries.meta.total_count);

                    if (response.data.stream_entries.meta.total_count > 90) {
                        $scope.drawPagination(response.data.stream_entries.meta.total_count);
                    }

                    $scope.eventPreloader = false;
                    $scope.saveLocationData = response.data.stream_entries.objects;
                    $scope.saveLocationHotspots = response.data.stream_entries.hotspots;

                    $scope.eventErrorMsg = false;

                    $scope.nextValueCheck = response.data.stream_entries.meta.next;
                    $scope.checkLastPage($scope.nextValueCheck);


                    $scope.drawMarkers();
                    if(mapDrawCheck == true) {
                        $scope.drawHotspots();
                    }
                },
                function(error) {
                    console.log(error);
                    $scope.eventPreloader = false;
                    $scope.eventErrorMsg = true;
                    $scope.rightBtn = true;
                }
                )
        }

        $scope.savedLocation();

        $scope.paginationRight = function() {
            if ($scope.nextValueCheck != null) {
                mapDrawCheck = true;
                $scope.nextValue += 9;
                //mapService.clearHotspots();
                mapService.setDefaultPin();
                $scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
                $rootScope.markersArray = [];
                $scope.paginationPageHighlight($scope.nextValue);
                $scope.paginationBar();
                $scope.savedLocation();
            }
        }
        $scope.paginationLeft = function() {
            if($scope.nextValue > 0) {
                mapDrawCheck = false;
                $scope.nextValue -= 9;
                mapService.setDefaultPin();
                $scope.eventMarkersArray = $scope.eventMarkersArray.concat($rootScope.markersArray);
                $rootScope.markersArray = [];
                $scope.paginationPageHighlight($scope.nextValue);
                $scope.paginationBar();
                $scope.savedLocation();
            }
        }

        $scope.cleanEventMarkers = function() {
            for(var marker in $scope.eventMarkersArray) {
                $scope.eventMarkersArray[marker].setMap(null);
            }
            $scope.eventMarkersArray = [];
        }

        //Show/Hide Twiiter/Instagram pins on map
        $scope.displayTwitterPins = function($event) {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;
            $scope.searchCategoryEventModel = [];
            $scope.goToPage(1);
        }
        $scope.displayInstagramPins = function($event) {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;
            $scope.searchCategoryEventModel = [];
            $scope.goToPage(1);
        }

        $scope.chooseCategory = function($event) {
            $scope.cleanEventMarkers();
            mapService.clearMarkers();
            mapService.clearHotspots();
            $rootScope.markersArray = [];
            $scope.nextValue = 0;
            $scope.progressBarValue = 0;
            $scope.paginationPageNum = 10;

            if ($event != undefined) {
                var categoryValue = $event.currentTarget.innerText;
                categoryValue = categoryValue.replace(/ /g, "");

                $scope.showCategorySelector = !$scope.showCategorySelector;
                $scope.optionsCategoryBarSaveLoc.push({value: $event.currentTarget.innerText, content: categoryValue, type: 'category'});
                $scope.searchCategorySaveLocModel.push($event.currentTarget.innerText);
            }

            $scope.goToPage(1);
        }


        //
        //  MODAL FUNCTIONS
        //

        $scope.callDatePickerModal = function($event) {
            modalService.datePickerModal($event, $scope);
        }

        //
        // GLOBAL KEYDOWN FUNCTION
        //

        $rootScope.keyPress = function(keyCode) {
            if (keyCode == 39) { // check if "Right Arrow" key
                $scope.paginationRight();
            }
            else if (keyCode == 37) { // check if "Left Arrow" key
                $scope.paginationLeft();
            }
        }

        //
        // TAB FUNCTIONS
        //

        $scope.callSplitView = function(){
            tabService.splitView();
            mapService.mapUpdate('event', 'mapEvent');
        }

        $scope.callMapShowFull = function(){
            tabService.mapShowFull();
            mapService.mapUpdate('event', 'mapEvent');
        }

        $scope.callGalleryShowFull = function(){
            tabService.galleryShowFull();
        }

        $scope.callChangeTab = function(tab) {
            tabService.changeTab(tab);
        }

        $scope.annotatePost = function($event) {
            modalService.annotatePost($event, $scope);
        }

        var initNewUserList = false;
        $scope.createNewUserList = function($event) {
            if ($scope.userListData.length == 0) {
                modalService.callCreateNewUserList($event, $scope);
            }
        }

        $scope.getListData = function() {
            ListService.getListData($scope.sortModel,
                function(response) {
                    console.log(response);
                    $rootScope.userListData = response.data.objects;
                    if (initNewUserList == true) {
                        if (response.data.objects.length == 0) {
                            $scope.createNewUserList();
                        }
                    }
                    initNewUserList = true;
                },
                function(error) {
                    console.log(error);
                }
            )
        }


        $scope.goToLive = function() {
            $scope.goToLiveObj = {
                radius: $scope.locationData.radius,
                uom: $scope.locationData.uom,
                center: { lat: $scope.locationData.lat, lng: $scope.locationData.lng},
                zoom: $scope.locationData.zoom,
                locationId: $scope.locationId,
                is_active: $scope.isStramActive
            }
            localStorage.setItem('savedLocation', JSON.stringify($scope.goToLiveObj));
            window.open('/dashboard');
        }

        $scope.addPostToList = function(postId, listId) {
            if (listId != null) {
                $rootScope.postsId.push(postId);
                ListService.addPostToList(postId, listId,
                    function(response) {
                        console.log(response);
                    },
                    function(error) {
                        console.log(error);
                    }
                )
            }
        }

        $scope.showFullImg = function($event, image, post) {
            modalService.showFullImage($event, $scope, image, post);
        }

        $(document).on("click", ".view-hotspot-btn", function() {
          sessionStorage.removeItem('hotspotCategory');
          var hotspotIdValue = $(this).attr('hotspotId');
          var redirectToVal = $(this).attr('redirectToValue');
          sessionStorage.setItem('hotspotCategory', JSON.stringify($scope.searchCategorySaveLocModel));
          mapService.initHotspot(hotspotIdValue, redirectToVal);
        });

        $(document).on("click", ".map-show-full-img", function() {
          var image = $(this).next().attr("src");
          modalService.showFullImage('', $scope, image);
        });

        $(document).on("click", ".pagination-list li:nth-child(1)", function() {
            if ($scope.paginationPageNum > 10) {
                $scope.paginationPageNum -= 1;
            }
        });
        $(document).on("click", ".pagination-list li:nth-child(2)", function() {
            if ($scope.paginationPageNum > 10) {
                $scope.paginationPageNum -= 1;
            }
        });
        $(document).on("click", ".pagination-list li:nth-child(3)", function() {
            if ($scope.paginationPageNum > 10) {
                $scope.paginationPageNum -= 1;
            }
        });

        $(document).on("click", ".pagination-list li:nth-child(10)", function() {
            if ($scope.paginationPageNum < $scope.paginationPages-1) {
                $scope.paginationPageNum += 1;
            }
        });
        $(document).on("click", ".pagination-list li:nth-child(9)", function() {
            if ($scope.paginationPageNum < $scope.paginationPages-1) {
                $scope.paginationPageNum += 1;
            }
        });
        $(document).on("click", ".pagination-list li:nth-child(8)", function() {
            if ($scope.paginationPageNum < $scope.paginationPages-1) {
                $scope.paginationPageNum += 1;
            }
        });

        /*  Examine tab

         */
        // Flags that rule of showing blocks on examine tab.
        $scope.examineHidePreInfo = false;
        $scope.examineHideInfo = true;

        // Initialize examine tab on currrent controller.
        analyticsService.examineTab($scope, {
            searchBarFieldName: 'searchBarSaveLocModel',
            categoryClassifierFieldName: 'searchCategorySaveLocModel',
            dateTimeFromFieldName: 'eventDateTimeFromObj',
            dateTimeToFieldName: 'eventDateTimeToObj'
        });

        // Define extra filters for analytics data.
        $scope.analyticsExtraFiltres = {};

        function updateAnalyticsExtraFilters () {
            /*  Set query params.
             */
            $scope.examineHidePreInfo = true;
            $scope.examineHideInfo = false;

            var now = new Date(),
                gte = $scope.locationData.created;

            $scope.analyticsExtraFiltres = {
                saved_location: $scope.locationId,
                timestamp__gte: gte,
                timestamp__lte: analyticsService.dateToString(now)
            }
            // Check if stream start lesser than 50 mins ago, than add
            // extended_bounds_min parameter.
            var fiftyMinsBefore = new Date(now.getUTCFullYear(), now.getUTCMonth(),
                now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(),
                now.getUTCSeconds());
            fiftyMinsBefore.setMinutes(fiftyMinsBefore.getMinutes() - 50);

            var diff = new Date(gte).getTime() - fiftyMinsBefore.getTime();
            if (diff > 0) {
                $scope.analyticsExtraFiltres.extended_bounds_min =
                    analyticsService.dateToString(fiftyMinsBefore, false);
            }
            if ($scope.tabIndex == 'examine') {
                $scope.callExamineView();
            }
        }

        // Change category
        var hideCategoryListHandler = function () {
            $(".change-category-list").hide();
            $(document).off("click", hideCategoryListHandler);
        };

        $(document).on("click", ".post-category .category-text, .post-category .change-category", function(event) {
            hideCategoryListHandler();
            $(this).parent().parent().find('.change-category-list').show();
            event.stopPropagation();

            $(document).on("click", hideCategoryListHandler);
        });

        $(document).on("click", ".change-category-list-item", function(event) {
            event.stopPropagation();
            var postID = $(this).attr('post-id');
            var category = $(this).html();
            var callback = function () {
                mapService.clearMarkers();
                mapService.clearHotspots();
                $scope.goToPage($scope.currentPageNum);
            };
            StreamingService.updateCategory(postID, category, callback, callback);
            hideCategoryListHandler();
        });
    }]);
})();

