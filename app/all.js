'use strict';

/* App init */
require('./app');

/* Directives */
require('./app-directives/gallery-scroller.directive');
require('./app-directives/scroll-to-top.directive');
require('./app-directives/img-error.directive');
require('./app-directives/isolate.directive');

/* Services */
require('./app-services/alert.service.js');
require('./app-services/analytics.service.js');
require('./app-services/authentication.service');
require('./app-services/items.service');
require('./app-services/list.service.js');
require('./app-services/map.service');
require('./app-services/modal.service');
require('./app-services/saveLocation.service.js');
require('./app-services/streaming.service.js');
require('./app-services/tabs.service');
require('./app-services/user.service');

/* Controllers */
require('./configFile');
require('./appCtrl');
require('./views/login/loginCtrl');
require('./views/register/registerCtrl');
require('./views/dashboard/dashboardCtrl');
require('./views/saved-locations/savedLocationsCtrl');
require('./views/saved-locations-detailed/savedLocationsDetailedCtrl');
require('./views/lists/listsCtrl');
require('./views/recorded/recordedCtrl');
require('./views/list-detailed/listDetailedCtrl');
require('./views/alerts/alertsCtrl');
require('./views/notifications/notificationsCtrl');
require('./views/examine/examineCtrl');
require('./views/trainer/trainerCtrl');
require('./views/event/eventCtrl');
require('./views/liveevent/liveeventCtrl');
require('./views/hotspot/hotspotCtrl');
require('./views/demo/demoCtrl');
require('./views/demo/demoLoopCtrl');
require('./views/streaming-page/streamingPageCtrl');



