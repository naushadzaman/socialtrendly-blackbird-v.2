(function () {
  'use strict';

  angular.module("exampleApp", ["app.config", "ngCookies", "ngMap", "selectize", "ui.select2", "ngMaterial", "ui.router", "ngAnimate", "ngSanitize", "ui.utils.masks", "kendo.directives"])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {

      // Delete not necessary symbols from url

      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });

      $stateProvider

      .state('dashboard', {
        url: "/dashboard",
        activetab: 'dashboard',
        data : { pageTitle: 'Dashboard' },
        views: {
          '@': {
            templateUrl: "views/dashboard/dashboard.html",
            controller: "dashboardCtrl"
          },
          'header@dashboard': {
            templateUrl: "views/header.html"
          },
          'footer@dashboard': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('login', {
        url: "/login",
        data : { pageTitle: 'Login' },
        views: {
          '@': {
            templateUrl: "/views/login/login.html",
            controller: "loginCtrl"
          }
        }
      })

      .state('registration', {
        url: "/registration",
        data : { pageTitle: 'Registration' },
        views: {
          '@': {
            templateUrl: "/views/login/registration.html",
            controller: "loginCtrl"
          }
        }
      })

      .state('approval', {
        url: "/approval",
        data : { pageTitle: 'Approval' },
        views: {
          '@': {
            templateUrl: "/views/login/approval.html",
            controller: "loginCtrl"
          }
        }
      })

      .state('locations', {
        url: "/locations",
        activetab: 'locations',
        data : { pageTitle: 'Saved locations' },
        views: {
          '@': {
            templateUrl: "/views/saved-locations/saved-locations.html",
            controller: "savedLocationsCtrl"
          },
          'header@locations': {
            templateUrl: "views/header.html"
          },
          'footer@locations': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('locationsDetailed', {
        url: "/locations/:locationId",
        activetab: 'locations-detailed',
        data : { pageTitle: 'Saved location' },
        views: {
          '@': {
            templateUrl: "/views/saved-locations-detailed/saved-locations-detailed.html",
            controller: "savedLocationsDetailedCtrl"
          },
          'header@locationsDetailed': {
            templateUrl: "views/header.html"
          },
          'footer@locationsDetailed': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('lists', {
        url: "/lists",
        activetab: 'lists',
        data : { pageTitle: 'Lists' },
        views: {
          '@': {
            templateUrl: "/views/lists/lists.html",
            controller: "listsCtrl"
          },
          'header@lists': {
            templateUrl: "views/header.html"
          },
          'footer@lists': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('demo', {
        url: "/demo",
        activetab: 'demo',
        data : { pageTitle: 'Demo' },
        views: {
          '@': {
            templateUrl: "/views/demo/demo.html",
            controller: "demoCtrl"
          },
          'header@demo': {
            templateUrl: "views/header.html"
          },
          'footer@demo': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('demoLopp', {
        url: "/loop-demo",
        activetab: 'demo',
        data : { pageTitle: 'Demo' },
        views: {
          '@': {
            templateUrl: "/views/demo/demo-loop.html",
            controller: "demoLoopCtrl"
          },
          'header@demoLopp': {
            templateUrl: "views/header.html"
          },
          'footer@demoLopp': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('recorded', {
        url: "/recorded*path",
        data : { pageTitle: 'Recorded mode' },
        views: {
          '@': {
            templateUrl: "/views/recorded/recorded.html",
            controller: "recordedCtrl"
          },
          'header@recorded': {
            templateUrl: "views/header.html"
          },
          'footer@recorded': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('event', {
        url: "/event/:eventName",
        data : { pageTitle: 'Event' },
        views: {
          '@': {
            templateUrl: "/views/event/event.html",
            controller: "eventCtrl"
          },
          'header@event': {
            templateUrl: "views/header.html"
          },
          'footer@event': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('eventGalerry', {
        url: "/event/loop/:eventName",
        data : { pageTitle: 'Event' },
        views: {
          '@': {
            templateUrl: "/views/event/event.html",
            controller: "eventCtrl"
          },
          'header@eventGalerry': {
            templateUrl: "views/header.html"
          },
          'footer@eventGalerry': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('hotspot', {
        url: "/hotspot/:hotspotId",
        data : { pageTitle: 'Hotspot' },
        views: {
          '@': {
            templateUrl: "/views/hotspot/hotspot.html",
            controller: "hotspotCtrl"
          },
          'header@hotspot': {
            templateUrl: "views/header.html"
          },
          'footer@hotspot': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('list_detailed', {
        url: "/lists/:listId",
        data : { pageTitle: 'List detailed' },
        views: {
          '@': {
            templateUrl: "/views/list-detailed/list-detailed.html",
            controller: "listDetailedCtrl"
          },
          'header@list_detailed': {
            templateUrl: "views/header.html"
          },
          'footer@list_detailed': {
            templateUrl: "views/footer.html"
          }
        }
      })

      .state('alerts', {
        url: "/alerts",
        data : { pageTitle: 'Alerts' },
        views: {
          '@': {
            templateUrl: "/views/alerts/alerts.html",
            controller: "alertsCtrl"
          },
          'header@alerts': {
            templateUrl: "views/header.html"
          }
        }
      })

      .state('notifications', {
        url: "/notifications",
        data : { pageTitle: 'Notification' },
        views: {
          '@': {
            templateUrl: "/views/notifications/notifications.html",
            controller: "notificationsCtrl"
          },
          'header@notifications': {
            templateUrl: "views/header.html"
          }
        }
      })

      .state('examine', {
        url: "/examine",
        data : { pageTitle: 'Examine' },
        views: {
          '@': {
            templateUrl: "/views/examine/examine.html",
            controller: "examineCtrl"
          },
          'header@examine': {
            templateUrl: "views/header.html"
          }
        }
      })

      .state('trainer', {
        url: "/trainer",
        templateUrl: "/views/trainer/trainer.html",
        controller: "trainerCtrl",
        data : { pageTitle: 'Trainer' }
      })

      .state('liveevent', {
        url: "/liveevent",
        data : { pageTitle: 'Live event' },
        views: {
          '@': {
            templateUrl: "/views/liveevent/liveevent.html",
            controller: "dashboardCtrl"
          },
          'header@liveevent': {
            templateUrl: "views/header.html"
          }
        }
      })

      .state('dashboardInfo', {
        url: "/info",
        data : { pageTitle: 'Streaming page' },
        views: {
          '@': {
            templateUrl: "/views/streaming-page/streaming-page.html",
            controller: "streamingPageCtrl"
          },
          'header@dashboardInfo': {
            templateUrl: "views/header.html"
          },
          'footer@dashboardInfo': {
            templateUrl: "views/footer.html"
          }
        }
      })

      $urlRouterProvider.otherwise('/info');

    }])

    .run(['$rootScope', '$location', '$cookies', '$http', 'AuthenticationService', '$state', '$stateParams', function($rootScope, $location, $cookies, $http, AuthenticationService, $state, $stateParams) {

      // Check current url and add appropriate page title

      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;

      // Keep user logged in after page refresh

      $rootScope.userInfo = $cookies.getObject('userInfo') || {};
      if ($rootScope.userInfo.username) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.userInfo.api_key;
      }

      $rootScope.$on('$locationChangeStart', function (event, next, current) {

        // Redirect to login page if not logged in and trying to access a restricted page

        if ( !AuthenticationService.checkLoginedUsers($location.path()) ) {
          $location.path('/login');
        }
      });

    }])

  })();
