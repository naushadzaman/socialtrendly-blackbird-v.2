var gulp = require('gulp');
	connect = require('gulp-connect'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	browserify = require('gulp-browserify'),
	mainBowerFiles = require('gulp-main-bower-files'),
	gulpFilter = require('gulp-filter'),
	history = require('connect-history-api-fallback'),
	rimraf = require('gulp-rimraf'),
	sequence = require('run-sequence'),
	ngConfig = require('gulp-ng-config');

/**
 * Sources Paths
 */
const PATHS = {
    styles: [
        `app/components/bootstrap/dist/css/bootstrap.css`
    ],
	sourceFonts: [
		`app/app-content/fonts/`,
	],
	distCSS: `app/dist/css/`,
	distJS: `app/dist/js/`,
    distFonts: `app/dist/fonts/`,
    distImg: `app/dist/images/`,
	distApp: `app`
};


/** Styles **/
gulp.task('sass:app', () => {
    return gulp
        .src('app/app-content/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(PATHS.distCSS))
});

gulp.task('sass:vendor', function() {
    return gulp
    	.src(PATHS.styles)
			.pipe(concat('vendor.min.css'))
			.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
			.pipe(gulp.dest(PATHS.distCSS))
});

gulp.task('styles', ['sass:app', 'sass:vendor']);

/** Scripts **/
gulp.task('js:app', function() {
    return gulp
    	.src('app/all.js')
        .pipe(browserify({
          insertGlobals : false //false for production
        }))
				//.pipe(uglify())
        .pipe(gulp.dest(PATHS.distJS))
});

gulp.task('js:vendor', function() {
    var filterJS = gulpFilter('**/*.js');
    return gulp
    	.src('./bower.json')
        .pipe(mainBowerFiles())
        .pipe(filterJS)
        .pipe(concat('vendor.js'))
				.pipe(uglify())
        .pipe(gulp.dest(PATHS.distJS));
});

gulp.task('scripts', ['angular-config', 'js:app', 'js:vendor']);

/** Server start **/
gulp.task('webserver', function() {
	connect.server({
		root: 'app',
        port: 80,
		middleware: function (connect, opt) {
			return [ history({}) ]
		}
	})
});

/** COMMON TASKS **/
gulp.task('clean', () => {
    return gulp
        .src('./app/dist', {read: false})
        .pipe(rimraf());
});

gulp.task('copy:fonts', () => {
    return gulp
        .src(PATHS.sourceFonts + '**/*.*')
        .pipe(gulp.dest(PATHS.distFonts));
});

gulp.task('copy:images', () => {
  return gulp
    .src(`app/app-content/images/` + '**/*.*')
    .pipe(gulp.dest(PATHS.distImg));
});

gulp.task('build', (cb) => {
    sequence('clean',
        [
        	'copy:fonts',
            'copy:images',
            'scripts',
            'styles',
        ], () => cb());
});

gulp.task('angular-config', function () {
    gulp.src('configFile.json')
        .pipe(ngConfig('app.config'))
        .pipe(gulp.dest(PATHS.distApp))
});

/** Watch **/
gulp.task('watch', () => {
    gulp.watch('app/app-content/scss/**/*.scss', ['sass:app']);
    gulp.watch('app/**/*.js', ['js:app']);
});

gulp.task('default', ['webserver', 'watch']);
